package fr.frivec.api.core.player.message;

import java.util.UUID;

public class PrivateMessage {
	
	private UUID sender, receiver;
	private UUID processUUID;
	private String message;
	
	public PrivateMessage(UUID sender, UUID receiver, UUID processUUID, String message) {
		
		this.sender = sender;
		this.processUUID = processUUID;
		this.receiver = receiver;
		this.message = message;
		
	}

	public UUID getSender() {
		return sender;
	}

	public void setSender(UUID sender) {
		this.sender = sender;
	}

	public UUID getReceiver() {
		return receiver;
	}

	public void setReceiver(UUID receiver) {
		this.receiver = receiver;
	}
	
	public UUID getProcessUUID() {
		return processUUID;
	}
	
	public void setProcessUUID(UUID processUUID) {
		this.processUUID = processUUID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
