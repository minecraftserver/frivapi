package fr.frivec.api.core.player.redisdata;

import java.util.UUID;

import javax.annotation.Nullable;

public class ChangeServerMessage {
	
	private UUID playerUUID,
				serverUUID,
				proxyUUID;
	private String message;
	
	public ChangeServerMessage(final UUID playerUUID, final UUID serverUUID, final UUID proxyUUID, @Nullable final String message) {
		
		this.playerUUID = playerUUID;
		this.serverUUID = serverUUID;
		this.proxyUUID = proxyUUID;
		this.message = message;
		
	}

	public UUID getPlayerUUID() {
		return playerUUID;
	}

	public void setPlayerUUID(UUID playerUUID) {
		this.playerUUID = playerUUID;
	}
	
	public UUID getServerUUID() {
		return serverUUID;
	}
	
	public void setServerUUID(UUID serverUUID) {
		this.serverUUID = serverUUID;
	}
	
	public UUID getProxyUUID() {
		return proxyUUID;
	}
	
	public void setProxyUUID(UUID proxyUUID) {
		this.proxyUUID = proxyUUID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
