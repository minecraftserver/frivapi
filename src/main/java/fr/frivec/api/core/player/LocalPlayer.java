package fr.frivec.api.core.player;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.hub.player.HubPlayer;
import fr.frivec.api.core.player.location.Location;
import fr.frivec.api.core.player.message.PlayerMessage;
import fr.frivec.api.core.player.redisdata.ChangeServerMessage;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.utils.Utils;
import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.Redis;
import fr.frivec.core.redis.pubsub.PubSubManager;
import fr.frivec.core.spectre.ServerInformations;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import javax.annotation.Nullable;
import java.util.UUID;

public class LocalPlayer {
	
	/*
	 * Contains Player data
	 * 
	 */
	
	private UUID uuid;
	private String name;
	private float emeralds, coins;
	private UUID currentServer, currentProxy;
	private RankList rank;
	
	private HubPlayer hubPlayer;
	
	private boolean ban, mute;
	
	private boolean friendsRequests, privateMessages, notifications;
	
	//Not save
	private boolean online;
	private UUID previousPrivateMessage;

	public LocalPlayer(final UUID uuid, final String name, final RankList rank, final float emeralds, final float coins, final boolean ban, final boolean mute,
			final boolean friendsRequests, final boolean privateMessages, final boolean notifications) {
		
		this.uuid = uuid;
		this.name = name;
		this.rank = rank;
		this.emeralds = emeralds;
		this.coins = coins;
		this.ban = ban;
		this.mute = mute;
		this.friendsRequests = friendsRequests;
		this.privateMessages = privateMessages;
		this.notifications = notifications;
		
		this.online = false;
		
	}
	
	public static LocalPlayer getLocalPlayer(final UUID uuid) {
		
		final RedissonClient client = Redis.getInstance().getClient();
		final RBucket<String> bucket = client.getBucket("LOCALPLAYER:" + uuid.toString());
		
		if(bucket.isExists())
			
			return (LocalPlayer) GsonManager.getInstance().deSeralizeJson(bucket.get(), LocalPlayer.class);
		
		else {
			
			if(Utils.isBungee) {
				
				final FindIterable<LocalPlayer> iterableLocalPlayer = ProxyAPI.getInstance().getLocalPlayerCollection().find(Filters.eq("_id", uuid.toString()));
				final FindIterable<HubPlayer> iterableHubPlayer = ProxyAPI.getInstance().getHubPlayerCollection().find(Filters.eq("_id", uuid.toString()));
				
				final LocalPlayer localPlayer = iterableLocalPlayer.first();
				final HubPlayer hubPlayer = iterableHubPlayer.first();
				
				if(localPlayer != null) {
				
					if(hubPlayer != null) {
						
						localPlayer.setHubPlayer(hubPlayer);
						hubPlayer.setLocalPlayer(localPlayer);
						
					}
					
					bucket.set(GsonManager.getInstance().serializeObject(localPlayer));
					
				}
				
				return localPlayer;
				
			}else {
				
				//for Spigot servers, a message will be sent to the proxy to get the local player from database
				return null;
				
			}
			
		}			
			// mangez 5 fruites et légumes par jour
	}

	/**
	 * Send a message to the player independently of the server
	 * The message will be sent using BungeeCord Chat API
	 * @param text The message to want to send to the player
	 */
	public void sendMessage(final String text) {

		final PlayerMessage message = new PlayerMessage(this.uuid, this.currentProxy, text);

		PubSubManager.getInstance().publish("PlayerMessage", GsonManager.getInstance().serializeObject(message));

	}
	
	/**
	 * Redirect the player on a server connected to a bungeecord proxy
	 * @param serverName: Server's name. If null, redirect the player to a random hub. Doesn't work if the player is already on a hub.
	 * @param message: Message sent to the player when redirected to the server. If null, no message will be sent.
	 */
	public void sendToServer(@Nullable final String serverName, @Nullable final String message) {
		
		final ServerInformations infos = ServerInformations.getInfos(Redis.getInstance().getClient(), serverName);
		
		if(infos != null)
		
			sendToServer(infos.getId(), infos.getProxyID(), message);
		
		else
		
			sendToServer(null, this.currentProxy, message);
		
	}
	
	/**
	 * Redirect the player on a server connected to a bungeecord proxy
	 * @param serverUUID: Server's name. If null, redirect the player to a random hub. Doesn't work if the player is already on a hub.
	 * @param proxyUUID: Server's proxy uuid. Cannot be null
	 * @param message: Message sent to the player when redirected to the server. If null, no message will be sent.
	 */
	public void sendToServer(@Nullable final UUID serverUUID, final UUID proxyUUID, @Nullable final String message) {
		
		final ChangeServerMessage serverMessage = new ChangeServerMessage(this.uuid, serverUUID, proxyUUID, message);
		
		PubSubManager.getInstance().publish("SendPlayer", GsonManager.getInstance().serializeObject(serverMessage));
		
	}
	
	public void sendInRedis() {
		
		final RedissonClient client = Redis.getInstance().getClient();
		final String json = GsonManager.getInstance().serializeObject(this);
		
		client.getBucket("LOCALPLAYER:" + this.uuid.toString()).set(json);
		
	}
	
	public void removeFromRedis() {
		
		final RedissonClient client = Redis.getInstance().getClient();
		
		client.getBucket("LOCALPLAYER:" + this.uuid.toString()).delete();
		
	}
	
	public void save(final boolean update) {
		
		if(!Utils.isBungee) {
			
			Logger.log(LogLevel.SEVERE, "Error. Trying to save localplayer data on database from a spigot server.", this.getClass());
			
			return;
			
		}
		
		if(update) {
			
			ProxyAPI.getInstance().getLocalPlayerCollection().findOneAndReplace(Filters.eq("_id", this.uuid.toString()), this);
			ProxyAPI.getInstance().getHubPlayerCollection().findOneAndReplace(Filters.eq("_id", this.uuid.toString()), this.hubPlayer);
		
		}else {
			
			ProxyAPI.getInstance().getLocalPlayerCollection().insertOne(this);
			ProxyAPI.getInstance().getHubPlayerCollection().insertOne(this.hubPlayer);
			
		}
		
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public RankList getRank() {
		return rank;
	}
	
	public void setRank(RankList rank) {
		this.rank = rank;
	}

	public float getEmeralds() {
		return emeralds;
	}

	public void setEmeralds(float emeralds) {
		this.emeralds = emeralds;
	}

	public float getCoins() {
		return coins;
	}

	public void setCoins(float coins) {
		this.coins = coins;
	}

	public UUID getCurrentServer() {
		return currentServer;
	}

	public void setCurrentServer(UUID currentServer) {
		this.currentServer = currentServer;
	}

	public UUID getCurrentProxy() {
		return currentProxy;
	}

	public void setCurrentProxy(UUID currentProxy) {
		this.currentProxy = currentProxy;
	}

	public boolean isBan() {
		return ban;
	}

	public void setBan(boolean ban) {
		this.ban = ban;
	}

	public boolean isMute() {
		return mute;
	}

	public void setMute(boolean mute) {
		this.mute = mute;
	}
	
	public boolean isOnline() {
		return online;
	}
	
	public void setOnline(boolean online) {
		this.online = online;
	}
	
	public UUID getPreviousPrivateMessage() {
		return previousPrivateMessage;
	}
	
	public void setPreviousPrivateMessage(UUID previousPrivateMessage) {
		this.previousPrivateMessage = previousPrivateMessage;
	}
	
	public Location getLocation() {		
		return getHubPlayer().getLocation();
	}
	
	public void setLocation(Location location) {
		this.getHubPlayer().setLocation(location);
	}
	
	public HubPlayer getHubPlayer() {
		return hubPlayer;
	}
	
	public void setHubPlayer(HubPlayer hubPlayer) {
		this.hubPlayer = hubPlayer;
	}
	
	public boolean isFriendsRequests() {
		return friendsRequests;
	}
	
	public void setFriendsRequests(boolean friendsRequests) {
		this.friendsRequests = friendsRequests;
	}
	
	public boolean isNotifications() {
		return notifications;
	}
	
	public void setNotifications(boolean notifications) {
		this.notifications = notifications;
	}
	
	public boolean isPrivateMessages() {
		return privateMessages;
	}
	
	public void setPrivateMessages(boolean privateMessages) {
		this.privateMessages = privateMessages;
	}

}
