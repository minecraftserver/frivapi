package fr.frivec.api.core.player.location;

public class Location {
	
	private double x, y, z;
	private float pitch, yaw;
	
	private String worldName;
	
	public Location(final double x, final double y, final double z, final float yaw, final float pitch, final String worldName) {
		
		this.x = x;
		this.y = y;
		this.z = z;
		
		this.yaw = yaw;
		this.pitch = pitch;
		
		this.worldName = worldName;
		
	}
	
	public Location(final double x, final double y, final double z, final String worldName) {
		
		this(x, y, z, 0.0f, 180.0f, worldName);
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public String getWorldName() {
		return worldName;
	}

	public void setWorldName(String worldName) {
		this.worldName = worldName;
	}

}
