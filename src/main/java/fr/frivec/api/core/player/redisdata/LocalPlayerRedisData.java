package fr.frivec.api.core.player.redisdata;

import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.player.LocalPlayer;
import net.md_5.bungee.api.ProxyServer;

public class LocalPlayerRedisData {
	
	private HashSet<LocalPlayer> playerDataToRemove;
	
	public LocalPlayerRedisData() {
		
		this.playerDataToRemove = new HashSet<>();
		
	}
	
	public void delayedDeleteData(final LocalPlayer localPlayer) {
		
		this.getPlayerDataToRemove().add(localPlayer);
		
		ProxyServer.getInstance().getScheduler().schedule(ProxyAPI.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				if(!getPlayerDataToRemove().contains(localPlayer))
					
					return;
				
				if(localPlayer.isOnline())
					
					return;
				
				ProxyAPI.getInstance().getRedis().getClient().getBucket("LOCALPLAYER:" + localPlayer.getUuid().toString()).delete();
				getPlayerDataToRemove().remove(localPlayer);
				
			}
			
		}, 1, TimeUnit.HOURS);
		
	}
	
	public void deleteAllData() {
		
		final Iterator<LocalPlayer> iterator = this.getPlayerDataToRemove().iterator();
		
		iterator.forEachRemaining(localPlayer -> {
			
			ProxyAPI.getInstance().getRedis().getClient().getBucket("LOCALPLAYER:" + localPlayer.getUuid().toString()).delete();
			iterator.remove();
			
		});
		
	}
	
	public HashSet<LocalPlayer> getPlayerDataToRemove() {
		return playerDataToRemove;
	}

}
