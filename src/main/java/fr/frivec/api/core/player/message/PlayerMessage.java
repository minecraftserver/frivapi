package fr.frivec.api.core.player.message;

import java.util.UUID;

public class PlayerMessage {

    private final UUID receiver;
    private final UUID processID;
    private final String message;

    public PlayerMessage(final UUID receiver, final UUID processID, final String message) {

        this.receiver = receiver;
        this.processID = processID;
        this.message = message;

    }

    public UUID getReceiver() {
        return receiver;
    }

    public UUID getProcessID() {
        return processID;
    }

    public String getMessage() {
        return message;
    }
}
