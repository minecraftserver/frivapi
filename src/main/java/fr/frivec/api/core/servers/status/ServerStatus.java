package fr.frivec.api.core.servers.status;

public enum ServerStatus {
	
	OPEN,
	MAP_LOADING,
	CLOSING,
	CLOSE;

}
