package fr.frivec.api.core.hub.jump;

public class JumpStatistic {
	
	private int trials;
	private Time bestTime;

	public JumpStatistic(final int trials, final Time bestTime) {
		
		this.trials = trials;
		this.bestTime = bestTime;
	
	}
	
	public int getTrials() {
		return trials;
	}
	
	public void setTrials(int trials) {
		this.trials = trials;
	}
	
	public Time getBestTime() {
		return bestTime;
	}
	
	public void setBestTime(Time bestTime) {
		this.bestTime = bestTime;
	}
	
}
