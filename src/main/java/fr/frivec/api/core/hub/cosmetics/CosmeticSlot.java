package fr.frivec.api.core.hub.cosmetics;

public class CosmeticSlot {
	
	private int slot,
				cosmeticID;

	public CosmeticSlot(final int slot, final int cosmeticID) {
		
		this.slot = slot;
		this.cosmeticID = cosmeticID;
		
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public int getCosmeticID() {
		return cosmeticID;
	}

	public void setCosmeticID(int cosmeticID) {
		this.cosmeticID = cosmeticID;
	}
	
}