package fr.frivec.api.core.hub.jump;

public class Time {
	
	private int minutes,
				seconds,
				milliseconds;

	public Time(int minutes, int seconds, int milliseconds) {
		
		this.minutes = minutes;
		this.seconds = seconds;
		this.milliseconds = milliseconds;
	
	}
	
	public boolean isBetterThan(final Time otherTime) {
		
		//If the other time is under 0, it's null so this time is better.
		if(otherTime.getMinutes() < 0 && otherTime.getSeconds() < 0 && otherTime.getMilliseconds() < 0)
			
			return true;
		
		if(this.minutes < otherTime.getMinutes())
			
			return true;
		
		else if(this.minutes == otherTime.getMinutes()) {
				
				if(this.seconds < otherTime.getSeconds())
					
					return true;
				
				else if(this.seconds == otherTime.getSeconds()) {
					
					if(this.milliseconds < otherTime.getMilliseconds())
						
						return true;
					
					else 
						
						/*
						 * If there is an equality, return false. Same if the other time is worst
						 */
						
						return false;
					
				}else
					
					return false;
				
		}else
				
			return false;
		
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getMilliseconds() {
		return milliseconds;
	}

	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}
	
}
