package fr.frivec.api.core.hub.player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.frivec.api.core.hub.cosmetics.CosmeticSlot;
import fr.frivec.api.core.hub.jump.JumpStatistic;
import fr.frivec.api.core.hub.jump.Time;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.location.Location;

public class HubPlayer {
	
	private UUID uuid;
	
	private Location location;
	private JumpStatistic jumpStatistics;
	private boolean invisibilityPowder;
	
	private ArrayList<Integer> ownedCosmetics;
	private List<CosmeticSlot> inventorySave;
	
	private boolean surprise, jumpSurprise;
	
	private int maxDoubleJump, currentDoubleJumpUse;
	
	private transient LocalPlayer localPlayer;
	
	public HubPlayer(final LocalPlayer localPlayer, final boolean invisibilityPowder, final ArrayList<Integer> ownedCosmetics, final List<CosmeticSlot> inventorySave) {
		
		this.localPlayer = localPlayer;
		
		if(this.localPlayer != null)
		
			this.uuid = localPlayer.getUuid();
		
		this.jumpStatistics = new JumpStatistic(0, new Time(-1, -1, -1));
		
		this.invisibilityPowder = invisibilityPowder;
		this.ownedCosmetics = ownedCosmetics;
		this.inventorySave = inventorySave;
		this.surprise = false;
		this.jumpSurprise = false;
				
	}
	
	public void addCosmeticInSave(final int slot, final int cosmeticID) {
		
		this.inventorySave.add(new CosmeticSlot(slot, cosmeticID));
		
	}
	
	public JumpStatistic getJumpStatistics() {
		return jumpStatistics;
	}
	
	public void setJumpStatistics(JumpStatistic jumpStatistics) {
		this.jumpStatistics = jumpStatistics;
	}
	
	public boolean hasInvisibilityPowder() {
		return invisibilityPowder;
	}
	
	public void setInvisibilityPowder(boolean invisibilityPowder) {
		this.invisibilityPowder = invisibilityPowder;
	}
	
	public ArrayList<Integer> getOwnedCosmetics() {
		return ownedCosmetics;
	}
	
	public void setOwnedCosmetics(ArrayList<Integer> ownedCosmetics) {
		this.ownedCosmetics = ownedCosmetics;
	}
	
	public int getMaxDoubleJump() {
		return maxDoubleJump;
	}
	
	public void setMaxDoubleJump(int maxDoubleJump) {
		this.maxDoubleJump = maxDoubleJump;
	}
	
	public int getCurrentDoubleJumpUse() {
		return currentDoubleJumpUse;
	}
	
	public void setCurrentDoubleJumpUse(int currentDoubleJumpUse) {
		this.currentDoubleJumpUse = currentDoubleJumpUse;
	}
	
	public LocalPlayer getLocalPlayer() {
		
		if(this.localPlayer == null)
			
			this.localPlayer = LocalPlayer.getLocalPlayer(this.uuid);
		
		return localPlayer;
	}
	
	public void setLocalPlayer(LocalPlayer localPlayer) {
		this.localPlayer = localPlayer;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public List<CosmeticSlot> getInventorySave() {
		return inventorySave;
	}
	
	public void setInventorySave(List<CosmeticSlot> inventorySave) {
		this.inventorySave = inventorySave;
	}
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	public void setSurprise(boolean surprise) {
		this.surprise = surprise;
	}
	
	public boolean hasSurprise() {
		return surprise;
	}
	
	public void setJumpSurprise(boolean jumpSurprise) {
		this.jumpSurprise = jumpSurprise;
	}
	
	public boolean hasJumpSurprise() {
		return jumpSurprise;
	}
	
}
