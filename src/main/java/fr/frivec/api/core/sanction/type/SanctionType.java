package fr.frivec.api.core.sanction.type;

import fr.frivec.api.core.sanction.Ban;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.Kick;

public enum SanctionType {
	
	BAN(Ban.class),
	MUTE(Mute.class),
	KICK(Kick.class);
	
	private Class<?> clazz;
	
	private SanctionType(final Class<?> clazz) {
		
		this.clazz = clazz;
		
	}
	
	public Class<?> getClazz() {
		return clazz;
	}
	
	public static SanctionType getSanctionType(final String name) {
		
		for(SanctionType types : SanctionType.values())
			
			if(types.toString().equalsIgnoreCase(name))
				
				return types;
		
		return null;
		
	}

}
