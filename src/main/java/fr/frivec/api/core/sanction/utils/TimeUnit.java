package fr.frivec.api.core.sanction.utils;

import java.util.Calendar;

public enum TimeUnit {
	
	MINUTE("m", Calendar.MINUTE),
	DAY("d", Calendar.DAY_OF_MONTH),
	HOUR("h", Calendar.HOUR),
	WEEK("w", Calendar.WEEK_OF_MONTH),
	MONTH("Mo", Calendar.MONTH),
	YEAR("y", Calendar.YEAR);
	
	private String text;
	private int calendarUnit;
	
	private TimeUnit(final String text, final int calendarUnit) {
		
		this.text = text;
		this.calendarUnit = calendarUnit;
		
	}
	
	public int getCalendarUnit() {
		return calendarUnit;
	}
	
	public String getText() {
		return text;
	}
	
	public static TimeUnit getTimeUnit(final String text) {
		
		for(TimeUnit unit : TimeUnit.values())
			
			if(unit.getText().equalsIgnoreCase(text))
				
				return unit;
		
		return null;
		
	}

}
