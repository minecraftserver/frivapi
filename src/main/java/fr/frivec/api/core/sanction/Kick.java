package fr.frivec.api.core.sanction;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.sanction.messaging.KickPlayerMessage;
import fr.frivec.api.core.sanction.type.SanctionType;
import fr.frivec.core.json.GsonManager;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Kick extends Sanction {
	
	public Kick(UUID mod, UUID target, String reason) {
		
		super(SanctionType.KICK, mod, target, reason, Date.from(Instant.now()).getTime(), Date.from(Instant.now()).getTime(), false);
		
	}
	
	@Override
	public void applySanction(LocalPlayer localPlayer) {
		
		if(localPlayer.isOnline()){
			
			final ArrayList<String> kickMessage = new ArrayList<>();
			
			kickMessage.add("§7§m-------------------------");
			kickMessage.add("§cVous avez été éjecté du serveur.");
			kickMessage.add("§bRaison : §7" + this.getReason());
			kickMessage.add("");
			kickMessage.add("§aCeci n'est aucunement un banissement. Vous pouvez vous reconnecter directement.");
			kickMessage.add("§7§m-------------------------");
			
			final KickPlayerMessage kickPlayerMessage = new KickPlayerMessage(localPlayer.getUuid(), kickMessage);
			
			ProxyAPI.getInstance().getChannelManager().publish("KickPlayer", GsonManager.getInstance().serializeObject(kickPlayerMessage));
			
		}
		
	}
	
	@Override
	public void kickPlayer(ProxiedPlayer player) {
		
		final List<String> kickMessage = new ArrayList<>();
		
		kickMessage.add("§7§m-------------------------");
		kickMessage.add("§cVous avez été éjecté du serveur.");
		kickMessage.add("§bRaison : §7" + this.getReason());
		kickMessage.add("");
		kickMessage.add("§aCeci n'est aucunement un banissement. Vous pouvez vous reconnecter directement.");
		kickMessage.add("§7§m-------------------------");
				
		player.disconnect(new TextComponent(String.join("\n", kickMessage)));
		
	}
	
	@Override
	public void kickPlayer(PendingConnection connection) {
		
		final List<String> kickMessage = new ArrayList<>();
		
		kickMessage.add("§7§m-------------------------");
		kickMessage.add("§cVous avez été éjecté du serveur.");
		kickMessage.add("§bRaison : §7" + this.getReason());
		kickMessage.add("");
		kickMessage.add("§aCeci n'est aucunement un banissement. Vous pouvez vous reconnecter directement.");
		kickMessage.add("§7§m-------------------------");
				
		connection.disconnect(new TextComponent(String.join("\n", kickMessage)));
		
	}

}
