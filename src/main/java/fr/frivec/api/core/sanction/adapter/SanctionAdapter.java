package fr.frivec.api.core.sanction.adapter;

import java.io.IOException;
import java.util.UUID;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import fr.frivec.api.core.sanction.Ban;
import fr.frivec.api.core.sanction.Kick;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.type.SanctionType;

public class SanctionAdapter extends TypeAdapter<Sanction> {
	
	@Override
	public Sanction read(JsonReader reader) throws IOException {
		
		int id = -1;
		SanctionType type = null;
		UUID mod = null, target = null;
		String reason = "";
		long start = 0, end = 0;
		boolean permanent = false, finished = false;
		
		if(reader != null && reader.hasNext()) {
			
			reader.beginObject();
			
			while(reader.hasNext()) {
				
				switch (reader.nextName()) {
				
				case "id":
					
					id = reader.nextInt();
					
					break;
					
				case "type":
					
					type = SanctionType.getSanctionType(reader.nextString());
					
					break;
					
				case "mod":
					
					mod = UUID.fromString(reader.nextString());
					
					break;
					
				case "target":
					
					target = UUID.fromString(reader.nextString());
					
					break;
					
				case "reason":
					
					reason = reader.nextString();
					
					break;
					
				case "start":
					
					start = reader.nextLong();
					
					break;
					
				case "end":
					
					end = reader.nextLong();
					
					break;
					
				case "permanent":
					
					permanent = reader.nextBoolean();
					
					break;
					
				case "finished":
					
					finished = reader.nextBoolean();
					
					break;

				default:
					break;
				}
				
			}
			
			reader.endObject();
			
		}
		
		Sanction sanction = null;
		
		switch (type) {
		
		case BAN:
			
			sanction = new Ban(mod, target, reason, start, end, permanent);
			
			break;
			
		case MUTE:
			
			sanction = new Mute(mod, target, reason, start, end, permanent);
			
			break;
			
		case KICK:
			
			sanction = new Kick(mod, target, reason);
			
			break;

		default:
			break;
		}
		
		sanction.setId(id);
		sanction.setFinished(finished);
		
		return sanction;
	}
	
	@Override
	public void write(JsonWriter writer, Sanction sanction) throws IOException {
		
		if(sanction != null) {
			
			writer.beginObject();
			
			writer.name("id").value(sanction.getId());
			writer.name("type").value(sanction.getSanctionType().toString());
			writer.name("mod").value(sanction.getMod().toString());
			writer.name("target").value(sanction.getTarget().toString());
			writer.name("reason").value(sanction.getReason());
			writer.name("start").value(sanction.getStart());
			writer.name("end").value(sanction.getEnd());
			writer.name("permanent").value(sanction.isPermanent());
			writer.name("finished").value(sanction.isFinished());
			
			writer.endObject();
			
		}
		
	}

}
