package fr.frivec.api.core.sanction;

import java.util.Date;
import java.util.UUID;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.sanction.type.SanctionType;
import fr.frivec.api.core.utils.Utils;
import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public abstract class Sanction {
	
	private static transient int previousID = -1;
	
	private int id;
	private SanctionType sanctionType;
	
	private UUID mod, target;
	private String reason;
	
	private long start, end;
	private boolean permanent, finished;
	
	private transient Date startDate, endDate;

	public Sanction(final SanctionType type, UUID mod, UUID target, String reason, long start, long end, boolean permanent) {
		
		this.id = -1;
		this.sanctionType = type;
		this.mod = mod;
		this.target = target;
		this.reason = reason;
		this.start = start;
		this.end = end;
		this.permanent = permanent;
		this.startDate = new Date(start);
		this.endDate = new Date(end);
		
	}
	
	public abstract void kickPlayer(final ProxiedPlayer player);
	
	public abstract void kickPlayer(final PendingConnection connection);
	
	public abstract void applySanction(final LocalPlayer localPlayer);
	
	public void create() {
		
		/*
		 * Define sanction@Override
	's id
		 */
		if(Sanction.previousID == -1) {
			
			//The previous id is not loaded.
			
			final Iterable<Document> iterable = ProxyAPI.getInstance().getSanctionCollection().find();
			
			iterable.forEach(sanction -> {
				
				int sanctionID = sanction.getInteger("_id");
				
				if(sanctionID > Sanction.previousID)
					
					Sanction.previousID = sanctionID;
				
			});
			
		}
		
		this.id = Sanction.previousID + 1;
		Sanction.previousID = this.id;
		
		saveInDatabase(false);
		
	}
	
	public void saveInDatabase(final boolean update) {
		
		if(!Utils.isBungee) {
			
			Logger.log(LogLevel.WARNING, "Method saveinDatabase has been used by a spigot server", getClass());
			
			return;
			
		}
		
		final Document document = new Document().append("_id", this.id)
												.append("type", this.sanctionType.toString())
												.append("target", this.target.toString())
												.append("mod", this.mod.toString())
												.append("reason", this.reason)
												.append("permanent", this.permanent)
												.append("start", this.start)
												.append("end", this.end)
												.append("finished", this.finished);
		
		if(update)
		
			ProxyAPI.getInstance().getSanctionCollection().findOneAndReplace(Filters.eq("_id", this.id), document);
		
		else
			
			ProxyAPI.getInstance().getSanctionCollection().insertOne(document);
		
	}
	
	public static Sanction getSanction(final UUID uuid, final SanctionType type, final int id) {
		
		FindIterable<Document> iterable = null;
		
		if(id > 0) {
			
			iterable = ProxyAPI.getInstance().getSanctionCollection().find(Updates.combine(Filters.eq("_id", id), Filters.eq("target", uuid.toString()), Filters.eq("type", type.toString())));
			
		}else {
			
			iterable = ProxyAPI.getInstance().getSanctionCollection().find(Updates.combine(Filters.eq("target", uuid.toString()), Filters.eq("type", type.toString()), Filters.eq("finished", false)));
			
		}
		
		final Document document = iterable.first();
		Sanction sanction = null;
		
		if(type.equals(SanctionType.BAN))
				
			sanction = new Ban(UUID.fromString(document.getString("mod")), UUID.fromString(document.getString("target")), document.getString("reason"), document.getLong("start"), document.getLong("end"), document.getBoolean("permanent"));
		
		else if(type.equals(SanctionType.MUTE))
			
			sanction = new Mute(UUID.fromString(document.getString("mod")), UUID.fromString(document.getString("target")), document.getString("reason"), document.getLong("start"), document.getLong("end"), document.getBoolean("permanent"));
		
		else if(type.equals(SanctionType.KICK)) {
			
			sanction = new Kick(UUID.fromString(document.getString("mod")), UUID.fromString(document.getString("target")), document.getString("reason"));
			
			sanction.setStart(document.getLong("start"));
			sanction.setEnd(document.getLong("end"));
			sanction.setPermanent(document.getBoolean("permanent"));
			
		}
			
		sanction.setId(document.getInteger("_id", -10));
		sanction.setFinished(document.getBoolean("finished", false));
		
		return sanction;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public SanctionType getSanctionType() {
		return sanctionType;
	}
	
	public void setSanctionType(SanctionType sanctionType) {
		this.sanctionType = sanctionType;
	}

	public UUID getMod() {
		return mod;
	}

	public void setMod(UUID mod) {
		this.mod = mod;
	}

	public UUID getTarget() {
		return target;
	}

	public void setTarget(UUID target) {
		this.target = target;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public void setFinished(boolean finished) {
		this.finished = finished;
	}

}
