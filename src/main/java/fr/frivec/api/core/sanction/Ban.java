package fr.frivec.api.core.sanction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.sanction.messaging.KickPlayerMessage;
import fr.frivec.api.core.sanction.type.SanctionType;
import fr.frivec.core.json.GsonManager;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Ban extends Sanction {
	
	public Ban(UUID mod, UUID target, String reason, long start, long end, boolean permanent) {
		
		super(SanctionType.BAN, mod, target, reason, start, end, permanent);		
		
	}
	
	@Override
	public void kickPlayer(final ProxiedPlayer player) {
		
		final List<String> kickMessage = new ArrayList<>();
		
		kickMessage.add("§7§m-------------------------");
		kickMessage.add("§cVous êtes bannis de ce serveur.");
		kickMessage.add("§bRaison : §7" + getReason());
		kickMessage.add(isPermanent() ? "§cCette sanction est permanente." : "§bDate d'expiration: §7" + new SimpleDateFormat("dd/MM/YY à HH:mm").format(getEndDate()));
		kickMessage.add("");
		kickMessage.add("§aSi vous souhaitez contester cette décision, merci de vous rendre sur le forum.");
		kickMessage.add("§6www.forum.unserveur.fr");
		kickMessage.add("§7§m-------------------------");
				
		player.disconnect(new TextComponent(String.join("\n", kickMessage)));
		
	}
	
	@Override
	public void kickPlayer(final PendingConnection connection) {
		
		final List<String> kickMessage = new ArrayList<>();
		
		kickMessage.add("§7§m-------------------------");
		kickMessage.add("§cVous êtes bannis de ce serveur.");
		kickMessage.add("§bRaison : §7" + getReason());
		kickMessage.add(isPermanent() ? "§cCette sanction est permanente." : "§bDate d'expiration: §7" + new SimpleDateFormat("dd/MM/YY à HH:mm").format(getEndDate()));
		kickMessage.add("");
		kickMessage.add("§aSi vous souhaitez contester cette décision, merci de vous rendre sur le forum.");
		kickMessage.add("§6www.forum.unserveur.fr");
		kickMessage.add("§7§m-------------------------");
				
		connection.disconnect(new TextComponent(String.join("\n", kickMessage)));
		
	}
	
	@Override
	public void applySanction(LocalPlayer localPlayer) {
		
		localPlayer.setBan(true);
		localPlayer.sendInRedis();
		
		if(localPlayer.isOnline()){
			
			final ArrayList<String> kickMessage = new ArrayList<>();
			
			kickMessage.add("§7§m-------------------------");
			kickMessage.add("§cVous êtes bannis de ce serveur.");
			kickMessage.add("§bRaison : §7" + getReason());
			kickMessage.add(isPermanent() ? "§cCette sanction est permanente." : "§bDate d'expiration: §7" + new SimpleDateFormat("dd/MM/YY à HH:mm").format(getEndDate()));
			kickMessage.add("");
			kickMessage.add("§aSi vous souhaitez contester cette décision, merci de vous rendre sur le forum.");
			kickMessage.add("§6www.forum.unserveur.fr");
			kickMessage.add("§7§m-------------------------");
			
			final KickPlayerMessage kickPlayerMessage = new KickPlayerMessage(localPlayer.getUuid(), kickMessage);
			
			ProxyAPI.getInstance().getChannelManager().publish("KickPlayer", GsonManager.getInstance().serializeObject(kickPlayerMessage));
			
		}
				
		return;
		
	}

}
