package fr.frivec.api.core.sanction;

import java.text.SimpleDateFormat;
import java.util.UUID;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.sanction.type.SanctionType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Mute extends Sanction {
	
	public Mute(UUID mod, UUID target, String reason, long start, long end, boolean permanent) {
		
		super(SanctionType.MUTE, mod, target, reason, start, end, permanent);
		
	}
	
	@Override
	public void applySanction(LocalPlayer localPlayer) {
		
		localPlayer.setMute(true);
		localPlayer.sendInRedis();
		
		final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(localPlayer.getUuid());
		
		player.sendMessage(new TextComponent("§7§m-----------------------"));
		player.sendMessage(new TextComponent("§cVous êtes mute."));
		player.sendMessage(new TextComponent("§bRaison : §7" + this.getReason()));
		player.sendMessage(new TextComponent(this.isPermanent() ? "§cCette sanction est permanente." : "§bDate d'expiration: §7" + new SimpleDateFormat("dd/MM/YY à HH:mm").format(this.getEndDate())));
		player.sendMessage(new TextComponent("§7§m-----------------------"));
		
		return;
		
	}
	
	@Override
	public void kickPlayer(ProxiedPlayer player) {/*Not used*/}
	
	@Override
	public void kickPlayer(PendingConnection connection) {/*Not used*/}

}
