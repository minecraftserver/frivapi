package fr.frivec.api.core.sanction.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	/**
	 * 
	 * @param date: Date to change
	 * @param amount: Amount of time to add
	 * @param calendarUnit; Unit of time. Use Calendar.*** to set the unit
	 * @return new date
	 */
	public static Date addTime(final int amount, final int calendarUnit) {
		
		final Calendar calendar = Calendar.getInstance();
		
		calendar.set(calendarUnit, calendar.get(calendarUnit) + amount);

		return calendar.getTime();
		
	}

}
