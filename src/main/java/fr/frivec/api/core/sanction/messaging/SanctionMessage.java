package fr.frivec.api.core.sanction.messaging;

import java.util.UUID;

import fr.frivec.api.core.sanction.Sanction;

public class SanctionMessage {
	
	private SanctionMessageType type;
	
	private UUID mod, target;
	private Sanction sanction;
	
	public SanctionMessage(SanctionMessageType type, UUID mod, UUID target, Sanction sanction) {
		super();
		this.type = type;
		this.mod = mod;
		this.target = target;
		this.sanction = sanction;
	}
	
	public SanctionMessageType getType() {
		return type;
	}

	public void setType(SanctionMessageType type) {
		this.type = type;
	}

	public UUID getMod() {
		return mod;
	}

	public void setMod(UUID mod) {
		this.mod = mod;
	}

	public UUID getTarget() {
		return target;
	}

	public void setTarget(UUID target) {
		this.target = target;
	}

	public Sanction getSanction() {
		return sanction;
	}

	public void setSanction(Sanction sanction) {
		this.sanction = sanction;
	}

	public enum SanctionMessageType {
		
		ADD_SANCTION(),
		REMOVE_SANCTION(),
		UPDATE_SANCTION(),
		GET_HISTORY(),
		DELETE_HISTORY(),
		UPDATE_HISTORY();
		
	}

}
