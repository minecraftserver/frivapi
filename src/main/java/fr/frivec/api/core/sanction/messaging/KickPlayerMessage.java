package fr.frivec.api.core.sanction.messaging;

import java.util.ArrayList;
import java.util.UUID;

public class KickPlayerMessage {
	
	private UUID player;
	private ArrayList<String> message;
	
	public KickPlayerMessage(final UUID player, final ArrayList<String> message) {
		
		this.player = player;
		this.message = message;
	
	}

	public UUID getPlayer() {
		return player;
	}

	public void setPlayer(UUID player) {
		this.player = player;
	}

	public ArrayList<String> getMessage() {
		return message;
	}

	public void setMessage(ArrayList<String> message) {
		this.message = message;
	}

}
