package fr.frivec.api.core.ranks;

public enum RankList {
	
	ADMIN(100, "Admin", "§c[Admin]", "", "§A"),
	MOD(50, "Mod", "§9[Mod]", "", "§B"),
	DEV(90, "Dev", "§6[Dev]", "", "§C"),
	STAFF(10, "Staff", "§3[Staff]", "", "§D"),
	FRIEND(10, "Ami", "§f[Ami]", "", "§E"),
	YT(30, "YouTube", "§d[YouTube]", "", "§F"),
	VIP_PLUS(30, "VIP+", "§b[VIP+]", "", "§G"),
	VIP(10, "VIP", "§a[VIP]", "", "§H"),
	PLAYER(0, "Joueur", "", "" , "§Z");
	
	private int power;
	private String name,
					prefix,
					suffix,
					tagName;
	
	private RankList(int power, String name, String prefix, String suffix, String tagName) {
	
		this.power = power;
		this.name = name;
		this.prefix = prefix;
		this.suffix = suffix;
		this.tagName = tagName;
	
	}
	
	public static RankList getRank(final String name) {
		
		for(RankList ranks : RankList.values())
			
			if(ranks.getName().equalsIgnoreCase(name))
				
				return ranks;
		
		return null;
		
	}
	
	public static RankList getRank(final int power) {
		
		for(RankList ranks : RankList.values())
			
			if(ranks.getPower() == power)
				
				return ranks;
		
		return null;
		
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
