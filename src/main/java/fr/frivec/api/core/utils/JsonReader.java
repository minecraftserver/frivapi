package fr.frivec.api.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class JsonReader {
	
	public static String readJson(final Path file) {
		
		if(Files.exists(file)) {
		
			final StringBuilder builder = new StringBuilder();
			
			try {
			
				final BufferedReader reader = Files.newBufferedReader(file);
				
				String line;
				
				while((line = reader.readLine()) != null)
					
					builder.append(line);
				
				reader.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return builder.toString();			
			
		}
		
		return null;
		
	}

}
