package fr.frivec.api.core.string;

import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.chat.TextComponent;

public class StringUtils {
	
	public static Component text(final String text) { return Component.text(text); }
	
	public static TextComponent textBungeecord(final String text) { return new TextComponent(text); }
	
	public static boolean compareStringAbsolute(final String str1, final String str2) {
		
		if(str1.equals(str2))
			
			return true;
		
		return false;
		
	}
	
	public static boolean compareStringRelative(final String str1, final String str2) {
	
		if(str1.equalsIgnoreCase(str2))
			
			return true;
		
		return false;
		
	}
	
	public static String translateColorSymbol(String text) {
		
		return text.replaceAll("[&]", "§");
		
	}

}
