package fr.frivec.api.spigot.scoreboard.server;

import fr.frivec.api.spigot.scoreboard.ScoreboardPattern;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.Random;

/**
 * A class to create server-sided scoreboards.
 * Usable only to show a scoreboard for all the server.
 * Compatible with ServerTeams.
 * 
 * If you want to show a personnal scoreboard for each players, use NMSScoreboard
 * 
 * @author Frivec
 *
 */
public class ServerScoreboard implements ScoreboardManager {
	
	private Scoreboard scoreboard;
	private Objective objective;
	
	private String scoreboardName;
	
	private ScoreboardPattern pattern;
	
	private String[] lines = new String[16];
	
	public ServerScoreboard(final ScoreboardPattern pattern) {
		
		this.scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
		this.pattern = pattern;
		this.scoreboardName = "sb." + new Random().nextInt(9999);
		
		this.objective = this.scoreboard.registerNewObjective(this.scoreboardName, "dummy", Component.text(pattern.getObjectiveName()));
		this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		for(int i = 0; i < this.pattern.getLines().length; i++)
			
			sendLine(this.pattern.getLines()[i], i);
		
	}
	
	public void removeLine(final int line) {
		
		final String text = lines[line];
		
		if(text != null) {
			
			lines[line] = null;
			
			this.scoreboard.resetScores(text);
			
		}	
		
		for(int i = 0; i < this.pattern.getLines().length; i++)
			
			sendLine(this.pattern.getLines()[i], i);
			
	}
	
	public void resetLines() {
		
		for(String lines : this.lines)
			
			if(lines != null)
			
				this.scoreboard.resetScores(lines);
		
	}
	
	public void sendLine(final String text, final int line) {

		if(text == null)

			return;
		
		lines[line] = text;
		
		this.objective.getScore(text).setScore(16 - line);
		
	}
	
	public void refreshScoreboard() {
		
		this.resetLines();
		
		for(int i = 0; i < this.pattern.getLines().length; i++)
			
			sendLine(this.pattern.getLines()[i], i);
		
		set();
				
	}
	
	private void set() {
		
		for(Player player : Bukkit.getOnlinePlayers())
			
			player.setScoreboard(this.scoreboard);
		
	}
	
	public String getLine(final int line) {
		
		return lines[line];
		
	}

	@Override
	public Scoreboard getMainScoreboard() {
		return this.scoreboard;
	}

	@Override
	public Scoreboard getNewScoreboard() {
		return null;
	}
	
	public void setPattern(ScoreboardPattern pattern) {
		
		this.pattern = pattern;
		
	}
	
	public ScoreboardPattern getPattern() {
		return pattern;
	}

}
