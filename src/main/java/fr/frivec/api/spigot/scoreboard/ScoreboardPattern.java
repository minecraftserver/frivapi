package fr.frivec.api.spigot.scoreboard;

public class ScoreboardPattern {
	
	/**
	 * 
	 * @author Frivec
	 * 
	 */
	
	private String objectiveName;
	private String[] lines;
	
	/**
	 * Create a scoreboard pattern with all lines and objective name. It will be reusable later
	 * @param objectiveName of the scoreboard
	 */
	public ScoreboardPattern(final String objectiveName) {
		
		this.objectiveName = objectiveName;
		this.lines = new String[16];

		/*
		Set a default value for each line. Null values are issues for the NMS or Server-side scoreboards
		 */
		for(int i = 0; i < this.lines.length; i++)

			this.lines[i] = "";
		
	}
	
	/**
	 * Add a line with an index. Example, first line of the scoreboard can be 0
	 * @param line: id of the line in the scoreboard. (1-16)
	 * @param text: text that will be set on the line on the scoreboard
	 */
	public void setLine(final int line, final String text) {
		
		if(this.lines[line] != null)
			
			removeLine(line);
		
		this.lines[line] = text;
		
	}
	
	/**
	 * Remove a line from the pattern
	 * @param line: id of the line that will be removed
	 */
	public void removeLine(final int line) {
		
		if(this.lines[line] != null)
			
			this.lines[line] = "";
		
	}
	
	public String getObjectiveName() {
		return objectiveName;
	}
	
	public String[] getLines() {
		return lines;
	}

}
