package fr.frivec.api.spigot.scoreboard.client;

import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.api.spigot.scoreboard.ScoreboardPattern;
import fr.frivec.api.spigot.teams.client.NMSTeam;
import net.minecraft.network.chat.ChatComponentText;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardDisplayObjective;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardObjective;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardScore;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardTeam;
import net.minecraft.server.ScoreboardServer;
import net.minecraft.server.ScoreboardServer.Action;
import net.minecraft.world.scores.ScoreboardObjective;
import net.minecraft.world.scores.ScoreboardTeam;
import net.minecraft.world.scores.criteria.IScoreboardCriteria;
import net.minecraft.world.scores.criteria.IScoreboardCriteria.EnumScoreboardHealthDisplay;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static fr.frivec.api.spigot.packets.PacketUtils.getEntityPlayer;
import static fr.frivec.api.spigot.packets.PacketUtils.sendPacket;

public class NMSScoreboard {
	
	private static final HashMap<Player, NMSScoreboard> scoreboards = new HashMap<>();
	
	private final Player player;

	private final ScoreboardNMSTeam[] lines = new ScoreboardNMSTeam[16];
	
	private ScoreboardPattern pattern;
	private final ScoreboardObjective objective;
	private boolean created;
	
	public NMSScoreboard(final Player player, final ScoreboardPattern pattern) {
		
		this.player = player;
		this.pattern = pattern;
		this.objective = getScoreboardObjective();
		
	}

	/**
	 * Create the scoreboard
	 * Draw all the lines
	 */
	public void create() {
		
		if(this.created)
			
			return;
		
		sendPacket(player, getObjectivePacket(ObjectiveMode.CREATE_OBJECTIVE));

		sendPacket(player, setScoreboardSlot());

		this.created = true;

		scoreboards.put(player, this);

		/*
		 * Set the lines 
		 */

		for(int i = 0; i < this.lines.length; i++)

			setLine(i, this.pattern.getLines()[i]);
		
	}
	
	/**
	 * Destroy the scoreboard for the player
	 */
	public void destroy() {
		
		if(!this.created)
			
			return;
		
		sendPacket(this.player, getObjectivePacket(ObjectiveMode.REMOVE_OBJECTIVE));

		for(ScoreboardNMSTeam teams : this.lines)
			
			if(teams != null)
				
				sendPacket(this.player, teams.createPacket(this.player, TeamMode.REMOVE, null));

		this.created = false;
		
		scoreboards.remove(this.player);
			
	}
	
	/**
	 * Update the scoreboard according to the current {@link ScoreboardPattern}
	 */
	public void refreshScoreboard() {
		
		if(!this.created || this.pattern == null)
			
			return;
		
		//Update objective name
		if(!this.objective.d().a().equals(this.pattern.getObjectiveName())) {

			System.out.println("Objective display changed");
			sendPacket(this.player, getObjectivePacket(ObjectiveMode.UPDATE_OBJECTIVE));

		}
		
		/*
		 * Set the content of pattern on lines
		 */
		for(int i = 0; i < this.pattern.getLines().length; i++) {

			final String text = this.getPattern().getLines()[i];
			final ScoreboardNMSTeam line = this.lines[i];

			if(text == null || text.isBlank()) {

				if (line != null)

					removeLine(i);

			}else

				if(line == null || !line.getValue().equals(text))

					setLine(i, text);

		}
		
		
	}
	
	/**
	 * Set the value of the line on the scoreboard
	 * @param line The index of the line you want to modify
	 * @param text The text you want to show on the line
	 */
	public void setLine(final int line, final String text) {

		if(text == null || text.isBlank())

			return;
		
		ScoreboardNMSTeam team = getTeam(line);
		
		if(this.created && team != null && team.getCurrentPlayer() != null) {

			removeLine(line);
			team = getTeam(line);

		}

		assert team != null;

		team.setValue(text);
		sendLine(line);
		
	}
	
	/**
	 * Remove a line from the scoreboard
	 * @param line The index of the line you want to remove
	 */
	private void removeLine(final int line) {
		
		final ScoreboardNMSTeam team = getTeam(line);
		
		if(this.created && team != null && team.getCurrentPlayer() != null) {
			
			sendPacket(this.player, getScorePacket(line, team.getValue(), ScoreAction.REMOVE));
			
			sendPacket(this.player, team.createPacket(this.player, TeamMode.REMOVE, null));
			
		}
		
		this.lines[line] = null;
		
	}
	
	/**
	 * Set the slot of the scoreboard. Place the scoreboard on the sidebar of the player
	 * @return the {@link PacketPlayOutScoreboardDisplayObjective} packet used to set the scoreboard to the sidebar
	 */
	private PacketPlayOutScoreboardDisplayObjective setScoreboardSlot() {

		return new PacketPlayOutScoreboardDisplayObjective(1, this.objective);
		
	}
	
	/**
	 * Create the scoreboard objective packet
	 * @param mode: Mode of the packet - 0: create objective, 1: remove objective, 2: update objective
	 * @return the {@link PacketPlayOutScoreboardObjective} used to display the objective of the scoreboard
	 */
	private PacketPlayOutScoreboardObjective getObjectivePacket(final ObjectiveMode mode) {

		return new PacketPlayOutScoreboardObjective(this.objective, mode.getMode());
		
	}
	
	/**
	 * get a new ScoreboardObjective object according to the current ScoreboardPattern object of the scoreboard
	 * @return the {@link ScoreboardObjective} used to create objectives packets
	 */
	private ScoreboardObjective getScoreboardObjective() {

		return new ScoreboardObjective(PacketUtils.getEntityPlayer(this.player).fF(), this.pattern.getObjectiveName(),
				IScoreboardCriteria.a, new ChatComponentText(this.pattern.getObjectiveName()), EnumScoreboardHealthDisplay.a);
		
	}
	
	/**
	 * get a score packet to create/update the text of a line
	 * @param line Index of the line you want to update
	 * @param text The text you want to show on the line
	 * @param action The {@link ScoreAction} you want to put into the packet
	 * @return the {@link PacketPlayOutScoreboardScore} packet to create/update the line
	 */
	private PacketPlayOutScoreboardScore getScorePacket(final int line, final String text, final ScoreAction action) {

		return new PacketPlayOutScoreboardScore(action.getAction(), this.pattern.getObjectiveName(), text, line);
		
	}
	
	/**
	 * Create all packets necessary to create or update a line
	 * @param line The index of the line you want to modify
	 */
	private void sendLine(final int line) {
		
		if(line > 15 || line < 0 || !this.created)

			return;
		
		final int score = 16 - line; //Reverse the order because index 16 in the first line from top to bottom
		final ScoreboardNMSTeam team = getTeam(line);
		
		/*
		 * Send packets to manage the team
		 */
		for(PacketPlayOutScoreboardTeam packets : team.sendLine(this.player))

			sendPacket(this.player, packets);
		
		//Send score
		sendPacket(player, getScorePacket(score, team.getValue(), ScoreAction.ADD_OR_UPDATE));

		//Reset team
		team.reset();

	}
	
	/**
	 * Get the Team linked to the line of the scoreboard
	 * @param line - The line you want to get the team associated
	 * @return the {@link NMSTeam} linked to the line
	 */
	private ScoreboardNMSTeam getTeam(final int line) {
		
		if(this.lines[line] == null)
			
			this.lines[line] = new ScoreboardNMSTeam("__fakeLine" + line);
		
		return this.lines[line];
		
	}

	private static class ScoreboardNMSTeam {

		private final String name;
		private String prefix,
						suffix,
						currentPlayer,
						oldPlayer;

		private boolean prefixChanged, suffixChanged, playerChanged = false,
						first = true;

		private ScoreboardNMSTeam(final String name, final String prefix, final String suffix) {

			this.name = name;
			this.prefix = prefix;
			this.suffix = suffix;

		}

		private ScoreboardNMSTeam(String name) {

			this(name, "", "");

		}

		public void reset() {

			this.prefixChanged = false;
			this.suffixChanged = false;
			this.playerChanged = false;
			this.oldPlayer = null;

		}


		private ScoreboardTeam createScoreboardTeamObject(final Player player){

			final ScoreboardTeam scoreboardTeam = new ScoreboardTeam(getEntityPlayer(player).fF(), this.name);

			scoreboardTeam.b(new ChatComponentText(this.prefix));
			scoreboardTeam.c(new ChatComponentText(this.suffix));

			return scoreboardTeam;

		}

		private PacketPlayOutScoreboardTeam createPacket(final Player player, final TeamMode mode, final String playerToAddOrRemove) {

			final ScoreboardTeam scoreboardTeam = createScoreboardTeamObject(player);
			PacketPlayOutScoreboardTeam packet = null;

			switch (mode) {

				case CREATE -> packet = PacketPlayOutScoreboardTeam.a(scoreboardTeam, true);

				case ADD_PLAYER -> {

					scoreboardTeam.g().add(playerToAddOrRemove);
					packet = PacketPlayOutScoreboardTeam.a(scoreboardTeam, playerToAddOrRemove, PacketPlayOutScoreboardTeam.a.a);

				}

				case REMOVE_PLAYER -> {

					scoreboardTeam.g().remove(playerToAddOrRemove);
					packet = PacketPlayOutScoreboardTeam.a(scoreboardTeam, playerToAddOrRemove, PacketPlayOutScoreboardTeam.a.b);

				}

				case UPDATE -> packet = PacketPlayOutScoreboardTeam.a(scoreboardTeam, false);

				case REMOVE -> packet = PacketPlayOutScoreboardTeam.a(scoreboardTeam);

			}

			return packet;

		}

		public Iterable<PacketPlayOutScoreboardTeam> sendLine(final Player player) {

			final List<PacketPlayOutScoreboardTeam> packets = new ArrayList<>();

			if(this.first)

				packets.add(createPacket(player, TeamMode.CREATE, null));

			else if(this.prefixChanged || this.suffixChanged)

				packets.add(createPacket(player, TeamMode.UPDATE, null));

			if(this.first || this.playerChanged) {

				if(this.oldPlayer != null)

					packets.add(createPacket(player, TeamMode.REMOVE_PLAYER, this.oldPlayer));

				packets.add(createPacket(player, TeamMode.ADD_PLAYER, this.currentPlayer));

			}

			if(this.first)

				this.first = false;

			return packets;

		}

		public String getName() {
			return name;
		}

		public String getPrefix() {
			return prefix;
		}

		public void setPrefix(String prefix) {

			if (this.prefix == null || !this.prefix.equals(prefix))

				this.prefixChanged = true;

			this.prefix = prefix;

		}

		public String getSuffix() {
			return suffix;
		}

		public void setSuffix(String suffix) {

			if(this.suffix == null || !this.suffix.equals(suffix))

				this.suffixChanged = true;

			this.suffix = suffix;

		}

		public void setPlayer(final String player) {

			if (this.currentPlayer == null || !this.currentPlayer.equals(player))

				this.playerChanged = true;

			this.oldPlayer = this.currentPlayer;
			this.currentPlayer = player;

		}

		public String getCurrentPlayer() {
			return currentPlayer;
		}

		public String getValue() {

			return getPrefix() + getCurrentPlayer() + getSuffix();

		}

		public void setValue(@NotNull String value) {

			if(value.length() <= 16) {

				this.setPrefix("");
				this.setPlayer(value);
				this.setSuffix("");

			}else if(value.length() <= 32) {

				this.setPrefix(value.substring(0, 16));
				this.setPlayer(value.substring(16));
				this.setSuffix("");

			}else if(value.length() <= 48) {

				this.setPrefix(value.substring(0, 16));
				this.setPlayer(value.substring(16, 32));
				this.setSuffix(value.substring(48));

			}else

				throw new IllegalArgumentException("Value is too long. Please set a value with 48 characters max.");

		}
	}
	
	public void setPattern(@NotNull ScoreboardPattern pattern) {
		this.pattern = pattern;
	}
	
	public ScoreboardPattern getPattern() {
		return pattern;
	}
	
	public static NMSScoreboard getScoreboard(final Player player) {
		
		if(scoreboards.containsKey(player))
			
			return scoreboards.get(player);
		
		return null;
		
	}
	
	/**
	 * All the actions available for the {@link PacketPlayOutScoreboardScore} packet
	 * @author Frivec
	 *
	 */
	private enum ScoreAction {
		
		ADD_OR_UPDATE(ScoreboardServer.Action.a),
		REMOVE(ScoreboardServer.Action.b);
		
		private final Action action;
		
		ScoreAction(final Action action) {
			
			this.action = action;
			
		}
		
		public Action getAction() {
			return action;
		}
		
	}
	
	/**
	 * All the mode available for the {@link PacketPlayOutScoreboardObjective} packet
	 * @author Frivec
	 *
	 */
	private enum ObjectiveMode {
		
		CREATE_OBJECTIVE(0),
		REMOVE_OBJECTIVE(1),
		UPDATE_OBJECTIVE(2);
		
		private final int mode;
		
		ObjectiveMode(final int mode) {
			
			this.mode = mode;
			
		}
		
		public int getMode() {
			return mode;
		}
		
	}

	private enum TeamMode {

		ADD_PLAYER,
		CREATE,
		REMOVE,
		REMOVE_PLAYER,
		UPDATE


	}

}
