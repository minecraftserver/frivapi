package fr.frivec.api.spigot.scoreboard;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.spigot.SpigotAPI;

public class ScoreboardAlternate extends BukkitRunnable {
	
	private Map<Integer, ScoreboardPattern> orderList;
	private Set<Player> playerList;
	
//	private int currentID = 0;
	
	/**
	 * Set an alternation between a few scoreboards
	 * The scoreboards will be saw by the players added in the list while they are online
	 * 
	 * @param scoreboards: list of the scoreboards you want to alternate
	 * @param order: order of apparition of these scoreboards
	 * @param delay: by default, set 0
	 * @param period: Period in ticks between two lap (20 for one second)
	 *
	 */
	public ScoreboardAlternate(final ScoreboardPattern[] scoreboards, final int[] order, final long delay, final long period) {
		
		this.orderList = new HashMap<>();
		this.playerList = new HashSet<>();
		
		if(scoreboards.length != order.length) {
			
			SpigotAPI.log(LogLevel.WARNING, "Error. The scoreboard task can't be applied because the lenghts of the scoreboard list and order ist aren't the same.");
			
			return;
			
		}
		
		for(int i = 0; i < scoreboards.length; i++)
			
			this.orderList.put(order[i] - 1, scoreboards[i]);
		
		this.runTaskTimer(SpigotAPI.getInstance(), delay, period);
		
	}
	
	/**
	 * Add a player to the list of viewers of the scoreboards
	 * @param player: Player you want to add
	 */
	public void addPlayer(final Player player) {
		
		this.playerList.add(player);
		
	}
	
	/**
	 * Remove a player from the list of viewers of the scoreboards
	 * @param player: Player you want to remove
	 */
	public void removePlayer(final Player player) {
		
		this.playerList.remove(player);
		
	}
	
	@Override
	public void run() {
//		
//		final ScoreboardPattern pattern = this.orderList.get(currentID);
//		
//		for(Player player : this.playerList) {
//			
//			if(player.isOnline()) {
//				
//				ServerScoreboard scoreboard = ServerScoreboard.getScoreboard(player);
//				
//				if(scoreboard == null)
//					
//					new ServerScoreboard(player, pattern).set();
//				
//				else {
//					
//					scoreboard.setPattern(pattern);
//					scoreboard.refreshScoreboard();
//					
//				}
//				
//			}
//			
//		}
//		
//		currentID++;
//		
//		if(currentID > this.orderList.size() - 1)
//			
//			this.currentID = 0;
		
	}

}
