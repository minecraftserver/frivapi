package fr.frivec.api.spigot.redis;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.message.PrivateMessage;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.messages.SpectreReloadMessage;
import fr.frivec.core.redis.pubsub.PubSubListener;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpigotRedisListener extends PubSubListener {
	
	private final GsonManager gsonManager;
	private final ServerInformations informations;
	
	public SpigotRedisListener() {
		
		this.gsonManager = GsonManager.getInstance();
		this.informations = SpigotAPI.getInstance().getInformations();
		
	}
	
	@Override
	public void onMessage(CharSequence channel, String msg) {
		
		if(channel.equals("Spectre")) {

			final SpectreMessage message = (SpectreMessage) this.gsonManager.deSeralizeJson(msg, SpectreMessage.class);
			final SpectreMessageType messageType = message.getType();

			//Stop the server
			if(messageType.equals(SpectreMessageType.STOP_SERVER)) {
				
				if(!message.getId().toString().equals(this.informations.getId().toString()))

					return;
				
				SpigotAPI.getInstance().stopServer();
				
			}else if(messageType.equals(SpectreMessageType.ERROR)) {

				//TODO manage errors

			}

		}else if(channel.equals("SpectreReload")) {
			
			/*
			 * Update informations (motd, maxSize) with new informations from Spectre
			 */
			
			final ServerInformations infos = SpigotAPI.getInstance().getInformations();
			final ServerType serverType = infos.getServerType();
			final SpectreReloadMessage message = (SpectreReloadMessage) GsonManager.getInstance().deSeralizeJson(msg, SpectreReloadMessage.class);
		
			infos.setMaxSize(message.getMaxSize().get(serverType));
			infos.setMotd(message.getMotd().get(serverType));
			
			SpigotAPI.getInstance().setInformations(infos);
			
			final Path root = Paths.get("").toAbsolutePath(), informationsFile = Paths.get(root + File.separator + "spectreInformations.json");
			
			try {
			
				final BufferedWriter writer = Files.newBufferedWriter(informationsFile);
				
				writer.write(GsonManager.getInstance().serializeObject(SpigotAPI.getInstance().getInformations()));
				writer.flush();
				writer.close();
			
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}else if(channel.equals("PrivateMessage")) {
			
			final PrivateMessage message = (PrivateMessage) this.gsonManager.deSeralizeJson(msg, PrivateMessage.class);
			final ServerInformations serverInfo = SpigotAPI.getInstance().getInformations();
			
			if(message.getProcessUUID().equals(serverInfo.getId())) {
				
				final Player player = Bukkit.getPlayer(message.getReceiver());
				
				final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(message.getReceiver()),
									localSender = LocalPlayer.getLocalPlayer(message.getSender());

				assert player != null;
				assert localPlayer != null;
				assert localSender != null;

				final RankList rank = localSender.getRank();

				localPlayer.setPreviousPrivateMessage(localSender.getUuid());
				localPlayer.sendInRedis();
				
				final Component privateMessage = Component.text("§a§o" + rank.getPrefix() + (rank.equals(RankList.PLAYER) ? "" : " ") + localSender.getName() + " §3§ovous a envoyé : §a" + message.getMessage())
						.clickEvent(ClickEvent.suggestCommand("/w " + localSender.getName() + " "));
				
				player.sendMessage(privateMessage);

			}
			
		}
		
	}

}
