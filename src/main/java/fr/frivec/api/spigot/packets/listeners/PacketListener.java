package fr.frivec.api.spigot.packets.listeners;

import org.bukkit.entity.Player;

import net.minecraft.network.protocol.Packet;

public interface PacketListener {
	
	public abstract void onPacketReceived(final Player player, final Packet<?> packet) throws Exception;
	
	public abstract void onPacketSent(final Player player, final Packet<?> packet) throws Exception;

}
