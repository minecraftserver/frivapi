package fr.frivec.api.spigot.packets.manager;

import fr.frivec.api.spigot.packets.listeners.PacketAdapter;
import fr.frivec.api.spigot.packets.listeners.PacketListener;
import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import org.bukkit.craftbukkit.v1_18_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PacketsManager {
	
	private final HashMap<Player, PacketAdapter> packetAdapters;
	
	public PacketsManager() {
		
		this.packetAdapters = new HashMap<>();
		
	}
	
	public void registerPlayer(final Player player, final PacketListener listener) {
		
		if(!this.packetAdapters.containsKey(player)) {
		
//			final ChannelPipeline channelPipeline = ((CraftPlayer) player).getHandle().b.a.m.pipeline();
			final ChannelPipeline channelPipeline = ((CraftPlayer) player).getHandle().b.a.m.pipeline();
			final PacketAdapter adapter = new PacketAdapter(player);
			
			channelPipeline.addBefore("packet_handler", player.getUniqueId().toString(), adapter);
			this.packetAdapters.put(player, adapter);
			
		}
		
		this.packetAdapters.get(player).getListeners().add(listener);
		
	}
	
	public void removePlayer(final Player player) {
		
//        final Channel channel = ((CraftPlayer) player).getHandle().b.a.m;
		final Channel channel = ((CraftPlayer) player).getHandle().b.a.m;

        channel.eventLoop().submit(() -> {
        	
            channel.pipeline().remove(player.getUniqueId().toString());
            
            return null;
            
        });
        
        this.packetAdapters.get(player).getListeners().clear();
        this.packetAdapters.remove(player);
		
	}

}
