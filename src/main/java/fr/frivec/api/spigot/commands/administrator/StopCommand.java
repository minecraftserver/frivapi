package fr.frivec.api.spigot.commands.administrator;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

import static fr.frivec.api.core.string.StringUtils.text;

public class StopCommand extends AbstractCommand {
	
	private SpigotAPI api;
	
	public StopCommand() {
		
		super("stop", "/stop (delete)", "Stop the current server", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));
	
		this.api = SpigotAPI.getInstance();
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			if(LocalPlayer.getLocalPlayer(((Player) sender).getUniqueId()).getRank().getPower() < RankList.DEV.getPower()) {
				
				sender.sendMessage(text(this.permMessage));
				return true;
				
			}
			
		}
			
		//TODO Add option to delete server when closed.
			
		sender.sendMessage(text("§aL'arrêt du serveur est bien programmée pour dans 5 secondes."));
		sender.sendMessage(text("§3Redirection vers un serveur en ligne..."));

		for(Player player : Bukkit.getOnlinePlayers()) {

			player.sendMessage(text("§4§m----------------------"));
			player.sendMessage(text("§cLe serveur " + this.api.getInformations().getName() + " a été arrêté par un administrateur."));
			player.sendMessage(text("§cVous avez été redirigé vers un hub disponible."));
			player.sendMessage(text("§4§m----------------------"));

		}
			
		this.api.stopServer();
		
		return false;
	}

}
