package fr.frivec.api.spigot.commands.player.privatemessage;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.message.PrivateMessage;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static fr.frivec.api.core.string.StringUtils.text;

public class PMCommand extends AbstractCommand {
	
	public PMCommand() {
		
		super("msg", "/msg <joueur> <message>", "A command to send a private message to a player", "", Arrays.asList("w", "whisper", "message"));
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer == null) {
				
				final ArrayList<String> message = new ArrayList<>();
				
				message.add("§c4Erreur.");
				message.add("§4Nous ne parvenons pas à retrouver vos données dans notre base de donnée.");
				message.add("");
				message.add("§bCela ne vient pas de vous. Veuillez vous reconnecter.");
				
				player.kick(text(String.join("\n", message)));
				
				return true;
				
			}else if(localPlayer.isMute()) {
				
				player.sendMessage(text("§cVous êtes mute. Vous ne pouvez pas discuter dans le chat global ni en messages privés."));
				player.sendMessage(text("§cVous pouvez toujours recevoir des messages privés, mais vous ne pourrez pas en envoyer."));
				
				return true;
				
			}
			
			if(args.length >= 2) {
				
				final UUID targetUUID = MojangRequest.getUUIDFromMojang(args[0]);
				
				if(args[0].equals(player.getName())) {
					
					player.sendMessage(text("§cErreur. Vous ne pouvez pas vous envoyer de message à vous-même."));
					
					return true;
					
					
				}else if(targetUUID == null) {
					
					player.sendMessage(text("§cErreur. Nous ne parvenons pas à trouver le joueur que vous souhaitez contacter."));
					
					return true;
					
				}
				
				final LocalPlayer localTarget = LocalPlayer.getLocalPlayer(targetUUID);
				
				if(localTarget != null) {
					
					if(localTarget.isOnline()) {
						
						/*
						 * Check if the target player enabled the private messages
						 */
						
						if(!localTarget.isPrivateMessages()) {
							
							player.sendMessage(text("§cLe joueur que vous souhaitez contacter a désactivé ses messages privés."));
							
							return true;
							
						}
						
						/*
						 * Getting the message
						 */
						
						final StringBuilder builder = new StringBuilder();
						
						for(int i = 1; i < args.length; i++)
							
							builder.append(args[i] + (i == args.length ? "" : " "));
						
						/*
						 * Send the message the target's current server
						 */
						
						final PrivateMessage message = new PrivateMessage(player.getUniqueId(), targetUUID, localTarget.getCurrentServer(), builder.toString());
						
						SpigotAPI.getInstance().getChannelManager().publish("PrivateMessage", SpigotAPI.getInstance().getGsonManager().serializeObject(message));
						
						/*
						 * Updating last message target in LocalPlayer
						 */
						
						localPlayer.setPreviousPrivateMessage(targetUUID);
						localPlayer.sendInRedis();
						
						/*
						 * Send message to the player
						 */
						
						final Component recapMessage = Component.text("§3§oVous avez envoyé à " + localTarget.getRank().getPrefix() + " " + localTarget.getName() + "§3§o: §7" + builder.toString())
																.clickEvent(ClickEvent.suggestCommand("/w " + localTarget.getName() + " "));
						
						player.sendMessage(recapMessage);
						
						return true;
						
					}else {
						
						player.sendMessage(text("§cLe joueur que vous souhaitez contacter n'est pas connecté sur le serveur !"));
						
						return true;
						
					}
					
				}else {
					
					player.sendMessage(text("§cErreur. Nous ne parvenons pas à trouver le joueur que vous souhaitez contacter."));
					
					return true;
					
				}
				
			}else {
				
				player.sendMessage(text("§cErreur. Il semble que la commande soit incomplète."));
				player.sendMessage(text("§bUtilisation: " + this.usage));
				
				return true;
				
			}
			
		}else {
			
			sender.sendMessage(text("§cErreur. Veuillez utiliser un compte joueur pour utiliser cette commande."));
			
			return true;
			
		}
		
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(args.length == 1) {
			
			final List<String> names = new ArrayList<>();
			
			if(!args[0].isEmpty()) {
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					if(player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
						
						names.add(player.getName());
			
			}else
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					names.add(player.getName());
			
			return names;
			
		}
		
		return null;
		
	}

}
