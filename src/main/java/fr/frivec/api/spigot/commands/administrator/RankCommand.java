package fr.frivec.api.spigot.commands.administrator;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.commands.AbstractCommand;

public class RankCommand extends AbstractCommand {

	public RankCommand() {
		super("rank", "/rank <player> <rank>", "Set a new rank to a player by using its power or its name", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList("ranks"));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
		
			if(LocalPlayer.getLocalPlayer(((Player) sender).getUniqueId()).getRank().getPower() < RankList.ADMIN.getPower()) {
				
				sender.sendMessage(text(this.permMessage));
				
				return true;
				
			}
		
		}
		
		if(args.length > 0) {
			
			if(args.length == 1) {
				
				sender.sendMessage("§cErreur. Il manque le grade que vous voulez attribuer à ce joueur.");
				sender.sendMessage(this.usage);
				
				return true;
				
			}else if(args.length >= 2) {
				
				final String playerName = args[0],
							rankID = args[1];
				
				final Player target = Bukkit.getPlayer(playerName);
				final UUID targetUUID = MojangRequest.getUUIDFromMojang(playerName);
				
				final LocalPlayer localTarget = LocalPlayer.getLocalPlayer(targetUUID);
				
				RankList rank = RankList.PLAYER;
				
				if(rankID instanceof String)
					
					rank = RankList.getRank(rankID);
				
				else
					
					rank = RankList.getRank(Integer.valueOf(rankID));
				
				if(rank == null) {
					
					sender.sendMessage(text("§cUne erreur est survenue. Le grade que vous essayez d'appliquer n'a pas été trouvé."));
					sender.sendMessage(text("§bUtilisation: " + this.usage));
					
					return true;
					
				}else if(localTarget == null) {
					
					sender.sendMessage(text("§cIl semblerait que le joueur que vous semblez vouloir modifier n'existe pas ou ne s'est jamais connecté sur le serveur."));
					
					return true;
					
				}else if(localTarget.getRank().equals(rank)) {
					
					sender.sendMessage(text("§cErreur. Ce joueur possède déjà ce grade."));
					
					return true;
					
				}
				
				localTarget.setRank(rank);
				localTarget.sendInRedis();
				
				if(target != null) {
					
					target.sendMessage(text("§aVotre grade a été mis à jour. Vous êtes à présent: " + rank.getName() + "."));
					target.sendMessage(text("§bCertaines fonctionnalités n'ont pas encore été mises à jour, comme votre nom ou vos permissions."));
					target.sendMessage(text("§bToutes les mises à jour seront effectives lors de votre prochaine connexion sur le serveur !"));
				
				}else
					
					localTarget.save(true);
				
				sender.sendMessage(text("§aAction effectuée. Le grade du joueur " + playerName + " a bien été mis à jour vers " + rank.getPrefix() + "§a."));
				
				return true;
				
			}
			
		}else {
			
			sender.sendMessage(text("§cErreur. La commande est incomplète."));
			sender.sendMessage(text("§bUtilisation: " + this.usage));
			
			return true;
			
		}
		
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player)
			
			if(LocalPlayer.getLocalPlayer(((Player) sender).getUniqueId()).getRank().getPower() < RankList.ADMIN.getPower())
				
				return null;
		
		if(args.length == 1) {
			
			final List<String> names = new ArrayList<>();
			
			if(!args[0].isEmpty()) {
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					if(player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
						
						names.add(player.getName());
			
			}else
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					names.add(player.getName());
			
			return names;
			
		}else if(args.length == 2) {
			
			final List<String> ranksNames = new ArrayList<>();
		
			for(RankList ranks : RankList.values())
				
				ranksNames.add(ranks.getName());
			
			return ranksNames;
			
		}
		
		return super.onTabComplete(sender, cmd, label, args);
	}

}
