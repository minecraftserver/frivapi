package fr.frivec.api.spigot.commands.dev;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.Arrays;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.commands.AbstractCommand;

public class WorldChangeCommand extends AbstractCommand {

	public WorldChangeCommand() {
		super("world", "/world <worldName>", "Téléportation sur un autre monde.", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() >= RankList.DEV.getPower()) {
				
				if(args.length > 0) {
					
					final String worldName = args[0];
					final World world = new WorldCreator(worldName).createWorld();
					
					if(world != null) {
						
						player.teleport(new Location(world, 0, 120, 0));
						player.sendMessage(text("§aVous avez bien été téléporté en 0 0 du monde §6" + worldName + "§a."));
						return true;
					
					}else {
						
						player.sendMessage(text("§cErreur. Le monde que vous recherchez n'a pas été trouvé sur ce serveur."));
						return true;
						
					}
					
				}else {
					
					sender.sendMessage(text("§cErreur. La commande est incomplète."));
					sender.sendMessage(text("§bUtilisation: " + this.usage));
					return true;
					
				}
				
			}else {
				
				player.sendMessage(text(this.permMessage));
				return true;
				
			}
			
		}else {
			
			sender.sendMessage(text("§cSérieusement ?"));
			return true;
			
		}
		
	}
	
}
