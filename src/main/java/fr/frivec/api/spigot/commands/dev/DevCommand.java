package fr.frivec.api.spigot.commands.dev;

import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.message.PrivateMessage;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.string.StringUtils;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.api.spigot.effects.worldborder.RedScreenEffect;
import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.api.spigot.tags.server.ServerSideNameTag;
import fr.frivec.core.json.GsonManager;
import net.kyori.adventure.text.format.NamedTextColor;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.network.syncher.DataWatcherObject;
import net.minecraft.network.syncher.DataWatcherRegistry;
import net.minecraft.server.level.EntityPlayer;
import org.bukkit.*;
import org.bukkit.Particle.DustOptions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_18_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Objects;

public class DevCommand extends AbstractCommand {
	
	private final List<String> args = List.of("test");
	
	public DevCommand() {
		
		super("dev", "/dev", "dev command", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", List.of("developper"));
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof final Player player) {

			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());

			assert localPlayer != null;

			if(localPlayer.getRank().getPower() >= RankList.DEV.getPower()) {

				player.sendMessage("args 0: " + args[0]);

				if (args[0].equalsIgnoreCase("ttt")) {

					player.sendMessage("test");

					if (args[1].equalsIgnoreCase("on")) {

						player.sendMessage("on");

						SpigotAPI.getInstance().getGlowEffect().setGlowColor(player, true);
						SpigotAPI.getInstance().getGlowEffect().addViewer(player, player);

						player.sendMessage("§aglow on");

					} else if (args[1].equalsIgnoreCase("off")) {

						SpigotAPI.getInstance().getGlowEffect().setGlowColor(player, false);

						player.sendMessage("§aglow off");

					}

					return true;

				}

				if (args[0].equalsIgnoreCase("playermessage")) {

					final PrivateMessage message = new PrivateMessage(player.getUniqueId(), player.getUniqueId(), Objects.requireNonNull(LocalPlayer.getLocalPlayer(player.getUniqueId())).getCurrentProxy(), "Test message from spigot.");

					SpigotAPI.getInstance().getChannelManager().publish("PlayerMessaging", GsonManager.getInstance().serializeObject(message));

					player.sendMessage(StringUtils.text("§aMessage envoyé"));

					return true;

				}

				if (args[0].equalsIgnoreCase("tag"))

					ServerSideNameTag.setNameTag(player, "9A", NamedTextColor.BLUE, "§aTest ", " §bMeh");

				else if (args[0].equalsIgnoreCase("redscreen")) {

					if (args[1].equalsIgnoreCase("on"))

						RedScreenEffect.redScreenEffect(player, true);

					else if (args[1].equalsIgnoreCase("off"))

						RedScreenEffect.redScreenEffect(player, false);

//				else if(args[0].equalsIgnoreCase("teams")) {

//					if(args[1].equalsIgnoreCase("join")) {
//						
//						if(args[2].equalsIgnoreCase("red")) {
//							
//							final NMSTeam team = NMSTeam.getTeams().get(Color.RED);
//							
//							team.createPacket(player, false, false, true, false, null);
//							
//							player.sendMessage("§aTu as rejoint les rouges c'est bon.");
//							
//							return true;
//							
//						}else if(args[2].equalsIgnoreCase("blue")) {
//							
//							final NMSTeam team = NMSTeam.getTeams().get(Color.CYAN);
//							
//							team.createPacket(player, false, false, true, false, null);
//							
//							player.sendMessage("§aTu as rejoint les bleus c'est bon.");
//							
//						}
//						
//					}else if(args[1].equalsIgnoreCase("remove")) {
//						
//						if(args[2].equalsIgnoreCase("red")) {
//							
//							final NMSTeam team = NMSTeam.getTeams().get(Color.RED);
//							
//							team.createPacket(player, false, false, false, true, null);
//							
//							player.sendMessage("§aTu as quitté les rouges c'est bon.");
//							
//							return true;
//							
//						}else if(args[2].equalsIgnoreCase("blue")) {
//							
//							final NMSTeam team = NMSTeam.getTeams().get(Color.CYAN);
//							
//							team.createPacket(player, false, false, false, true, null);
//							
//							player.sendMessage("§aTu as quitté les bleus c'est bon.");
//							
//						}
//						
//					}

				}else if(args[0].equalsIgnoreCase("jump")) {
					
					player.sendMessage("§aSaut");
					player.getInventory().setChestplate(new ItemCreator(Material.ELYTRA, 1).build());
					player.setVelocity(new Vector(0, 5, 0));
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(SpigotAPI.getInstance(), () -> {

						enableGliding(player);
						player.sendMessage("§aEnvol");

					}, 30L);
					
				}else if(args[0].equalsIgnoreCase("animation")) {
					
					Bukkit.getScheduler().runTaskTimer(SpigotAPI.getInstance(), () -> {

						final Location location = player.getLocation().clone().add(0, 1, 0);
						final World world = location.getWorld();
						final float yaw = location.getYaw();
						final double radYaw = Math.toRadians(yaw), sin = Math.sin(radYaw), cos = Math.cos(radYaw);

						final Location center = new Location(world, location.getX() + sin * 0.8, location.getY(), location.getZ() - cos);
						final double z = w(center, radYaw);

//							API.log(LogLevel.INFO, "Center: " + center.getX() + " - " + center.getZ() + " | w: " + w[0] + " - " + w[1]);

						final Location p1 = new Location(world, center.getX(), center.getY() + 0.3, center.getZ() - z);

						SpigotAPI.log(LogLevel.INFO, "p1: " + p1);

						p1.getWorld().spawnParticle(Particle.REDSTONE, p1.getX(), p1.getY(), p1.getZ(), 10, 0, 0, 0, 1, new DustOptions(Color.AQUA, 1));

					}, 0, 1L);
					
				}
				
			}else {
				
				player.sendMessage(this.permMessage);
				return true;
				
			}
			
		}
		
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		for(final String names : this.alias)
			
			if(cmd.getName().equalsIgnoreCase(names) || cmd.getName().equalsIgnoreCase(this.command))
				
				if(sender instanceof Player)
					
					return this.args;
		
		return super.onTabComplete(sender, cmd, label, args);
	}
	
	private void enableGliding(final Player player) {
		
    	final EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
    	final DataWatcherObject<Byte> object = new DataWatcherObject<>(0, DataWatcherRegistry.a);
    	final DataWatcher current = entityPlayer.ai(),
    						dataWatcher = new DataWatcher(entityPlayer);
    	
    	final byte initialBitMask = current.a(object),
    			bitMask = (byte) 0x80,
    			newByte = (byte) (initialBitMask | bitMask);
    	
    	dataWatcher.a(object, newByte);

        final PacketPlayOutEntityMetadata entityMetaData = new PacketPlayOutEntityMetadata(entityPlayer.ae(), dataWatcher, true);
        
        for(final Player players : Bukkit.getOnlinePlayers())
                
           ((CraftPlayer) players).getHandle().b.a(entityMetaData);
        
        player.setGliding(true);
		
	}
	
	private double w(final Location center, final double radians) {
		
		final double cos = Math.cos(radians) * 0.8, sin = Math.sin(radians) * 0.8;
		final double a = cos / sin;
		
		return a*center.getX();
		
	}

}
