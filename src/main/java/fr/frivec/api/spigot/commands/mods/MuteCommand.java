package fr.frivec.api.spigot.commands.mods;

import static fr.frivec.api.core.string.StringUtils.text;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.messaging.SanctionMessage;
import fr.frivec.api.core.sanction.messaging.SanctionMessage.SanctionMessageType;
import fr.frivec.api.core.sanction.utils.DateUtils;
import fr.frivec.api.core.sanction.utils.TimeUnit;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.core.json.GsonManager;
import net.kyori.adventure.text.Component;

public class MuteCommand extends AbstractCommand {

	public MuteCommand() {
		
		super("mute", "§c/mute <pseudonyme> <durée> <unité> <raison>", "Mute command to mute a player temporally or permanently", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));
		
	}
	
	//ban pseudo duree unite raison | ban pseudo permanent raison
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			final Date start = Date.from(Instant.now());
			
			if(localPlayer.getRank().getPower() == RankList.MOD.getPower() || localPlayer.getRank().getPower() == RankList.ADMIN.getPower()) {
				
				if(args.length == 0 || args.length < 2) {
					
					sendArgsMessage(player);
				
					return true;
					
				}else {
					
					final String targetName = args[0],
								time = args[1];
					
					final Player targetPlayer = Bukkit.getPlayer(targetName);
					
					final UUID targetUUID = targetPlayer != null ? targetPlayer.getUniqueId() : MojangRequest.getUUIDFromMojang(targetName);
					
					if(targetUUID == null) {
						
						player.sendMessage(text("§4Erreur. Impossible de trouver l'UUID du joueur spécifié dans la commande. Merci de réessayer."));
						
						return true;
						
					}
					
					if(isInteger(time)) {
						
						//time is an integer, so not permanent
						
						final int timeInteger = Integer.valueOf(time);
						
						if(args.length < 3) {
							
							player.sendMessage(text("§4Erreur. Merci d'indiquer une unité de temps à la suite de la durée."));
							sendArgsMessage(player);
							
							return true;
							
						}else if(args.length < 4) {
							
							player.sendMessage(text("§4Erreur. La raison de la sanction est absente. Merci de la préciser."));
							sendArgsMessage(player);
							
							return true;
							
						}else {
							
							final String timeUnitString = args[2];
							final TimeUnit timeUnit = TimeUnit.getTimeUnit(timeUnitString);
							
							if(timeUnit == null) {
								
								player.sendMessage(text("§cErreur. L'unité de temps n'a pas été reconnue."));
								player.sendMessage(text("§aListe des unités disponibles : §bm (minute) §a| §bh (heure) §a| §bd (jour) §a| §bw (semaine) §a| §bMo (mois) §a| §by (année)§a."));
								
								return true;
								
							}
							
							final StringBuilder reason = new StringBuilder();
							
							for(int i = 3; i < args.length; i++)
								
								reason.append(args[i] + ((i + 1) == args.length ? "" : " "));
							
							final Mute mute = new Mute(player.getUniqueId(), targetUUID, reason.toString(), start.getTime(), DateUtils.addTime(timeInteger, timeUnit.getCalendarUnit()).getTime(), false);
							final SanctionMessage message = new SanctionMessage(SanctionMessageType.ADD_SANCTION, player.getUniqueId(), targetUUID, mute);
							
							SpigotAPI.getInstance().getChannelManager().publish("Sanction", GsonManager.getInstance().serializeObject(message));
							
							player.sendMessage(text("§aVotre demande a bien été envoyée. Veuillez patienter le temps de son traitement."));
							
							return true;
							
						}
						
					}else {
						
						//time is a string, not an integer
						
						if(time.equalsIgnoreCase("permanent")) {
							
							if(args.length < 3) {
								
								player.sendMessage(text("§4Erreur. La raison de la sanction est absente. Merci de la préciser."));
								sendArgsMessage(player);
								
								return true;
								
							}else {
								
								final StringBuilder reason = new StringBuilder();
								
								for(int i = 2; i < args.length; i++)
									
									reason.append(args[i] + ((i + 1) == args.length ? "" : " "));
								
								final Mute mute = new Mute(player.getUniqueId(), targetUUID, reason.toString(), start.getTime(), -1, true);
								final SanctionMessage message = new SanctionMessage(SanctionMessageType.ADD_SANCTION, player.getUniqueId(), targetUUID, mute);
								
								SpigotAPI.getInstance().getChannelManager().publish("Sanction", GsonManager.getInstance().serializeObject(message));
								
								player.sendMessage(text("§aVotre demande a bien été envoyée. Veuillez patienter durant son traitement."));
								
								return true;
								
							}
							
						}else {
							
							player.sendMessage(text("§4Erreur. Le deuxième argument a mal été compris. Veuillez réessayer."));
							
							sendArgsMessage(player);
							
							return true;
							
						}
						
					}
					
				}
				
			}else {
				
				player.sendMessage(Component.text(this.permMessage));
				return true;
				
			}
			
		}else {
			
			sender.sendMessage("§cErreur. Merci d'utiliser un compte joueur pour utiliser cette commande.");
			return true;
			
		}
		
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(((Player) sender).getUniqueId());
			
			if(localPlayer.getRank().getPower() != RankList.MOD.getPower() && localPlayer.getRank().getPower() != RankList.ADMIN.getPower())
				
				return null;
				
		}
		
		final List<String> reasons = Arrays.asList("Insultes", "Spoil", "Spam"),
							time = Arrays.asList("permanent", "1", "2", "3", "4", "5", "6", "7","8", "9"),
							units = Arrays.asList("y", "Mo", "w", "d", "h", "m");
		
		if(args.length == 1) {
			
			final List<String> names = new ArrayList<>();
			
			if(!args[0].isEmpty()) {
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					if(player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
						
						names.add(player.getName());
			
			}else
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					names.add(player.getName());
			
			return names;
			
		}else if(args.length == 2)
			
			return time;
		
		else if(args.length == 3)
			
			if(args[1].equalsIgnoreCase("permanent"))
				
				return reasons;
			
			else
				
				return units;
		
		else if(args.length == 4)
			
			if(!args[1].equalsIgnoreCase("permanent"))
				
				return reasons;
		
		return super.onTabComplete(sender, cmd, label, args);
	}
	
	private boolean isInteger(final String str) {
		
		try {
			
			Integer.valueOf(str);
			
			return true;
			
		} catch (NumberFormatException e) {
			
			return false;
			
		}
		
	}
	
	private void sendArgsMessage(final Player player) {
		
		player.sendMessage(text("§bExemples:"));
		player.sendMessage(text("§a/mute Frivec permanent Insultes (récidive)"));
		player.sendMessage(text("§a/ban Frivec 1 d Spoil film génial"));
		
	}

}
