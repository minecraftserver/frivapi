package fr.frivec.api.spigot.commands.mods;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.Arrays;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.messaging.SanctionMessage;
import fr.frivec.api.core.sanction.messaging.SanctionMessage.SanctionMessageType;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.core.json.GsonManager;

public class UnmuteCommand extends AbstractCommand {
	
	public UnmuteCommand() {
		
		super("unmute", "/unmute <Player>", "Unmute a player", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() == RankList.MOD.getPower() || localPlayer.getRank().getPower() == RankList.ADMIN.getPower()) {
				
				if(args.length == 0) {
					
					player.sendMessage(text("§cMerci d'indiquer le pseudonyme d'un joueur."));
					
					return true;
					
				}else {
					
					final String targetName = args[0];
					final UUID targetUUID = MojangRequest.getUUIDFromMojang(targetName);
					
					final SanctionMessage message = new SanctionMessage(SanctionMessageType.REMOVE_SANCTION, player.getUniqueId(), targetUUID, new Mute(player.getUniqueId(), targetUUID, "", 0, 0, false));
					
					SpigotAPI.getInstance().getChannelManager().publish("Sanction", GsonManager.getInstance().serializeObject(message));
					
					player.sendMessage(text("§aVotre demande a bien été envoyée. Veuillez patienter durant son traitement."));
					
					return true;
					
				}
				
			}else {
				
				player.sendMessage(text(this.permMessage));
				
				return true;
				
			}
			
		}else {
			
			sender.sendMessage("§cErreur. Merci d'utiliser un compte joueur pour utiliser cette commande.");
			
			return true;
			
		}
		
	}
	
}
