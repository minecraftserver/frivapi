package fr.frivec.api.spigot.commands.mods;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.sanction.Kick;
import fr.frivec.api.core.sanction.messaging.SanctionMessage;
import fr.frivec.api.core.sanction.messaging.SanctionMessage.SanctionMessageType;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.core.json.GsonManager;
import net.kyori.adventure.text.Component;

public class KickCommand extends AbstractCommand {

	public KickCommand() {
		
		super("kick", "§c/kick <pseudonyme> <raison>", "Kick command to kick a player temporally or permanently", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));
		
	}
	
	//ban pseudo duree unite raison | ban pseudo permanent raison
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() == RankList.MOD.getPower() || localPlayer.getRank().getPower() == RankList.ADMIN.getPower()) {
				
				if(args.length == 0 || args.length < 2) {
					
					sendArgsMessage(player);
				
					return true;
					
				}else {
					
					final String targetName = args[0];
					
					final Player targetPlayer = Bukkit.getPlayer(targetName);
					
					final UUID targetUUID = targetPlayer != null ? targetPlayer.getUniqueId() : MojangRequest.getUUIDFromMojang(targetName);
					
					if(targetUUID == null) {
						
						player.sendMessage(text("§4Erreur. Impossible de trouver l'UUID du joueur spécifié dans la commande. Merci de réessayer."));
						
						return true;
						
					}
							
					if(args.length < 2) {
								
						player.sendMessage(text("§4Erreur. La raison de la sanction est absente. Merci de la préciser."));
						sendArgsMessage(player);
								
						return true;
								
					}else {
								
						final StringBuilder reason = new StringBuilder();
								
						for(int i = 1; i < args.length; i++)
									
							reason.append(args[i] + ((i + 1) == args.length ? "" : " "));
								
						final Kick kick = new Kick(player.getUniqueId(), targetUUID, reason.toString());
						final SanctionMessage message = new SanctionMessage(SanctionMessageType.ADD_SANCTION, player.getUniqueId(), targetUUID, kick);
						
						SpigotAPI.getInstance().getChannelManager().publish("Sanction", GsonManager.getInstance().serializeObject(message));
								
						player.sendMessage(text("§aVotre demande a bien été envoyée. Veuillez patienter durant son traitement."));
						
						return true;
						
					}
					
				}
				
			}else {
				
				player.sendMessage(Component.text(this.permMessage));
				return true;
				
			}
			
		}else {
			
			sender.sendMessage("§cErreur. Merci d'utiliser un compte joueur pour utiliser cette commande.");
			return true;
			
		}
		
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(((Player) sender).getUniqueId());
			
			if(localPlayer.getRank().getPower() != RankList.MOD.getPower() && localPlayer.getRank().getPower() != RankList.ADMIN.getPower())
				
				return null;
				
		}
		
		final List<String> reasons = Arrays.asList("Team en jeu solo");
		
		if(args.length == 1) {
			
			final List<String> names = new ArrayList<>();
			
			if(!args[0].isEmpty()) {
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					if(player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
						
						names.add(player.getName());
			
			}else
				
				for(Player player : Bukkit.getOnlinePlayers())
					
					names.add(player.getName());
			
			return names;
			
		}else if(args.length == 2)
			
			return reasons;
		
		return super.onTabComplete(sender, cmd, label, args);
	}
	
	private void sendArgsMessage(final Player player) {
		
		player.sendMessage(text("§bExemples:"));
		player.sendMessage(text("§a/ban Frivec permanent Forcefield"));
		player.sendMessage(text("§a/ban Frivec 1 d Antijeu"));
		
	}

}
