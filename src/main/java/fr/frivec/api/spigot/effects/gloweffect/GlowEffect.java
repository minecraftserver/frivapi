package fr.frivec.api.spigot.effects.gloweffect;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import fr.frivec.api.spigot.teams.client.NMSTeam;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.api.spigot.teams.TeamManager;
import net.minecraft.EnumChatFormat;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.server.level.EntityPlayer;

public class GlowEffect {
	
	private final TeamManager teamManager;
	private final Map<Player, GlowListener> listeners;
	
	public GlowEffect(final TeamManager teamManager) {
		
		this.listeners = new HashMap<Player, GlowListener>();
		this.teamManager = teamManager;
		
	}
	
	/**
	 * Enable or disable the glowing effect for a player
	 * 
	 * @param player: The player you want to be glowing/stop to glow
	 * @param active: Set the activation of the glowing. True to enable, false to disable
	 */
	public void setGlowColor(final Player player, final boolean active) {
            	
		/*	
		 * 
		 * Enable/Disable GlowListener
		 * 
		 */
		
		GlowListener listener;
		
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
		final NMSTeam playerTeam = this.teamManager.getNMSTeam(localPlayer);
		
		if(active) {
			
			listener = new GlowListener(player, playerTeam); //Create a new listener with the player and the NMSTeam to set the color of the glowing effect
			
			this.getListeners().put(player, listener);
			SpigotAPI.getInstance().getPacketsManager().registerPlayer(player, listener);
		
		}else {
			
			listener = this.listeners.get(player);
			
			//Remove viewers from the glow listener
			final Set<UUID> uuids = new HashSet<UUID>();
			
			for(final Player players : listener.getViewers())
				
				uuids.add(players.getUniqueId());
			
			removeViewers(player, uuids);
			listener.getViewers().clear();
			
			//Disable packet listener
			SpigotAPI.getInstance().getPacketsManager().removePlayer(player);
			this.listeners.remove(player);
			
		}
		
	}
	
	/*
	 * Add a player to the viewers list of the player's glowing effect
	 * 
	 */
	
	public void addViewerByNames(final Player player, final Collection<String> viewers) {
		
		for(final String viewer : viewers)
		
			addViewer(player, Bukkit.getPlayer(viewer));
		
	}
	
	public void addViewer(final Player player, final Collection<Player> viewers) {
		
		for(final Player viewer : viewers)
		
			addViewer(player, viewer);
		
	}
	
	public void addViewer(final Player player, final Player viewer) {
		
		final GlowListener listener = this.getListeners().get(player);
		
		if(viewer == null || player == null || listener == null)
			
			return;
		
		if(listener.getViewers().contains(viewer))
			
			return;
		
		listener.getViewers().add(viewer);
		
		listener.getTeam().addPlayer(player, viewer);
		updateGlowing(player, viewer);
		
	}
	
	/*
	 * Methods to remove a viewer from a player's glowing effect
	 * 
	 */
	
	public void removeViewers(final Player player, final Collection<UUID> viewers) {
		
		for(final UUID uuids : viewers)
			
			removeViewer(player, Bukkit.getPlayer(uuids));
		
	}
	
	public void removeViewersByName(final Player player, final Collection<String> viewers) {
		
		for(final String names : viewers)
			
			removeViewer(player, Bukkit.getPlayer(names));
		
	}
	
	public void removeViewer(final Player player, final Player viewer) {
		
		final GlowListener listener = this.getListeners().get(player);
		
		if(!listener.getViewers().contains(viewer))
			
			return;
		
		listener.getViewers().remove(viewer);
		
		updateGlowing(player, viewer);
		
	}
	
	/**
	 * Send a packet to the viewer to update the state of glowing
	 * 
	 * @param player: The player which is glowing
	 * @param viewer: The player you want to update the glowing effect
	 */
	
	private void updateGlowing(final Player player, final Player viewer) {
		
		final EntityPlayer entityPlayer = PacketUtils.getEntityPlayer(player);
		final DataWatcher dataWatcher = entityPlayer.ai();
		
		PacketUtils.sendPacket(viewer, new PacketPlayOutEntityMetadata(PacketUtils.getEntityId(entityPlayer), dataWatcher, true));
		
	}
	
	public Map<Player, GlowListener> getListeners() {
		return listeners;
	}
	
	public enum EffectColor {
		
		BLACK("0", EnumChatFormat.a), //Black
		DARK_BLUE("1", EnumChatFormat.b), //Dark blue
		DARK_GREEN("2", EnumChatFormat.c), //Dark Green
		CYAN("3", EnumChatFormat.d), //Cyan
		DARK_RED("4", EnumChatFormat.e), //Dark Red
		PURPLE("5", EnumChatFormat.f), //Purple
		GOLD("6", EnumChatFormat.g), //Gold
		LIGHT_GRAY("7", EnumChatFormat.h), //Light gray
		GRAY("8", EnumChatFormat.i), //Gray
		BLUE("9", EnumChatFormat.j), //Blue
		LIME("a", EnumChatFormat.k), //Lime
		LIGHT_BLUE("b", EnumChatFormat.l), //Light Blue
		RED("c", EnumChatFormat.m), //Red
		PINK("d", EnumChatFormat.n), //Pink
		YELLOW("e", EnumChatFormat.o), //Yellow
		WHITE("f", EnumChatFormat.p), //White
		SCROLLING("k", EnumChatFormat.q), //Scrolling
		BOLD("l", EnumChatFormat.r), //Bold
		STRIKE("m", EnumChatFormat.s), //Strike
		UNDERLINE("n", EnumChatFormat.t), //Underline
		ITALIC("o", EnumChatFormat.u), //Italic
		RESET("r", EnumChatFormat.v); //Reset
		
		private String code;
		private EnumChatFormat chatFormat;
		
		private EffectColor(final String code, final EnumChatFormat chatFormat) {
			
			this.code = code;
			this.chatFormat = chatFormat;
			
		}
		
		public EnumChatFormat getChatFormat() {
			return chatFormat;
		}
		
		public String getCode() {
			return code;
		}
		
	}

}
