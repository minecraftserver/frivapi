package fr.frivec.api.spigot.effects.gloweffect;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.api.spigot.packets.listeners.PacketListener;
import fr.frivec.api.spigot.teams.client.NMSTeam;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.network.syncher.DataWatcherObject;
import net.minecraft.network.syncher.DataWatcherRegistry;
import net.minecraft.server.level.EntityPlayer;

public class GlowListener implements PacketListener {
	
	private Set<Player> viewers;
	
	private NMSTeam team;
	
	private byte bitMask = 1 << 6;
	
	private DataWatcherObject<Byte> object;
	private DataWatcher dataWatcher;
	
	private boolean firstPacket = true;
	
	public GlowListener(final Player player, final NMSTeam team) {
		
		this(player, new HashSet<>(), team);
		
	}
	
	public GlowListener(final Player player, final Set<Player> viewers, final NMSTeam team) {
		
		this.viewers = viewers;
		this.team = team;
		
		this.object = new DataWatcherObject<>(0, DataWatcherRegistry.a);
		this.dataWatcher = new DataWatcher(PacketUtils.getEntityPlayer(player));
		
	}
	
	@Override
	public void onPacketReceived(Player entity, Packet<?> packet) {/**/}
	
    @Override
    public void onPacketSent(final Player entity, final Packet<?> packet) throws IllegalAccessException {
		
        if(packet instanceof PacketPlayOutEntityMetadata && entity.getType().equals(EntityType.PLAYER)) {
        	        	
        	final Player player = entity;
        	final int id = (int) PacketUtils.readField(packet, "a");
        	
        	if(id < 0) //My Packet
        		
        		PacketUtils.setField(packet, "a", -id);
        		
        	else {//Modify the real packet
        	
	        	final EntityPlayer entityPlayer = PacketUtils.getEntityPlayer(player);
	        	final int entityId = PacketUtils.getEntityId(entityPlayer);
	        	final DataWatcher current = entityPlayer.ai(); //entityPlayer.ai = entityPlayer.getDataWatcher()
	        	
	        	final byte initialBitMask = current.a(this.object), //current.a = current.get
	        			newByte = (byte) (initialBitMask | this.bitMask);
	        		        	
	        	if(this.firstPacket) {
	        			            
	        		this.dataWatcher.a(this.object, newByte); //dataWatcher.register
	        		this.firstPacket = false;
	        		
	        	}else
		        		
	        		this.dataWatcher.b(this.object, newByte); //dataWatcher.set
	
	            final PacketPlayOutEntityMetadata entityMetaData = new PacketPlayOutEntityMetadata(-entityId, this.dataWatcher, true); //entityPlayer.at() = entityPlayer.getId()
	            
	            for(Player players : this.viewers)
	                    
	                PacketUtils.sendPacket(players, entityMetaData);
	            
        	}
            
        }
        
    }
	
	public Set<Player> getViewers() {
		return viewers;
	}
	
	public NMSTeam getTeam() {
		return team;
	}
	
	public void setTeam(NMSTeam team) {
		this.team = team;
	}

}
