package fr.frivec.api.spigot.effects.worldborder;

import net.minecraft.network.protocol.game.ClientboundInitializeBorderPacket;
import net.minecraft.world.level.border.WorldBorder;
import org.bukkit.entity.Player;

import static fr.frivec.api.spigot.packets.PacketUtils.getEntityPlayer;
import static fr.frivec.api.spigot.packets.PacketUtils.sendPacket;

public class RedScreenEffect {
	
	/**
	 * Add a red screen effect to a player
	 * 
	 * @param player: Player object
	 * @param enable: True if you want to activate the effect, False if you want to disable the effect
	 */
	
	public static void redScreenEffect(final Player player, final boolean enable) {
		
		final WorldBorder border = getEntityPlayer(player).s.p_(); //EntityPlayer#s : WorldServer | EntityPlayer#s#p_() : WorldBorder
		
		/*
		 * 
		 * Equality between 1.17 and 1.18
		 * 
		 * border.c(double) (1.18) = border.setDamageAmount(double) (1.17)
		 * border.b(double) (1.18) = border.setDamageBuffer(double) (1.17)
		 * border.c(int) (1.18) = border.setWarningDistance(int) (1.17)
		 * border.b(int) (1.18) = border.setWarningTime(int) (1.17)
		 * 
		 * border.c(double, double) (1.18) = border.setCenter(double, double) (1.17)
		 * 
		 * border.a(int) (1.18) = border.setSize(int) (1.17) 
		 * 
		 */
		
		border.c(0.0); //setDamageAmount
		border.b(0.0); //setDamageBuffer
		
		if(enable) {
			
			border.a(1); //setSize
			border.c(player.getLocation().getX() + 1000000.0, player.getLocation().getZ() + 1000000.0);
			
		}else {
			
			border.a(30000000);
			border.c(player.getLocation().getX(), player.getLocation().getZ());
			
		}
		
		sendPacket(player, new ClientboundInitializeBorderPacket(border));
		
	}

}
