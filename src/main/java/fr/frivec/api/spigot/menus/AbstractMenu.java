package fr.frivec.api.spigot.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;

import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.item.ItemCreator;
import net.kyori.adventure.text.Component;

public abstract class AbstractMenu implements Listener {
	
	/**
	 * @author Frivec
	 * Class inspired by Samagames Development Team work
	 * 
	 */
	
	protected Inventory inventory;
	protected String name;
	
	private NamespacedKey key;
	
	private JavaPlugin javaPlugin;
	
	/**
	 * 
	 * Create a new inventory
	 * 
	 * @param javaPlugin, the main class of the plugin
	 * @param title of the inventory
	 * @param number of slots
	 */
	
	public AbstractMenu(final JavaPlugin javaPlugin, final String title, int slots) {
		
		this.javaPlugin = javaPlugin;
		this.inventory = Bukkit.createInventory(null, slots, Component.text(title));
		this.javaPlugin.getServer().getPluginManager().registerEvents(this, this.javaPlugin);
		
		this.key = new NamespacedKey(this.javaPlugin, "ITEM_USE");
		
	}
	
	/**
	 * Action when the inventory is opened by the player
	 * 
	 * defines all the items, their slots and their actions
	 * 
	 * @param player who opens the inventory
	 */
	
	public abstract void onOpen(final Player player);
	
	/**
	 * Action when the inventory is closed
	 * 
	 * @param player who closes the inventory
	 */
	
	public abstract void onClose(final Player player);
	
	/**
	 * Actions executed when the player interact with the inventory
	 * 
	 * 
	 * @param player who interacts with the inventory
	 * @param itemstack: the current item used
	 * @param slot: the current item's slot
	 * @param the action (right click, middle click or left click) of the player on the item
	 */
	
	public abstract void onInteract(final Player player, final ItemStack itemStack, final int slot, final InventoryAction action);
	
	/**
	 * 
	 * check if a player is using the inventory
	 * 
	 * @return a boolean (true if a player is using it, false if not)
	 */
	
	public boolean isPlayerUsingInventory() {
		
		if(this.inventory.getViewers().get(0).hasMetadata("GUI_USE_" + this.name))
			
			return true;
		
		return false;
		
	}
	
	/**
	 * Open the menu to the specified player
	 * @param player: The player that will see the menu
	 */
	public void openMenu(final Player player) {
		
		SpigotAPI.getInstance().getMenuManager().onOpenMenu(player, this);
		
	}
	
	public void closeMenu(final Player player) {
		
		SpigotAPI.getInstance().getMenuManager().onCloseMenu(player, this);
		
	}
	
	/**
	 * 
	 * add an item to the inventory with an action
	 * 
	 * @param itemStack which is added to the inventory
	 * @param slot where the item is added
	 * @param action which is associate to the item
	 */
	
	public void addItem(final ItemStack itemStack, int slot) {
				
		if(itemStack != null)
		
			this.inventory.setItem(slot, itemStack);
		
	}
	
	/**
	 * 
	 * inventory click event
	 * 
	 * execute the onInteract method
	 * 
	 */
	
	@EventHandler
	public void onCloseInventory(final InventoryCloseEvent e) {
		
		final Player player = (Player) e.getPlayer();
		final AbstractMenu menu = MenuManager.getInstance().getMenu(player);
		
		if(menu != null)
			
			MenuManager.getInstance().onCloseMenu(player, menu);
			
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		
		final Player player = (Player) e.getWhoClicked();
		final AbstractMenu menu = MenuManager.getInstance().getMenu(player);
		
		if(e.getCurrentItem() == null || e.getCurrentItem().getType().equals(Material.AIR))
			
			return;
		
		if(menu != null && this.equals(menu)) {
			
			e.setCancelled(true);
			
			MenuManager.getInstance().getMenu(player).onInteract(player, e.getCurrentItem(), e.getSlot(), e.getAction());
			
		}
		
	}
	
	/**
	 * Add PeristentDataContainer to an item
	 * Allows you to recognize an item later
	 * 
	 * @param item - ItemCreator - the items you want to use
	 * @param value - Value of the container (String)
	 * @return ItemStack - The item in ItemStack
	 */
	public ItemStack addTag(final ItemCreator item, final String value) {
		
		final ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.getPersistentDataContainer().set(this.key, PersistentDataType.STRING, value);
		
		item.setItemMeta(itemMeta);
		
		return item.build();
		
	}
	
	/**
	 * Add PeristentDataContainer to an item
	 * Allows you to recognize an item later
	 * 
	 * @param item - ItemStack - the items you want to use
	 * @param value - Value of the container (String)
	 * @return ItemStack - The item with the container
	 */
	public ItemStack addTag(final ItemStack item, final String value) {
		
		final ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.getPersistentDataContainer().set(this.key, PersistentDataType.STRING, value);
		
		item.setItemMeta(itemMeta);
		
		return item;
		
	}
	
	/**
	 * Check if the item has a specific tag
	 * @param item - ItemStack - The item you want to use
	 * @param value - String - The value of the container
	 * @return
	 */
	public boolean hasTag(final ItemStack item, final String value) {
		
		return ItemCreator.hasTag(item, this.key, value);
		
	}
	
	public String getTagValue(final ItemStack item) {
		
		final ItemMeta itemMeta = item.getItemMeta();
		final PersistentDataContainer container = itemMeta.getPersistentDataContainer();
		
		if(container.has(this.key, PersistentDataType.STRING))
			
			return container.get(this.key, PersistentDataType.STRING);
		
		return null;
		
	}
	
}
