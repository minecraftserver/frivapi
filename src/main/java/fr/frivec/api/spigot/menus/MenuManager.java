package fr.frivec.api.spigot.menus;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.managers.AbstractManager;

public class MenuManager extends AbstractManager {
	
	private Map<UUID, AbstractMenu> menus;
	
	private static MenuManager instance;
	
	public MenuManager(final JavaPlugin javaPlugin) {
		
		super(javaPlugin);
		
		menus = new HashMap<>();
		instance = this;
		
	}
	
    @Override
    public void onDisable() {
    	
        this.javaPlugin.getServer().getOnlinePlayers().forEach(this::onDisconnect);
        
    }

    @Override
    public void onConnect(Player player) { /*Not needed*/ }
    
    @Override
    public void onDisconnect(Player player) {
    	
    	this.onCloseMenu(player, this.getMenu(player));
    	
    }
    
    public void onOpenMenu(final Player player, final AbstractMenu menu) {
    	
    	if(menus.containsKey(player.getUniqueId())) {
    		
    		this.menus.get(player.getUniqueId()).onClose(player);
    		this.menus.remove(player.getUniqueId());
    		player.closeInventory();
    		
    	}
    	
    	this.menus.put(player.getUniqueId(), menu);
    	player.setMetadata("GUI_USE_" + menu.name, new FixedMetadataValue(SpigotAPI.getInstance(), true));
    	menu.onOpen(player);
    	player.openInventory(menu.inventory);
    	
    }
    
    public void onCloseMenu(final Player player, final AbstractMenu menu) {
    	
    	if(menus.containsKey(player.getUniqueId())) {
    		
    		menu.onClose(player);
    		menu.inventory.clear();
    		this.menus.remove(player.getUniqueId());
    		player.closeInventory();
    		
    	}
    	
    }
    
    public AbstractMenu getMenu(final Player player) {
    	
    	if(this.menus.containsKey(player.getUniqueId()))
    		
    		return this.menus.get(player.getUniqueId());
    	
    	return null;
    	
    }
    
    public static MenuManager getInstance() {
		return instance;
	}
    
}
