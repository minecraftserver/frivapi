package fr.frivec.api.spigot.messages.connection;

import java.util.HashMap;
import java.util.Map;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import net.kyori.adventure.text.Component;

public class ConnectionMessages {
	
	private Map<RankList, String> joinModels, quitModels;
	
	/**
	 * Define default models. Can be changed with the setter below
	 */
	public ConnectionMessages() {
		
		this.joinModels = new HashMap<>();
		this.quitModels = new HashMap<>();
		
		for(RankList ranks : RankList.values()) {
			
			this.setJoinMessage(ranks, "%prefix% %playerName% §3vient de se connecter !");
			this.setQuitMessage(ranks, "%prefix% %playerName% §cs'est déconnecté.");
			
		}
		
	}
	
	public Component formatJoinModel(final LocalPlayer localPlayer) {
		
		final RankList rank = localPlayer.getRank();
		
		String newMessage = this.joinModels.get(rank);
		
		if(newMessage != null) {
			
			newMessage = newMessage.replace("%prefix%", rank.getPrefix());
			newMessage = newMessage.replace("%playerName%", localPlayer.getName());
			
			return Component.text(newMessage);
			
		}else
			
			return null;
		
	}
	
	public Component formatQuitModel(final LocalPlayer localPlayer) {
		
		final RankList rank = localPlayer.getRank();
		
		String newMessage = this.quitModels.get(rank);
		
		if(newMessage != null) {
			
			newMessage = newMessage.replace("%prefix%", rank.getPrefix());
			newMessage = newMessage.replace("%playerName%", localPlayer.getName());
			
			return Component.text(newMessage);
			
		}else
			
			return null;
		
	}
	
	/**
	 * Set the new model for the join messages for a rank
	 * @param rank: The rank the model will be applied
	 * @param model: The join message model.
	 * 
	 * Keys: 
	 *   %prefix%: Rank Prefix
	 *   %playerName%: Player's name
	 *   If you want the message to be canceled, put the model to null
	 */
	public void setJoinMessage(final RankList rank, final String model) {
		
		if(this.joinModels.containsKey(rank))
		
			this.joinModels.replace(rank, model);
		
		else
			
			this.joinModels.put(rank, model);
		
	}
	
	/**
	 * Set the new model for the quit messages for a rank
	 * @param rank: The rank the model will be applied
	 * @param model: The join message model.
	 * 
	 * Keys: 
	 *   %prefix%: Rank Prefix
	 *   %playerName%: Player's name
	 *   If you want the message to be canceled, put the model to null
	 */
	public void setQuitMessage(final RankList rank, final String model) {
		
		if(this.quitModels.containsKey(rank))
			
			this.quitModels.replace(rank, model);
		
		else
			
			this.quitModels.put(rank, model);
		
	}

}
