package fr.frivec.api.spigot.teams.server;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;

import fr.frivec.api.spigot.teams.options.TeamsOptions;
import net.kyori.adventure.text.format.NamedTextColor;

public class ServerTeam {
	
	private ScoreboardManager manager;
	private Scoreboard mainScoreboard;
	
	private Team bukkitTeam;
	
	/*
	 * Options
	 */
	
	private String prefix,
					suffix;
	
	private NamedTextColor color;
	private TeamsOptions options;
	
	/*
	 * Players
	 */
	
	private HashSet<Player> players;
	
	public ServerTeam(final String name, final String tagName, final String prefix, final String suffix, final NamedTextColor color) {
		
		this(name, tagName, prefix, suffix, color, new TeamsOptions());
		
	}
	
	public ServerTeam(final String name, final String tagName, final String prefix, final String suffix, final NamedTextColor color, final TeamsOptions options) {
		
		this.manager = Bukkit.getScoreboardManager();
		this.mainScoreboard = this.manager.getMainScoreboard();
		this.options = options;
		
		this.players = new HashSet<>();
		
		this.bukkitTeam = this.mainScoreboard.registerNewTeam(name);
		
		refreshTeam();
		
	}
	
	public void refreshTeam() {
		
		this.bukkitTeam.color(this.color);
		this.bukkitTeam.setAllowFriendlyFire(this.options.isFriendlyFire());
		this.bukkitTeam.setCanSeeFriendlyInvisibles(this.options.isSeeInvisibleAllies());
		
		this.bukkitTeam.setOption(Option.COLLISION_RULE, this.options.getCollisions());
		this.bukkitTeam.setOption(Option.DEATH_MESSAGE_VISIBILITY, this.options.getSeeDeathMessages());
		this.bukkitTeam.setOption(Option.NAME_TAG_VISIBILITY, this.options.getTagVisibility());
		
		this.bukkitTeam.prefix(text(this.prefix));
		this.bukkitTeam.suffix(text(this.suffix));
		
	}
	
	public void addPlayer(final Player player) {
		
		this.bukkitTeam.addPlayer(player);
		
		this.players.add(player);
		
	}
	
	public void removePlayer(final Player player) {
		
		this.bukkitTeam.removePlayer(player);
		
		this.players.remove(player);
		
	}
	
	/**
	 * Check if the player is registered in this team
	 * @param player : The player you want to see
	 * @return true if the player is present in the team
	 */
	public boolean hasPlayer(final Player player) {
		
		return this.bukkitTeam.hasPlayer(player);
		
	}
	
	/**
	 * get all the players registered in the Team
	 * @return HashSet of all players registered in team
	 */
	public HashSet<Player> getPlayers() {
		return players;
	}
	
	/**
	 * Unregister the team from the main scoreboard
	 */
	public void unregisterTeam() {
		
		this.bukkitTeam.unregister();
		
	}
	
	public Team getBukkitTeam() {
		return bukkitTeam;
	}
	
	public NamedTextColor getColor() {
		return color;
	}
	
	public void setColor(NamedTextColor color) {
		this.color = color;
	}
	
	public String getSuffix() {
		return suffix;
	}
	
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public TeamsOptions getOptions() {
		return options;
	}
	
	public void setOptions(TeamsOptions options) {
		this.options = options;
	}

}
