package fr.frivec.api.spigot.teams;

import java.util.HashSet;
import java.util.Set;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.teams.client.NMSTeam;
import fr.frivec.api.spigot.teams.server.ServerTeam;
import org.bukkit.Server;
import org.bukkit.entity.Player;

public class TeamManager {
	
	private final Set<NMSTeam> nmsTeams;
	private final Set<ServerTeam> serverTeams;
	
	public TeamManager() {
		
		this.nmsTeams = new HashSet<>();
		this.serverTeams = new HashSet<>();

	}

	/**
	 * Get a NMSTeam by its name
	 * @param teamName: Name of the team registered while creating it
	 * @return NMSTeam: the team corresponding to the name
	 */
	public NMSTeam getNMSTeam(final String teamName) {

		for(NMSTeam teams : this.nmsTeams)

			if(teams.getName().equals(teamName))

				return teams;

		return null;

	}

	/**
	 * Get a ServerTeam by its name
	 * @param teamName: Name of the team registered while creating it
	 * @return ServerTeam: the team corresponding to the name
	 */
	public ServerTeam getServerTeam(final String teamName) {

		for(ServerTeam teams : this.serverTeams)

			if(teams.getBukkitTeam().getName().equals(teamName))

				return teams;

		return null;

	}
	
	/**
	 * Get the player's current team 
	 * @param localPlayer: The LocalPlayer you want to get the team
	 * @return a Team object
	 */
	public NMSTeam getNMSTeam(final LocalPlayer localPlayer) {
		
		for(NMSTeam team : this.nmsTeams)
			
			for(Player players : team.getPlayers())
				
				if(players.getUniqueId().equals(localPlayer.getUuid()))
					
					return team;
		
		return null;
		
	}

	/**
	 * Get the player's current team
	 * @param localPlayer: The LocalPlayer you want to get the team
	 * @return a Team object
	 */
	public ServerTeam getServerTeam(final LocalPlayer localPlayer) {

		for(ServerTeam team : this.serverTeams)

			for(Player players : team.getPlayers())

				if(players.getUniqueId().equals(localPlayer.getUuid()))

					return team;

		return null;

	}
	
	/**
	 * Register a new NMSTeam in the list. Will throw an error if already exists
	 * @param team: The Team you want to register
	 */
	public void registerTeam(final NMSTeam team) throws TeamAlreadyRegistered {

		if(this.nmsTeams.contains(team))

			throw new TeamAlreadyRegistered(team.getName());
		
		this.nmsTeams.add(team);
		
	}

	/**
	 * Register a new ServerTeam in the list. Will throw an error if already exists
	 * @param team: The Team you want to register
	 */
	public void registerTeam(final ServerTeam team) throws TeamAlreadyRegistered {

		if(this.serverTeams.contains(team))

			throw new TeamAlreadyRegistered(team.getBukkitTeam().getName());

		this.serverTeams.add(team);

	}
	
	/**
	 * Unregister a team in the list
	 * Works for NMSTeam and ServerTeam
	 * @param teamName: The name of the team you want to unregister
	 */
	public void unregisterTeam(final String teamName) {
		
		if(this.getNMSTeam(teamName) != null)

			this.nmsTeams.remove(this.getNMSTeam(teamName));

		else

			if(this.getServerTeam(teamName) != null)

				this.serverTeams.remove(this.getServerTeam(teamName));

		return;
		
	}
	
	/**
	 * Get the list of all registered NMSTeams
	 * @return  Set of NMSTeams
	 */
	public Set<NMSTeam> getNMSTeams() {
		return this.nmsTeams;
	}

	/**
	 * Get the list of all registered ServerTeams
	 * @return  Set of ServerTeams
	 */
	public Set<ServerTeam> getServerTeams() {
		return this.serverTeams;
	}

	public class TeamAlreadyRegistered extends Exception {

		public TeamAlreadyRegistered(final String teamName) {

			super("The team " + teamName + " already exists and is already registered.");

		}
	}

}
