package fr.frivec.api.spigot.teams.options;

import org.bukkit.scoreboard.Team.OptionStatus;

public class TeamsOptions {
	
	private boolean friendlyFire,
						seeInvisibleAllies;
	
	private OptionStatus tagVisibility,
							seeDeathMessages,
							collisions;

	public TeamsOptions(final boolean friendlyFire, final boolean seeInvisibleAllies, final OptionStatus tagVisibility, final OptionStatus seeDeathMessages, final OptionStatus collisions) {
		
		this.friendlyFire = friendlyFire;
		this.seeInvisibleAllies = seeInvisibleAllies;
		this.tagVisibility = tagVisibility;
		this.seeDeathMessages = seeDeathMessages;
		this.collisions = collisions;
		
	}
	
	public TeamsOptions() {
		
		this(false, true, OptionStatus.ALWAYS, OptionStatus.ALWAYS, OptionStatus.ALWAYS);
		
	}

	public boolean isFriendlyFire() {
		return friendlyFire;
	}
	
	public void setFriendlyFire(boolean friendlyFire) {
		this.friendlyFire = friendlyFire;
	}

	public boolean isSeeInvisibleAllies() {
		return seeInvisibleAllies;
	}

	public void setSeeInvisibleAllies(boolean seeInvisibleAllies) {
		this.seeInvisibleAllies = seeInvisibleAllies;
	}

	public OptionStatus getTagVisibility() {
		return tagVisibility;
	}

	public void setTagVisibility(OptionStatus tagVisibility) {
		this.tagVisibility = tagVisibility;
	}

	public OptionStatus getSeeDeathMessages() {
		return seeDeathMessages;
	}

	public void setSeeDeathMessages(OptionStatus seeDeathMessages) {
		this.seeDeathMessages = seeDeathMessages;
	}

	public OptionStatus getCollisions() {
		return collisions;
	}

	public void setCollisions(OptionStatus collisions) {
		this.collisions = collisions;
	}

}
