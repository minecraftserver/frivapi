package fr.frivec.api.spigot.teams.client;

import fr.frivec.api.spigot.effects.gloweffect.GlowEffect.EffectColor;
import fr.frivec.api.spigot.tags.NameTagProperty;
import fr.frivec.api.spigot.teams.options.TeamsOptions;
import net.minecraft.network.chat.ChatComponentText;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardTeam;
import net.minecraft.network.protocol.game.PacketPlayOutScoreboardTeam.a;
import net.minecraft.world.scores.ScoreboardTeam;
import net.minecraft.world.scores.ScoreboardTeamBase.EnumNameTagVisibility;
import net.minecraft.world.scores.ScoreboardTeamBase.EnumTeamPush;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;

import static fr.frivec.api.spigot.packets.PacketUtils.getEntityPlayer;
import static fr.frivec.api.spigot.packets.PacketUtils.sendPacket;

public class NMSTeam {
	
	private String name;
	private EffectColor color;
	private final String tagName;
	private String prefix,
					suffix;
	
	private EnumNameTagVisibility nameTagVisibility;
	private EnumTeamPush collision;
	
	private List<Player> players;
	
	public NMSTeam(final String name, final NameTagProperty nameTagProperty, final TeamsOptions options, final List<Player> players) {
		
		this.name = name;
		this.color = nameTagProperty.getNmsColor();
		this.tagName = nameTagProperty.getTagName();
		this.prefix = nameTagProperty.getPrefix();
		this.suffix = nameTagProperty.getSuffix();
		this.players = players;

		this.nameTagVisibility = options.getTagVisibility().equals(Team.OptionStatus.ALWAYS) ? EnumNameTagVisibility.a :
									options.getTagVisibility().equals(Team.OptionStatus.FOR_OTHER_TEAMS) ? EnumNameTagVisibility.c :
									options.getTagVisibility().equals(Team.OptionStatus.FOR_OWN_TEAM) ? EnumNameTagVisibility.d :
									EnumNameTagVisibility.b;

		this.collision = options.getCollisions().equals(Team.OptionStatus.ALWAYS) ? EnumTeamPush.a :
								options.getCollisions().equals(Team.OptionStatus.FOR_OTHER_TEAMS) ? EnumTeamPush.d :
								options.getCollisions().equals(Team.OptionStatus.FOR_OWN_TEAM) ? EnumTeamPush.d :
								EnumTeamPush.b;

	}
	
	public NMSTeam(String name, EffectColor color, String tagName, String prefix, String suffix) {
		
		this(name, new NameTagProperty(tagName, prefix, suffix, null, color), new TeamsOptions(), new ArrayList<>());
		
	}
	
	public void createTeam(final Player receiver) {
		
		createPacket(null, true, false, false, false, false, receiver);
		
	}
	
	public void updateTeam(final Player receiver) {
		
		createPacket(null, false, false, true, false, false, receiver);
		
	}
	
	public void deleteTeam(final Player receiver) {
		
		createPacket(null, false, true, false, false, false, receiver);
		
	}
	
	public void addPlayer(final Player player, final Player receiver) {
		
		createPacket(player, false, false, false, true, false, receiver);
		
	}
	
	public void removePlayer(final Player player, final Player receiver) {
		
		createPacket(player, false, false, false, false, true, receiver);
		
	}
	
	public void createPacket(final Player player, final boolean createTeam, final boolean deleteTeam, final boolean updateTeam, final boolean addPlayer, final boolean removePlayer, Player receiver) {
		
		if(receiver == null)
			
			receiver = player;

		assert receiver!= null;
		
		try {

			final ScoreboardTeam scoreboardTeam = new ScoreboardTeam(getEntityPlayer(receiver).fF(), this.name);
			
			if(createTeam || updateTeam) {
				
				scoreboardTeam.a(this.nameTagVisibility);
				scoreboardTeam.a(this.collision);
				scoreboardTeam.a(this.color.getChatFormat());
				scoreboardTeam.a(new ChatComponentText(this.tagName));
				scoreboardTeam.b(this.prefix == null ? null : new ChatComponentText(this.prefix));
				scoreboardTeam.c(this.suffix == null ? null : new ChatComponentText(this.suffix));
				
				if(!this.players.isEmpty())
					
					for(final Player players : this.players)
						
						scoreboardTeam.g().add(players.getName());
				
			}else {
					
				if(addPlayer) {
						
					if(!this.players.contains(player))
						
						this.players.add(player);
			
					scoreboardTeam.g().add(player.getName());
					
				}else if(removePlayer) {

					this.players.remove(player);
						
					scoreboardTeam.g().remove(player.getName());
											
				}
				
			}
			
			PacketPlayOutScoreboardTeam team = PacketPlayOutScoreboardTeam.a(scoreboardTeam, true);
			
			if(deleteTeam)
				
				team = PacketPlayOutScoreboardTeam.a(scoreboardTeam);
			
			else if(addPlayer)
				
				team = PacketPlayOutScoreboardTeam.a(scoreboardTeam, player.getName(), a.a);
			
			else if(removePlayer)
				
				team = PacketPlayOutScoreboardTeam.a(scoreboardTeam, player.getName(), a.b);
			
			else if(updateTeam)
				
				team = PacketPlayOutScoreboardTeam.a(scoreboardTeam, false);

			/*
			For test - Temporary !
			 */
			
			sendPacket(receiver, team);
		
		} catch (IllegalArgumentException | SecurityException e1) {
			e1.printStackTrace();
		}
		
	}
	
	public boolean isPlayerInTeam(final Player player) {

		return this.players.contains(player);

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EffectColor getColor() {
		return color;
	}

	public void setColor(EffectColor color) {
		this.color = color;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public EnumNameTagVisibility getNameTagVisibility() {
		return nameTagVisibility;
	}

	public void setNameTagVisibility(EnumNameTagVisibility nameTagVisibility) {
		this.nameTagVisibility = nameTagVisibility;
	}

	public EnumTeamPush getCollision() {
		return collision;
	}

	public void setCollision(EnumTeamPush collision) {
		this.collision = collision;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

}

