package fr.frivec.api.spigot.listeners.player.connection;

import fr.frivec.api.spigot.teams.client.NMSTeam;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.redisson.api.RMap;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.effects.gloweffect.GlowEffect;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.spectre.ServerInformations;

public class PlayerConnection implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(final PlayerJoinEvent event) {
		
		final Player player = event.getPlayer();
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
		
		localPlayer.setCurrentServer(SpigotAPI.getInstance().getInformations().getId());
		localPlayer.sendInRedis();
		
		/*
		 * Create all registered NMS teams for the player
		 */
		
		Bukkit.getScheduler().runTask(SpigotAPI.getInstance(), ()->{
			
			for(NMSTeam teams : SpigotAPI.getInstance().getTeamManager().getNMSTeams())
				
				teams.createTeam(player);
			
		});
		
		/*
		 * onConnect for all registered managers
		 */
		
		SpigotAPI.getInstance().getManagerController().onConnect(player);
		
		/*
		 * Update server size in informations
		 */
		
		final ServerInformations infos = SpigotAPI.getInstance().getInformations();
		final RMap<String, String> rMap = SpigotAPI.getInstance().getRedis().getClient().getMap(ServerInformations.redisKey);
		
		infos.setSize(Bukkit.getServer().getOnlinePlayers().size());
		infos.getOnlinePlayers().add(player.getUniqueId());
		rMap.replace(infos.getId().toString(), GsonManager.getInstance().serializeObject(infos));
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onQuit(final PlayerQuitEvent event) {
		
		final Player player = event.getPlayer();

		/*
		Remove player from the server list
		 */
		
		/*
		 * onDisconnect for all registered managers
		 */
		
		SpigotAPI.getInstance().getManagerController().onDisconnect(player);
		
		/*
		 * Update server size in informations
		 */
				
		final ServerInformations infos = SpigotAPI.getInstance().getInformations();
		final RMap<String, String> rMap = SpigotAPI.getInstance().getRedis().getClient().getMap(ServerInformations.redisKey);
		
		infos.setSize(Bukkit.getServer().getOnlinePlayers().size() - 1); //- 1 because the size isn't update when this event is triggered
		infos.getOnlinePlayers().remove(player.getUniqueId());
		rMap.replace(infos.getId().toString(), GsonManager.getInstance().serializeObject(infos));
		
		/*
		 * Remove player from packets utilities
		 */
		
		final GlowEffect effect = SpigotAPI.getInstance().getGlowEffect();
		
		if(!effect.getListeners().containsKey(player))
			
			return;
		
		for(Player viewer : Bukkit.getOnlinePlayers())
			
			if(effect.getListeners().containsKey(player) && effect.getListeners().get(player).getViewers().contains(viewer))
				
				effect.removeViewer(player, viewer);
		
		effect.setGlowColor(player, false);
		
		/*
		 * Delete all teams client-side
		 */
			
		for(NMSTeam teams : SpigotAPI.getInstance().getTeamManager().getNMSTeams())
				
			teams.deleteTeam(player);
		
	}

}
