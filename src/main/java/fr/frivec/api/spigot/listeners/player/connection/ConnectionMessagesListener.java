package fr.frivec.api.spigot.listeners.player.connection;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.messages.connection.ConnectionMessages;
import net.kyori.adventure.text.Component;

public class ConnectionMessagesListener implements Listener {
	
	private static ConnectionMessagesListener instance;
	
	private ConnectionMessages messages;
	
	public ConnectionMessagesListener() {
		
		instance = this;
		
		this.messages = new ConnectionMessages();
		
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onJoin(final PlayerJoinEvent event) {
		
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(event.getPlayer().getUniqueId());
		final Component joinMessage = this.getConnectionMessages().formatJoinModel(localPlayer);
		
		event.joinMessage(joinMessage);
		
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onQuit(final PlayerQuitEvent event) {
		
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(event.getPlayer().getUniqueId());
		
		event.quitMessage(this.getConnectionMessages().formatQuitModel(localPlayer));
		
	}
	
	/**
	 * Get instance of ConnectionMessagesManager
	 * @return instance of ConnectionMessages
	 */
	public ConnectionMessages getConnectionMessages() {
		return messages;
	}
	
	public static ConnectionMessagesListener getInstance() {
		return instance;
	}

}
