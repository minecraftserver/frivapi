package fr.frivec.api.spigot.listeners.player.chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import io.papermc.paper.chat.ChatRenderer;
import io.papermc.paper.event.player.AsyncChatEvent;

public class ChatListener implements Listener {
	
	private static ChatListener instance;
	
	private ChatRenderer renderer;
	
	public ChatListener(final ChatRenderer renderer) {
		
		instance = this;
		
		this.renderer = renderer;
		
	}
	
	@EventHandler
	public void onMessage(final AsyncChatEvent event) {
		
		event.renderer(this.renderer);
		
		for(Player player : Bukkit.getOnlinePlayers())
		
			event.renderer().render(event.getPlayer(), event.getPlayer().displayName(), event.message(), player);
		
	}
	
	/**
	 * Get the instance of ChatListener.
	 * Can be used to change ChatRenderer
	 * @return ChatListener's instance
	 */
	public static ChatListener getInstance() {
		return instance;
	}
	
	/**
	 * Get instance of the current chat renderer
	 * @return ChatRenderer
	 */
	public ChatRenderer getRenderer() {
		return renderer;
	}
	
	/**
	 * Set the instance of a new ChatRenderer to modify the chat's format
	 * @param renderer: The new ChatRenderer you want to use.
	 */
	public void setRenderer(ChatRenderer renderer) {
		this.renderer = renderer;
	}

}
