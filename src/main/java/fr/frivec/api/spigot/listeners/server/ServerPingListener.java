package fr.frivec.api.spigot.listeners.server;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.destroystokyo.paper.event.server.PaperServerListPingEvent;

import fr.frivec.api.core.string.StringUtils;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.core.spectre.ServerInformations;

public class ServerPingListener implements Listener {
	
	@EventHandler
	public void onPing(final PaperServerListPingEvent event) {
		
		final ServerInformations infos = SpigotAPI.getInstance().getInformations();
		
		event.setMaxPlayers(infos.getMaxSize());
		event.motd(StringUtils.text(infos.getMotd()[0] + infos.getMotd()[1]));
		
	}

}
