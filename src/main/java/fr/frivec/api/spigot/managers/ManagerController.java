package fr.frivec.api.spigot.managers;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ManagerController {
	
	private Set<AbstractManager> managers;
	
	public ManagerController(JavaPlugin javaPlugin) {
		
		this.managers = new HashSet<>();
		
	}
	
	/*
	 * Action to execute when a player is connecting
	 */
	public void onConnect(Player player) {
		
		for(AbstractManager manager : this.managers)
			
			manager.onConnect(player);
		
	}
	
	/**
	 * Action to do when the manager is disabling
	 */
	public void onDisable() {
	
		for(AbstractManager manager : this.managers)
				
			manager.onDisable();
		
	}
	
	/**
	 * Action to do when the player is disconnecting
	 */
	public void onDisconnect(Player player) {
		
		for(AbstractManager manager : this.managers)
			
			manager.onDisconnect(player);
		
	}
	
	public void registerManager(final AbstractManager manager) {
		
		this.managers.add(manager);
		
	}
	public void unregisterManager(final AbstractManager manager) {
		
		this.managers.remove(manager);
		
	}

}

