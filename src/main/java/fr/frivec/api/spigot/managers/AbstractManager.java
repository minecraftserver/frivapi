package fr.frivec.api.spigot.managers;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.frivec.api.spigot.SpigotAPI;

public abstract class AbstractManager {

	protected JavaPlugin javaPlugin;
	
	public AbstractManager(final JavaPlugin javaPlugin) {
		
		this.javaPlugin = javaPlugin;
		
		SpigotAPI.getInstance().getManagerController().registerManager(this);
		
	}
	
	public abstract void onConnect(final Player player);
	
	public abstract void onDisconnect(final Player player);
	
	public abstract void onDisable();
	
}
