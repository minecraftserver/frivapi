package fr.frivec.api.spigot.tags;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.tags.server.ServerSideNameTag;
import fr.frivec.api.spigot.teams.client.NMSTeam;

public class NameTagManager {
	
	private final Map<RankList, NameTagProperty> properties;
	private final Map<RankList, NMSTeam> rankTeams;
	
	public NameTagManager() {
		
		this.properties = new HashMap<>();
		this.rankTeams = new HashMap<>();
		
	}
	
	/*
	 * 
	 */
	public void updateNameTags(final Player player) {
		
		for(Player players : Bukkit.getOnlinePlayers()) {
			
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(players.getUniqueId());
			final NameTagProperty property = this.getNTProperty(localPlayer.getRank());
			
			final NMSTeam nmsTeam = this.getNMSTeam(localPlayer.getRank());
			
			if(player == null) {
			
				//Update server-side name tag
				ServerSideNameTag.setNameTag(players, property.getTagName(), property.getColor(), property.getPrefix(), property.getSuffix());
				
				//Update client-side name tag (packets)
				nmsTeam.setColor(property.getNmsColor());
				nmsTeam.setPrefix(property.getPrefix());
				nmsTeam.setSuffix(property.getSuffix());
				nmsTeam.setName(property.getTagName());
				
				nmsTeam.updateTeam(players);
				
			}else {
				
				//Update server-side name tag
				ServerSideNameTag.setNameTag(player, property.getTagName(), property.getColor(), property.getPrefix(), property.getSuffix());
				
				//Update client-side name tag (packets)
				nmsTeam.setColor(property.getNmsColor());
				nmsTeam.setPrefix(property.getPrefix());
				nmsTeam.setSuffix(property.getSuffix());
				nmsTeam.setName(property.getTagName());
				
				nmsTeam.updateTeam(players);
				
			}
			
		}
		
	}
	
	public Map<RankList, NameTagProperty> getProperties() {
		return properties;
	}
	
	public Map<RankList, NMSTeam> getRankTeams() {
		return rankTeams;
	}
	
	public void setNameTag(final RankList rank, final NameTagProperty newProperty) {
		
		this.properties.replace(rank, newProperty);
		updateNameTags(null);
		
	}
	
	public void setNameTag(final RankList rank, final NameTagProperty newProperty, final Player player) {
		
		this.properties.replace(rank, newProperty);
		updateNameTags(player);
		
	}
	
	public NameTagProperty getNTProperty(final RankList rank) {
		
		return this.properties.get(rank);
		
	}
	
	public NMSTeam getNMSTeam(final RankList rank) {
		
		return this.rankTeams.get(rank);
		
	}

}
