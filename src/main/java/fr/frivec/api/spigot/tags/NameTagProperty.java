package fr.frivec.api.spigot.tags;

import fr.frivec.api.spigot.effects.gloweffect.GlowEffect.EffectColor;
import net.kyori.adventure.text.format.NamedTextColor;

public class NameTagProperty {
	
	private String tagName,
					prefix,
					suffix;
	
	private NamedTextColor color;
	private EffectColor nmsColor;

	public NameTagProperty(final String tagName, final String prefix, final String suffix, final NamedTextColor color, final EffectColor nmsColor) {
		
		this.tagName = tagName;
		this.prefix = prefix;
		this.suffix = suffix;
		this.color = color;
		this.nmsColor = nmsColor;
	
	}
	
	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public NamedTextColor getColor() {
		return color;
	}

	public void setColor(NamedTextColor color) {
		this.color = color;
	}
	
	public EffectColor getNmsColor() {
		return nmsColor;
	}
	
	public void setNmsColor(EffectColor nmsColor) {
		this.nmsColor = nmsColor;
	}

}
