package fr.frivec.api.spigot.tags.server;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class ServerSideNameTag {
	
	private String prefix;
	private String suffix;
	private Team team;
	public static Scoreboard scoreboard;

	public ServerSideNameTag(String name, NamedTextColor nameColor, String prefix, String suffix, Scoreboard current) throws Exception {
		
		this.prefix = prefix;
		this.suffix = suffix;
		this.team = current.getTeam(name);
		
		if(this.team == null){
			this.team = current.registerNewTeam(name);
		}
		
		scoreboard = current;
		this.team.setCanSeeFriendlyInvisibles(false);
		this.team.setAllowFriendlyFire(true);
		
		/* VERIFICATION */
		int prefixLength = 0;
		int suffixLength = 0;
		if (prefix != null) {
			prefixLength = prefix.length();
		}
		if (suffix != null) {
			suffixLength = suffix.length();
		}
		
		if (prefixLength + suffixLength >= 32) {
			throw new Exception("prefix and suffix lenghts are greater than 16");
		}
		
		if(prefixLength >= 16 || suffixLength >= 16)
			
			throw new Exception("prefix or suffix lenghts are greater than 16");
		
		if (suffix != null) {
			this.team.suffix(Component.text(ChatColor.translateAlternateColorCodes('&', suffix)));
		}
		if (prefix != null) {
			this.team.prefix(Component.text(ChatColor.translateAlternateColorCodes('&', prefix)));
		}
		if(nameColor != null)
			
			this.team.color(nameColor);
	}
	
	public ServerSideNameTag(String name, NamedTextColor nameColor, String prefix, String suffix) throws Exception {
		this(name, nameColor, prefix, suffix, Bukkit.getScoreboardManager().getMainScoreboard());
	}
	
	public void set(Player player){
		this.team.addPlayer(player);
		player.setScoreboard(scoreboard);
	}
	
	public void remove(Player player){
		this.team.removePlayer(player);
	}

	public void resetTagUtils(UUID uuid) {
		remove(Bukkit.getPlayer(uuid));
	}
	
	public void setAll(Collection<Player> players) {
		for (Player player : players) {
			set(player);
		}
	}
	
	public void setAll(Player[] players) {
		Player[] arrayOfPlayer;
		int j = (arrayOfPlayer = players).length;
		for (int i = 0; i < j; i++) {
			Player player = arrayOfPlayer[i];
			set(player);
		}
	}
	
	public void setPrefix(String prefix) {
		
		this.prefix = ChatColor.translateAlternateColorCodes('&', prefix);
		this.team.prefix(Component.text(this.prefix));
	
	}
	
	public void setSuffix(String suffix) {
		
		this.suffix = ChatColor.translateAlternateColorCodes('&', suffix);
		this.team.suffix(Component.text(this.suffix));
		
	}
	
	public String getPrefix() {
		return this.prefix;
	}
	
	public String getSuffix() {
		return this.suffix;
	}
	
	public Team getTeam() {
		return this.team;
	}
	
	public void removeTeam() {
		this.team.unregister();
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}
	
	public static void setNameTag(Player player, String name, NamedTextColor nameColor, String prefix, String suffix){
		
		if(prefix.length() >= 16 || suffix.length() >= 16)
			
			player.sendMessage("§cError. The prefix or the suffix for this tag is greater than 16 characters. Please modify it.");
		
		try{
			
			ServerSideNameTag tagplayer = new ServerSideNameTag(name, nameColor, prefix, suffix);
			tagplayer.set(player);
		
		}catch (Exception e){
		
			e.printStackTrace();
		
		}
	}
}
