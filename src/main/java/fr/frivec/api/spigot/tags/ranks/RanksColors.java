package fr.frivec.api.spigot.tags.ranks;

import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.effects.gloweffect.GlowEffect.EffectColor;
import net.kyori.adventure.text.format.NamedTextColor;

public enum RanksColors {
	
	ADMIN(RankList.ADMIN, NamedTextColor.RED, EffectColor.RED),
	MOD(RankList.MOD, NamedTextColor.BLUE, EffectColor.BLUE),
	DEV(RankList.DEV, NamedTextColor.GOLD, EffectColor.GOLD),
	STAFF(RankList.STAFF, NamedTextColor.DARK_AQUA, EffectColor.CYAN),
	FRIEND(RankList.FRIEND, NamedTextColor.WHITE, EffectColor.WHITE),
	YT(RankList.YT, NamedTextColor.LIGHT_PURPLE, EffectColor.PINK),
	VIP_PLUS(RankList.VIP_PLUS, NamedTextColor.AQUA, EffectColor.LIGHT_BLUE),
	VIP(RankList.VIP, NamedTextColor.GREEN, EffectColor.LIME),
	PLAYER(RankList.PLAYER, NamedTextColor.GRAY, EffectColor.LIGHT_GRAY);
	
	private RankList rank;
	
	private NamedTextColor serverColor;
	private EffectColor effectColor;
	
	private RanksColors(RankList rank, NamedTextColor serverColor, EffectColor effectColor) {
		
		this.rank = rank;
		this.serverColor = serverColor;
		this.effectColor = effectColor;
		
	}
	
	public static RanksColors getColor(final RankList rank) {
		
		for(RanksColors colors : RanksColors.values())
			
			if(colors.getRank().equals(rank))
				
				return colors;
		
		return null;
	}
	
	public RankList getRank() {
		return rank;
	}
	
	public void setRank(RankList rank) {
		this.rank = rank;
	}
	
	public NamedTextColor getServerColor() {
		return serverColor;
	}
	
	public void setServerColor(NamedTextColor serverColor) {
		this.serverColor = serverColor;
	}
	
	public EffectColor getEffectColor() {
		return effectColor;
	}
	
	public void setEffectColor(EffectColor effectColor) {
		this.effectColor = effectColor;
	}

}
