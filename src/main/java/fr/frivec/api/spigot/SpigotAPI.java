package fr.frivec.api.spigot;

import fr.frivec.api.core.logger.Logger;
import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.adapter.SanctionAdapter;
import fr.frivec.api.core.servers.status.ServerStatus;
import fr.frivec.api.core.utils.JsonReader;
import fr.frivec.api.spigot.commands.CommandManager;
import fr.frivec.api.spigot.commands.administrator.HologramCommand;
import fr.frivec.api.spigot.commands.administrator.RankCommand;
import fr.frivec.api.spigot.commands.administrator.StopCommand;
import fr.frivec.api.spigot.commands.dev.DevCommand;
import fr.frivec.api.spigot.commands.dev.WorldChangeCommand;
import fr.frivec.api.spigot.commands.mods.*;
import fr.frivec.api.spigot.commands.player.privatemessage.PMCommand;
import fr.frivec.api.spigot.commands.player.privatemessage.ReplyCommand;
import fr.frivec.api.spigot.effects.gloweffect.GlowEffect;
import fr.frivec.api.spigot.listeners.ListenerManager;
import fr.frivec.api.spigot.listeners.dev.DevListener;
import fr.frivec.api.spigot.listeners.player.connection.ConnectionMessagesListener;
import fr.frivec.api.spigot.listeners.player.connection.PlayerConnection;
import fr.frivec.api.spigot.listeners.server.ServerPingListener;
import fr.frivec.api.spigot.managers.ManagerController;
import fr.frivec.api.spigot.menus.MenuManager;
import fr.frivec.api.spigot.packets.manager.PacketsManager;
import fr.frivec.api.spigot.pluginmessages.BungeeCordPMListener;
import fr.frivec.api.spigot.pluginmessages.PluginMessageManager;
import fr.frivec.api.spigot.redis.SpigotRedisListener;
import fr.frivec.api.spigot.teams.TeamManager;
import fr.frivec.core.Credentials;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.Redis;
import fr.frivec.core.redis.pubsub.PubSubManager;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.redisson.api.RMap;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

public class SpigotAPI extends JavaPlugin {

	/*
	 *
	 * FrivAPI de Antoine Letessier est mis à disposition selon les termes de la licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International.
	 * Fondé(e) sur une œuvre à https://gitlab.com/Frivec/frivapi.
	 *
	 */

	private static SpigotAPI instance;

	private boolean registered;

	private Logger logger;
	private ListenerManager listenerManager;
	private CommandManager commandManager;
	private PluginMessageManager pluginMessageManager;
	private PacketsManager packetsManager;
	private GsonManager gsonManager;
	private TeamManager teamManager;
	
	/*
	 * IG Managers
	 */
	
	private ManagerController managerController;
	private MenuManager menuManager;
	
	//Redis
	private Redis redis;
	private PubSubManager channelManager;

	private GlowEffect glowEffect;

	/*
	 * Server infos
	 */
	private ServerInformations informations;
	private ServerStatus status;
	private UUID serverUUID;

	@Override
	public void onEnable() {

		instance = this;

		/*
		 * Utils class
		 */
		
		this.logger = new Logger(this);
		this.listenerManager = new ListenerManager(this);
		this.commandManager = new CommandManager();
		this.teamManager = new TeamManager();
		this.glowEffect = new GlowEffect(this.teamManager);
		this.pluginMessageManager = new PluginMessageManager();
		this.packetsManager = new PacketsManager();
		this.gsonManager = new GsonManager(List.of(new SanctionAdapter()), List.of(Sanction.class));
		
		/*
		 * InGame Managers
		 */
		
		this.managerController = new ManagerController(this);
		this.menuManager = new MenuManager(this);
		
		log(LogLevel.INFO, "Managers started");

		this.listenerManager.registerPluginMessageListener("BungeeCord", new BungeeCordPMListener());
		log(LogLevel.INFO, "Plugin Messages registered.");

		//Commands
		new DevCommand();
		new RankCommand();
		new HologramCommand();
		new BanCommand();
		new KickCommand();
		new MuteCommand();
		new UnbanCommand();
		new UnmuteCommand();
		new PMCommand();
		new ReplyCommand();
		new WorldChangeCommand();
		new StopCommand();

		log(LogLevel.INFO, "Commands registered");

		this.getListenerManager().registerListener(new PlayerConnection());
		this.getListenerManager().registerListener(new DevListener());
		this.getListenerManager().registerListener(new ConnectionMessagesListener());
		this.getListenerManager().registerListener(new ServerPingListener());

		log(LogLevel.INFO, "Listeners registered");

		final Path root = Paths.get("").toAbsolutePath(), informationsFile = Paths.get(root + File.separator + "spectreInformations.json");
		final String json = JsonReader.readJson(informationsFile);

		if(json != null) {

			this.informations = (ServerInformations) this.gsonManager.deSeralizeJson(json, ServerInformations.class);
			this.serverUUID = this.informations.getId();

		}else {
			
			/*
			 * Stop the server. The plugin will not be able to work without these informations.
			 */
			
			log(LogLevel.SEVERE, "Cannot read server informations' file. Cannot launch the process.");
			
			this.getServer().shutdown();
			
			return;
			
		}
		
		this.registered = true;

		log(LogLevel.INFO, "Loaded server informations from Spectre.");

		this.redis = new Redis(new Credentials("localhost", "SpigotAPI", "t3iw32ZQA3", 6379));
		
		if(!this.redis.getClient().isShutdown())
			
			log(LogLevel.INFO, "Connected to Redis");
		
		else {
			
			log(LogLevel.SEVERE, "Unable to connect to Redis. Stopping the server...");
			
			this.getServer().shutdown();
			
			return;
			
		}
		
		/*
		 * PubSub
		 */

		SpigotRedisListener redisListener = new SpigotRedisListener();
		this.channelManager = new PubSubManager(redisListener);

		//Channels for pubsub messaging
		this.channelManager.subscribe("PrivateMessage");
		this.channelManager.subscribe("Spectre");
		this.channelManager.subscribe("Sanction");
		this.channelManager.subscribe("LocalPlayer");
		this.channelManager.subscribe("KickPlayer");
		this.channelManager.subscribe("SpectreReload");
		this.channelManager.subscribe("SendPlayer");
		this.channelManager.subscribe("PlayerMessage");

		this.channelManager.publish("Spectre", this.gsonManager.serializeObject(new SpectreMessage(SpectreMessageType.REGISTER_SERVER, null, this.informations.getId(), this.informations.getProxyID(), this.informations.getServerType(), this.informations.getName(), this.informations.getPort(), 0, 0, false)));
		
		/*
		 * Server status
		 */
		
		updateServerStatus(ServerStatus.OPEN);
		
		super.onEnable();

	}

	@Override
	public void onDisable() {
		
		updateServerStatus(ServerStatus.CLOSING);
		
		if(this.redis != null) {
		
			/*
			 * Send message to unregister server from the proxy
			 * Remove the server from the redis server list
			 */
			
			if(this.registered)
			
				unregisterServer();
				
			this.channelManager.unsubscribe("PrivateMessage");
			this.channelManager.unsubscribe("Spectre");
			this.channelManager.unsubscribe("Sanction");
			this.channelManager.unsubscribe("LocalPlayer");
			this.channelManager.unsubscribe("KickPlayer");
			this.channelManager.unsubscribe("SpectreReload");
			this.channelManager.unsubscribe("SendPlayer");
			this.channelManager.unsubscribe("PlayerMessage");
			
			updateServerStatus(ServerStatus.CLOSE);
			
			this.redis.close();
			
		}
		
		this.getServer().getMessenger().unregisterIncomingPluginChannel(this);
		this.getServer().getMessenger().unregisterOutgoingPluginChannel(this);
		
		/*
		 * onDisable for all registered managers
		 */
		
		SpigotAPI.getInstance().getManagerController().onDisable();

		super.onDisable();

	}

	/**
	 * Update the server status on Redis.
	 * Allows others plugins to show the status of this server and to adapt actions to it.
	 * @param serverStatus The new status of the server
	 */
	public void updateServerStatus(final ServerStatus serverStatus) {
		
		final ServerInformations infos = this.getInformations();
		
		this.status = serverStatus;
		infos.setServerStatus(serverStatus.toString());
		
		final RMap<String, String> rMap = this.getRedis().getClient().getMap(ServerInformations.redisKey);
		
		rMap.replace(infos.getId().toString(), GsonManager.getInstance().serializeObject(infos));
		
	}

	/**
	 * Stop the server.
	 * Will unregister it from its proxy then shutdown it after 5 seconds.
	 * Redirect all players to an available hub.
	 */
	public void stopServer() {

		if(this.getInformations().getProxyID() != null)

			this.unregisterServer();

		//Shutdown the server 5 seconds after unregister to give time to the players to be redirected.
		Bukkit.getScheduler().scheduleSyncDelayedTask(SpigotAPI.getInstance(), Bukkit::shutdown, 5*20L);

	}

	/**
	 * Send a redis message to the proxy of the server to unregister it.
	 * Will redirect all players to an available hub.
	 */
	public void unregisterServer() {
		
		this.channelManager.publish("Spectre", this.gsonManager.serializeObject(new SpectreMessage(SpectreMessageType.UNREGISTER_SERVER, null, this.informations.getId(), this.informations.getProxyID(), this.informations.getServerType(), this.informations.getName(), 0, 0, 0, false)));
		this.getRedis().getClient().getMap(ServerInformations.redisKey).remove(this.informations.getId().toString());
		
		this.registered = false;
		
	}

	//Getters and setters

	/**
	 * Get the main instance of the API
	 * @return the instance
	 */
	public static SpigotAPI getInstance() {
		return instance;
	}

	/**
	 * Get the listener manager's instance.
	 * Can be used to register a new listener
	 * @return the listener manager's instance
	 */
	public ListenerManager getListenerManager() {
		return listenerManager;
	}

	/**
	 * Get the commands manager's instance
	 * Can be used to register a new command
	 * @return the instance of the command manager
	 */
	public CommandManager getCommandManager() {
		return commandManager;
	}

	/**
	 * Get the redis instance
	 * @return redis instance
	 */
	public Redis getRedis() {
		return redis;
	}

	/**
	 * Get the GlowEffect manager
	 * @return glow effect manager instance
	 */
	public GlowEffect getGlowEffect() {
		return glowEffect;
	}

	/**
	 * Get the plugin messages manager
	 * @return PM manager instance
	 */
	public PluginMessageManager getPluginMessageManager() {
		return pluginMessageManager;
	}

	public UUID getServerUUID() {
		return serverUUID;
	}

	public PacketsManager getPacketsManager() {
		return packetsManager;
	}

	public GsonManager getGsonManager() {
		return gsonManager;
	}

	public PubSubManager getChannelManager() {
		return channelManager;
	}

	public ServerInformations getInformations() {
		return informations;
	}
	
	public void setInformations(ServerInformations informations) {
		this.informations = informations;
	}
	
	public TeamManager getTeamManager() {
		return teamManager;
	}
	
	public ConnectionMessagesListener getConnectionMessageListener() {
		return ConnectionMessagesListener.getInstance();
	}
	
	public ManagerController getManagerController() {
		return managerController;
	}
	
	public MenuManager getMenuManager() {
		return menuManager;
	}
	
	public boolean isRegistered() {
		return registered;
	}

	public ServerStatus getStatus() {
		return status;
	}
	
	/**
	 * Send a log in the console of the server
	 * The color of the text will change according of the importance of the message
	 * @param level: The importance of the message.
	 * @param message; The message that will be send in the console.
	 */
	public static void log(final LogLevel level, final String message) {

		SpigotAPI.getInstance().logger.log(level, message);

	}

	/**
	 * Send a plugin message on the bungeecord instance of this server.
	 * To send a message to all the bungeecord, if there are more than 2 proxy, use Redis.
	 * @param channel: The channel you want to send in the message
	 * @param sender: The player that will be the sender. If null, the console will send the message
	 * @param message: The message in a table of byte. Use the ByteArrayDataOutPut class to create the message.
	 */
	public static void sendPluginMessage(final String channel, final Player sender, final byte[] message) {

		if(sender != null && sender.isOnline())

			sender.sendPluginMessage(SpigotAPI.getInstance(), channel, message);

		else

			SpigotAPI.getInstance().getServer().sendPluginMessage(SpigotAPI.getInstance(), channel, message);

	}

}
