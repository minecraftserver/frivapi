package fr.frivec.api.spigot.title;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.kyori.adventure.text.Component;

public class ActionBar {
	
	public static void sendActionBarToPlayer(final Player player, final Component text) {
		
		player.sendActionBar(text);
		
	}
	
	public static void sendActionBarToAllPlayer(final Component text) {
		
		Bukkit.getServer().sendActionBar(text);
		
	}

}
