package fr.frivec.api.spigot.title;

import net.kyori.adventure.title.Title.Times;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.time.Duration;

import static fr.frivec.api.core.string.StringUtils.text;

public class Title {
	
	public static void sendTitleToPlayer(final Player player, final String title, final String subTitle, int fadeIn, int stay, int fadeOut) {

		player.showTitle(net.kyori.adventure.title.Title.title(text(title), text(subTitle), Times.times(Duration.ofMillis(fadeIn), Duration.ofMillis(stay), Duration.ofMillis(fadeOut))));
		
	}
	
	public static void sendTitleToAllPlayers(final String title, final String subTitle, int fadeIn, int stay, int fadeOut) {

		Bukkit.getServer().showTitle(net.kyori.adventure.title.Title.title(text(title), text(subTitle), Times.times(Duration.ofMillis(fadeIn), Duration.ofMillis(stay), Duration.ofMillis(fadeOut))));
		
	}

}
