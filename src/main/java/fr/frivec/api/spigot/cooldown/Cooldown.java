package fr.frivec.api.spigot.cooldown;

import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;

import fr.frivec.api.spigot.SpigotAPI;

public abstract class Cooldown extends BukkitRunnable {
	
	private Player player;
	private int timer;
	private NamespacedKey key;
	
	public Cooldown(final int timer, final Player player, final String key) {
		
		this.player = player;
		this.timer = timer;
		this.key = new NamespacedKey(SpigotAPI.getInstance(), key);
		
	}
	
	public void start(final long delay, final long period) {
		
		if(hasCooldown(this.player, this.key.getKey()))
			
			return;
		
		player.getPersistentDataContainer().set(this.key, PersistentDataType.BYTE, (byte) 1);
		
		this.runTaskTimer(SpigotAPI.getInstance(), delay, period);
		
	}
	
	public void stop() {
		
		player.getPersistentDataContainer().remove(this.key);
		
		this.cancel();
		
	}
	
	public abstract void onRun();
	
	@Override
	public void run() {
		
		onRun();
		
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}
	
	public NamespacedKey getKey() {
		return key;
	}
	
	public void setKey(NamespacedKey key) {
		this.key = key;
	}
	
	public static boolean hasCooldown(final Player player, final String key) {
		
		return player.getPersistentDataContainer().has(new NamespacedKey(SpigotAPI.getInstance(), key), PersistentDataType.BYTE);
		
	}

}
