package fr.frivec.api.bungeecord.redis;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.bungeecord.servers.HubChooser;
import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.message.PlayerMessage;
import fr.frivec.api.core.player.message.PrivateMessage;
import fr.frivec.api.core.player.redisdata.ChangeServerMessage;
import fr.frivec.api.core.sanction.Ban;
import fr.frivec.api.core.sanction.Kick;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.messaging.KickPlayerMessage;
import fr.frivec.api.core.sanction.messaging.SanctionMessage;
import fr.frivec.api.core.sanction.messaging.SanctionMessage.SanctionMessageType;
import fr.frivec.api.core.sanction.type.SanctionType;
import fr.frivec.api.core.utils.Utils;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.messages.SpectreReloadMessage;
import fr.frivec.core.redis.pubsub.PubSubListener;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BungeeRedisListener extends PubSubListener {
	
	private final GsonManager gsonManager;
	private final ServerInformations informations;
	
	public BungeeRedisListener() {
		
		this.gsonManager = GsonManager.getInstance();
		this.informations = ProxyAPI.getInstance().getInformations();
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onMessage(CharSequence channel, String msg) {
		
		if(channel.equals("Spectre")) {

			final SpectreMessage message = (SpectreMessage) this.gsonManager.deSeralizeJson(msg, SpectreMessage.class);
			final SpectreMessageType messageType = message.getType();
			
			if(messageType.equals(SpectreMessageType.STOP_SERVER)) {
				
				if(!message.getId().toString().equals(this.informations.getId().toString()))

					return;
				
				ProxyServer.getInstance().getScheduler().schedule(ProxyAPI.getInstance(), () -> {

					for(ProxiedPlayer players : ProxyServer.getInstance().getPlayers())

						players.disconnect(new TextComponent("§cDésolé, ce proxy a été fermé et aucun remplacement n'a été trouvé."));

					ProxyServer.getInstance().stop("Proxy stopped by Spectre.");

				}, 1, TimeUnit.SECONDS);

			}else if(messageType.equals(SpectreMessageType.REGISTER_SERVER)) {

				if(message.getCurrentProxyID() == null || !message.getCurrentProxyID().toString().equals(this.informations.getId().toString()))

					return;
				
				ProxyAPI.log(LogLevel.INFO, "Server registered: Server Name: " + message.getServerName() + " | Server port: " + message.getPort());

				final ServerInfo info = ProxyServer.getInstance().constructServerInfo(message.getServerName(), new InetSocketAddress("0.0.0.0", message.getPort()), "§bServer " + message.getServerName() + " | PROXY: " + this.informations.getName(), false);

				ProxyServer.getInstance().getServers().put(message.getServerName(), info);
				ProxyAPI.getInstance().getInformations().getServersInProxy().add(message.getId());
				
				if(message.getServerType().equals(ServerType.HUB))
				
					ProxyAPI.getInstance().getHubChooser().addHub(message.getId(), info);

			}else if(Utils.isBungee && messageType.equals(SpectreMessageType.UNREGISTER_SERVER)) {

				if(message.getCurrentProxyID() == null || !message.getCurrentProxyID().toString().equals(this.informations.getId().toString()) || !ProxyAPI.getInstance().getInformations().getServersInProxy().contains(message.getId()))

					return;
				
				final ServerInfo server = ProxyServer.getInstance().getServersCopy().get(message.getServerName());
				
				ProxyAPI.log(LogLevel.INFO, "Server unregistered: Server Name: " + message.getServerName() + " | Server port: " + message.getPort());
				
				/*
				 * Define a new hub as current hub if this server was the hub used
				 */
				
				if(message.getServerType().equals(ServerType.HUB))
					
					ProxyAPI.getInstance().getHubChooser().removeHub(message.getId());
				
				/*
				 * Redirect all players from the server to the hub
				 */
				
				final HubChooser chooser = ProxyAPI.getInstance().getHubChooser();
				ServerInfo newHub = null; 
				
				if(!chooser.getHubs().isEmpty()) {
					
					chooser.chooseHub();
					newHub = chooser.getCurrentHub();
					
				}
					
				for(ProxiedPlayer players : server.getPlayers())
					
					if(newHub != null)
						
						players.connect(newHub);
				
					else
						
						players.disconnect(new TextComponent("§cDésolé, ce serveur a été fermé et aucun remplacement n'a été trouvé."));
				
				ProxyServer.getInstance().getServers().remove(message.getServerName());
				ProxyAPI.getInstance().getInformations().getServersInProxy().remove(message.getId());

			}else if(messageType.equals(SpectreMessageType.ERROR)) {

				//TODO manage errors

			}
			
		}else if(channel.equals("Sanction")) {
			
			/*
			 * Sanction messaging
			 * Add/Remove/Update sanctions, send/update/delete history
			 * 
			 */
			
			final SanctionMessage message = (SanctionMessage) this.gsonManager.deSeralizeJson(msg, SanctionMessage.class);
			
			final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(message.getMod());
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(message.getMod()),
							localTarget = LocalPlayer.getLocalPlayer(message.getTarget());
			
			if(player == null)
				
				/*
				 * If the mod player is null, we cancel the message to prevent multiple executions of the code.
				 */
				
				return;
			
			if(message.getType().equals(SanctionMessageType.ADD_SANCTION)) {
				
				/*
				 * Add a new sanction
				 */
									
				if(localTarget != null) {
					
					final Sanction sanction = message.getSanction();
					final SanctionType type = sanction.getSanctionType();
					
					if(type.equals(SanctionType.BAN)) {

						final Ban ban = (Ban) sanction;
						
						if(localTarget.isBan()) {
							
							player.sendMessage(new TextComponent("§cErreur. Le joueur " + localTarget.getName() + " est déjà banni."));

						}else {
								
							ban.create();
							ban.applySanction(localTarget);
							
							player.sendMessage(new TextComponent("§aLe joueur " + localTarget.getName() + " a bien été banni du serveur !"));

						}
						
					}else if(type.equals(SanctionType.MUTE)) {
						
						if(localTarget.isMute()) {
							
							player.sendMessage(new TextComponent("§cErreur. Le joueur " + localTarget.getName() + " est déjà mute."));

						}else {

							final Mute mute = (Mute) sanction;
								
							mute.create();
							mute.applySanction(localTarget);
								
							player.sendMessage(new TextComponent("§aLe joueur " + localTarget.getName() + " a bien été mute !"));

						}
						
					}else if(type.equals(SanctionType.KICK)) {

						final Kick kick = (Kick) sanction;
							
						kick.create();
						kick.applySanction(localTarget);
							
						player.sendMessage(new TextComponent("§aLe joueur " + localTarget.getName() + " a bien été éjecté du serveur !"));

					}

				}else {
					
					player.sendMessage(new TextComponent("§cErreur. Le joueur que vous souhaitez sanctionner ne s'est jamais connecté sur le serveur."));

				}
				
			}else if(message.getType().equals(SanctionMessageType.REMOVE_SANCTION)) {

				assert localPlayer != null;

				if(localPlayer.getCurrentProxy().equals(ProxyAPI.getInstance().getInformations().getId())) {
					
					if(localTarget == null) {
						
						player.sendMessage(new TextComponent("§cErreur. Le joueur que vous avez demandé ne s'est jamais connecté sur le serveur."));
						
						return;
						
					}
					
					if(message.getSanction().getSanctionType().equals(SanctionType.BAN)) {
						
						//Remove a ban
						
						if(localTarget.isBan()) {
							
							final Sanction sanction = Sanction.getSanction(localTarget.getUuid(), SanctionType.BAN, -1);
							
							localTarget.setBan(false);
							localTarget.sendInRedis();
							localTarget.save(true);
							
							sanction.setFinished(true);
							sanction.saveInDatabase(true);
							
							player.sendMessage(new TextComponent("§aLe joueur " + localTarget.getName() + " a bien été dé-banni."));

						}else {
							
							player.sendMessage(new TextComponent("§cErreur. Le joueur " + localTarget.getName() + " n'est pas banni du serveur."));

						}

						
					}else if(message.getSanction().getSanctionType().equals(SanctionType.MUTE)) {
						
						//Remove a mute
						
						if(localTarget.isMute()) {
							
							final Sanction sanction = Sanction.getSanction(localTarget.getUuid(), SanctionType.MUTE, -1);
							
							localTarget.setMute(false);
							localTarget.sendInRedis();
							localTarget.save(true);
							
							sanction.setFinished(true);
							sanction.saveInDatabase(true);
							
							player.sendMessage(new TextComponent("§aLe joueur " + localTarget.getName() + " a bien été dé-mute."));

						}else {
							
							player.sendMessage(new TextComponent("§cErreur. Le joueur " + localTarget.getName() + " n'est pas mute sur le serveur."));

						}
						
					}
					
				}
				
			}
			
		}else if(channel.equals("PrivateMessage")) {
			
			final PrivateMessage message = (PrivateMessage) this.gsonManager.deSeralizeJson(msg, PrivateMessage.class);
			final ServerInformations serverInfo = ProxyAPI.getInstance().getInformations();
			
			if(message.getProcessUUID().equals(serverInfo.getProxyID())) {
				
				final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(message.getReceiver());
				
				player.sendMessage(new TextComponent(message.getMessage()));

			}
			
		}else if(channel.equals("KickPlayer")) {
			
			final KickPlayerMessage kickPlayerMessage = (KickPlayerMessage) this.gsonManager.deSeralizeJson(msg, KickPlayerMessage.class);
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(kickPlayerMessage.getPlayer());
			
			if(localPlayer != null && localPlayer.isOnline())
				
				ProxyServer.getInstance().getPlayer(kickPlayerMessage.getPlayer()).disconnect(new TextComponent(String.join("\n", kickPlayerMessage.getMessage())));
			
		}else if(channel.equals("SpectreReload")) {
			
			/*
			 * Update informations (motd, maxSize) with new informations from Spectre
			 */
			
			final ServerInformations infos = ProxyAPI.getInstance().getInformations();
			final ServerType serverType = infos.getServerType();
			final SpectreReloadMessage message = (SpectreReloadMessage) GsonManager.getInstance().deSeralizeJson(msg, SpectreReloadMessage.class);
		
			infos.setMaxSize(message.getMaxSize().get(serverType));
			infos.setMotd(message.getMotd().get(serverType));
			
			ProxyAPI.getInstance().setInformations(infos);
			
			final Path root = Paths.get("").toAbsolutePath(), informationsFile = Paths.get(root + File.separator + "spectreInformations.json");
			
			try {
			
				final BufferedWriter writer = Files.newBufferedWriter(informationsFile);
				
				writer.write(GsonManager.getInstance().serializeObject(infos));
				writer.flush();
				writer.close();
			
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}else if(channel.equals("SendPlayer")) {
			
			/*
			 * Channel to send players on a specific server or on a random hub
			 */
			final ChangeServerMessage serverMessage = (ChangeServerMessage) this.gsonManager.deSeralizeJson(msg, ChangeServerMessage.class);
			
			if(serverMessage.getProxyUUID().equals(this.informations.getId())) {
				
				final UUID serverUUID = serverMessage.getServerUUID();
				final ProxiedPlayer player = ProxyServer.getInstance().getPlayer(serverMessage.getPlayerUUID());
				
				if(serverUUID != null)
					
					player.connect(ProxyAPI.getInstance().getServerInfo(serverUUID));
					
				else {
					
					final HubChooser chooser = ProxyAPI.getInstance().getHubChooser();
					
					chooser.chooseHub();
					player.connect(chooser.getCurrentHub());
					
				}
				
				if(serverMessage.getMessage() != null)
					
					player.sendMessage(new TextComponent(serverMessage.getMessage()));
				
			}
			
		}else if(channel.equals("PlayerMessage")) {

			final PlayerMessage playerMessage = (PlayerMessage) this.gsonManager.deSeralizeJson(msg, PlayerMessage.class);

			if(this.informations.getId().equals(playerMessage.getProcessID()))

				ProxyServer.getInstance().getPlayer(playerMessage.getReceiver()).sendMessage(new TextComponent(playerMessage.getMessage()));

		}
		
	}

}
