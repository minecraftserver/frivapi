package fr.frivec.api.bungeecord.commands;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.core.process.LocalProcessUtils;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HubCommand extends Command {
	
	public HubCommand() {

		super("hub");
	
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(sender instanceof ProxiedPlayer) {
			
			final ProxiedPlayer player = (ProxiedPlayer) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			final ServerInformations currentServer = LocalProcessUtils.getProcessInformation(localPlayer.getCurrentServer());
			
			if(currentServer.getServerType().equals(ServerType.HUB))
				
				return;
			
			else { 
				
				if(!ProxyAPI.getInstance().getHubChooser().chooseHub()) {
					
					player.sendMessage(new TextComponent("§cUne erreur est survenue lors de votre redirection vers un hub."));
					player.sendMessage(new TextComponent("§cVeuillez réessayer."));
					player.sendMessage(new TextComponent(""));
					player.sendMessage(new TextComponent("§3Erreur: §cAucun serveur n'a été trouvé."));
					
					return;
					
				}
				
				final ServerInfo hub = ProxyAPI.getInstance().getHubChooser().getCurrentHub();
				
				player.sendMessage(new TextComponent("§aRetour au hub..."));
				player.connect(hub);
				
				return;
				
			}
			
		}else {
			
			sender.sendMessage(new TextComponent("§cErreur. Cette commande n'est utilisable qu'avec un compte joueur."));
			
			return;
			
		}
		
	}

}
