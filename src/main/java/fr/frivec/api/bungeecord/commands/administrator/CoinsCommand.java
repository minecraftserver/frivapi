package fr.frivec.api.bungeecord.commands.administrator;

import java.util.UUID;

import fr.frivec.api.core.mojang.MojangRequest;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CoinsCommand extends Command {
	
	public CoinsCommand() {
		
		super("coins", "", new String[] {"pieces"});
		
		this.setPermissionMessage("§cErreur. Vous n'avez pas la permission d'utiliser cette commande.");
		
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(sender instanceof ProxiedPlayer) {
			
			final ProxiedPlayer player = (ProxiedPlayer) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() != RankList.ADMIN.getPower()) {
				
				player.sendMessage(new TextComponent(this.getPermissionMessage()));
				
				return;
				
			}
			
		}
		
		if(args.length >= 3) {
			
			if(args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("remove")) {
				
				final UUID uuid = MojangRequest.getUUIDFromMojang(args[1]);
				
				if(uuid == null) {
					
					sender.sendMessage(new TextComponent("§cErreur. Impossible de retrouver le joueur que vous avez demandé."));
					return;
					
				}
				
				final LocalPlayer target = LocalPlayer.getLocalPlayer(uuid);
				
				try {
					
					final int amount = Integer.parseInt(args[2]);
					
					if(target != null) {
						
						if(args[0].equalsIgnoreCase("add"))
						
							target.setCoins(target.getCoins() + amount);
						
						else
							
							target.setCoins(target.getCoins() - amount);
						
						sender.sendMessage(new TextComponent("§aVous avez bien " + (args[0].equalsIgnoreCase("add") ? "ajouté" : "retiré") + " §6" + amount + " §3pièces §aà §6" + target.getName() + "§a."));
						
						if(target.isOnline())
							
							target.sendInRedis();
						
						else {
						
							target.save(true);
							target.removeFromRedis();
							
						}
						
						return;
						
					}else {
						
						sender.sendMessage(new TextComponent("§cIl semblerait que le joueur que vous semblez vouloir modifier n'existe pas ou ne s'est jamais connecté sur le serveur."));
						
						return;
						
					}
					
				} catch (NumberFormatException e) {
					
					sender.sendMessage(new TextComponent("§cle montant indiqué n'est pas une valeur chiffrée entière."));
					
					return;
					
				}
				
			}else {
				
				sender.sendMessage(new TextComponent("§cErreur. La commande est incomplète."));
				sender.sendMessage(new TextComponent("§bUtilisation: " + "/coins <add/remove> <Joueur> <Montant>"));
				
				return;
				
			}
			
		}else {
			
			sender.sendMessage(new TextComponent("§cErreur. La commande est incomplète."));
			sender.sendMessage(new TextComponent("§bUtilisation: " + "/coins <add/remove> <Joueur> <Montant>"));
			
			return;
			
		}
		
	}

}
