package fr.frivec.api.bungeecord;

import com.mongodb.client.MongoCollection;
import fr.frivec.api.bungeecord.commands.HubCommand;
import fr.frivec.api.bungeecord.commands.administrator.CoinsCommand;
import fr.frivec.api.bungeecord.commands.administrator.EmeraldCommand;
import fr.frivec.api.bungeecord.database.providers.HubPlayerProvider;
import fr.frivec.api.bungeecord.database.providers.JumpProvider;
import fr.frivec.api.bungeecord.database.providers.LocalPlayerProvider;
import fr.frivec.api.bungeecord.listeners.ListenerManager;
import fr.frivec.api.bungeecord.listeners.player.ChatListener;
import fr.frivec.api.bungeecord.listeners.player.PlayerConnectListener;
import fr.frivec.api.bungeecord.listeners.server.PingListener;
import fr.frivec.api.bungeecord.listeners.server.ServerChangeListener;
import fr.frivec.api.bungeecord.redis.BungeeRedisListener;
import fr.frivec.api.bungeecord.servers.HubChooser;
import fr.frivec.api.core.hub.player.HubPlayer;
import fr.frivec.api.core.logger.Logger;
import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.player.redisdata.LocalPlayerRedisData;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.adapter.SanctionAdapter;
import fr.frivec.api.core.servers.status.ServerStatus;
import fr.frivec.api.core.utils.JsonReader;
import fr.frivec.api.core.utils.Utils;
import fr.frivec.core.Credentials;
import fr.frivec.core.database.Database;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.Redis;
import fr.frivec.core.redis.pubsub.PubSubManager;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.redisson.api.RMap;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;

public class ProxyAPI extends Plugin {

	private static ProxyAPI instance;

	private Logger logger;

	private GsonManager gsonManager;
	private ListenerManager listenerManager;
	private HubChooser hubChooser;
	private LocalPlayerRedisData redisData;

	//Redis
	private Redis redis;
	private BungeeRedisListener redisListener;
	private PubSubManager channelManager;
	
	//Database
	private Database database;

	private ServerInformations informations;
	private UUID proxyUUID;

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {

		instance = this;
		Utils.isBungee = true;

		/*
		 * Load managers
		 *
		 */
		
		this.logger = new Logger(this);
		this.gsonManager = new GsonManager(Arrays.asList(new SanctionAdapter()), Arrays.asList(Sanction.class));
		this.listenerManager = new ListenerManager(this);
		this.hubChooser = new HubChooser(20);
		this.redisData = new LocalPlayerRedisData();

		/*
		 * Register listeners
		 *
		 */
		
//		this.listenerManager.registerListener(new DevListener());
		this.listenerManager.registerListener(new PlayerConnectListener());
		this.listenerManager.registerListener(new ChatListener());
		this.listenerManager.registerListener(new PingListener());
		this.listenerManager.registerListener(new ServerChangeListener());
		
		/*
		 * Register commands
		 */
		
		this.getProxy().getPluginManager().registerCommand(this, new HubCommand());
		this.getProxy().getPluginManager().registerCommand(this, new CoinsCommand());
		this.getProxy().getPluginManager().registerCommand(this, new EmeraldCommand());

		/*
		 * Load proxies informations from Spectre
		 *
		 */
		
		final Path root = Paths.get("").toAbsolutePath(), informationsFile = Paths.get(root.toString() + File.separator + "spectreInformations.json");
		final String json = JsonReader.readJson(informationsFile);

		if(json != null) {

			this.informations = (ServerInformations) this.gsonManager.deSeralizeJson(json, ServerInformations.class);
			this.proxyUUID = this.informations.getProxyID();

		}else {
			
			/*
			 * Stop the server. The plugin will not be able to work without these informations.
			 */
			
			log(LogLevel.SEVERE, "Cannot read server informations' file. Cannot launch the process.");
			
			this.getProxy().stop();
			
			return;
			
		}

		/*
		 * Redis initialisation
		 *
		 */
		
		this.redis = new Redis(new Credentials("localhost", "ProxyAPI", "t3iw32ZQA3", 6379));
		
		if(this.redis != null)
			
			log(LogLevel.INFO, "Connected to Redis");
		
		else {
			
			log(LogLevel.SEVERE, "Unable to connect to Redis. Stopping the server...");
			
			this.getProxy().stop();
			
			return;
			
		}
		
		this.redisListener = new BungeeRedisListener();
		this.channelManager = new PubSubManager(this.redisListener);
		
		this.channelManager.subscribe("Spectre");
		this.channelManager.subscribe("Sanction");
		this.channelManager.subscribe("PlayerMessaging");
		this.channelManager.subscribe("KickPlayer");
		this.channelManager.subscribe("SpectreReload");
		this.channelManager.subscribe("SendPlayer");
		this.channelManager.subscribe("PlayerMessage");

		/*
		 * Database initialisation
		 * user: API, pwd : PAh,633/ndF]
		 */
		
		this.database = new Database("localhost", "MinecraftServer", CodecRegistries.fromProviders(new HubPlayerProvider(), new LocalPlayerProvider(), new JumpProvider()));
		
		if(this.database != null)
			
			log(LogLevel.INFO, "Connected to Database");
		
		else {
			
			log(LogLevel.SEVERE, "Unable to connect to database. Stopping the server...");
			
			this.getProxy().stop();
			
			return;
			
		}

		/*
		 * Clear server list and priorities list
		 *
		 */
		
		this.getProxy().getServers().clear();
		
		/*
		 * Update server status
		 */
		
		updateServerStatus(ServerStatus.OPEN);

		super.onEnable();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onDisable() {
		
		updateServerStatus(ServerStatus.CLOSING);
		
		if(this.redis == null || this.database == null)
			
			return;
		
		/*
		 * Remove the proxy from list of process in redis server
		 */
		
		this.getRedisData().deleteAllData();
		this.getRedis().getClient().getMap(ServerInformations.redisKey).remove(this.informations.getId().toString());
		
		/*
		 * close all servers connected to the proxy
		 */
		
		for(UUID uuid : this.informations.getServersInProxy()) {
			
			final SpectreMessage message = new SpectreMessage(SpectreMessageType.STOP_SERVER, null, uuid, this.informations.getId(), null, null, 0, 0, 0, false);
			
			this.channelManager.publish("Spectre", this.getGsonManager().serializeObject(message));
			
		}
		
		while(!this.getProxy().getServersCopy().isEmpty()) {
			
			try {
			
				Thread.sleep(500);
			
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		this.channelManager.unsubscribe("Spectre");
		this.channelManager.unsubscribe("Sanction");
		this.channelManager.unsubscribe("PlayerMessaging");
		this.channelManager.unsubscribe("KickPlayer");
		this.channelManager.unsubscribe("SpectreReload");
		this.channelManager.unsubscribe("SendPlayer");
		this.channelManager.unsubscribe("PlayerMessage");

		updateServerStatus(ServerStatus.CLOSE);
		
		this.redis.close();
		
		this.getProxy().getServers().clear(); //Clear the servers list

		super.onDisable();
	}
	
	public ServerInfo getServerInfo(final UUID serverUUID) {
		
		final String serverName = ServerInformations.getInfos(this.getRedis().getClient(), serverUUID).getName();
		
		return ProxyServer.getInstance().getServerInfo(serverName);
		
	}
	
	public void updateServerStatus(final ServerStatus serverStatus) {
		
		this.informations.setServerStatus(serverStatus.toString());
		
		final RMap<String, String> rMap = this.getRedis().getClient().getMap(ServerInformations.redisKey);
		
		rMap.replace(this.informations.getId().toString(), this.getGsonManager().serializeObject(this.informations));
		
	}

	public static ProxyAPI getInstance() {
		return instance;
	}

	public UUID getProxyUUID() {
		return proxyUUID;
	}

	public Redis getRedis() {
		return redis;
	}

	public GsonManager getGsonManager() {
		return gsonManager;
	}
	
	public HubChooser getHubChooser() {
		return hubChooser;
	}

	public ServerInformations getInformations() {
		return informations;
	}
	
	public void setInformations(ServerInformations informations) {
		this.informations = informations;
	}
	
	public Database getDatabase() {
		return database;
	}
	
	public LocalPlayerRedisData getRedisData() {
		return redisData;
	}
	
	public PubSubManager getChannelManager() {
		return channelManager;
	}
	
	public MongoCollection<LocalPlayer> getLocalPlayerCollection() {
		
		return this.database.getDatabase().getCollection("localPlayers", LocalPlayer.class);
		
	}
	
	public MongoCollection<HubPlayer> getHubPlayerCollection() {
		
		return this.database.getDatabase().getCollection("hubPlayer", HubPlayer.class);
		
	}
	
	public MongoCollection<Document> getSanctionCollection() {
		
		return this.database.getCollection("sanctions");
		
	}

	public static void log(final LogLevel level, final String message) {

		ProxyAPI.getInstance().logger.log(level, message);

	}

}
