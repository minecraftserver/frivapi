package fr.frivec.api.bungeecord.listeners;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

public class ListenerManager {
	
	private Plugin plugin;
	
	/**
	 * Class that register all the listeners of the server.
	 * @param javaPlugin: the class extends JavaPlugin
	 */
	public ListenerManager(final Plugin plugin) {
		
		this.plugin = plugin;
		
	}
	
	/**
	 * Register a listener on the api plugin. Use it to regroup all the listeners on one plugin.
	 * @param listener: Listener you want to register
	 */
	public void registerListener(final Listener listener) {
		
		this.plugin.getProxy().getPluginManager().registerListener(this.plugin, listener);
		
	}

}
