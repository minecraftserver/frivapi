package fr.frivec.api.bungeecord.listeners.player;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.type.SanctionType;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener implements Listener {
	
	@EventHandler
	public void onChat(final ChatEvent event) {
		
		final ProxiedPlayer player = (ProxiedPlayer) event.getSender();
		final UUID uuid = player.getUniqueId();
		
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(uuid);
		
		if(event.isCommand())
			
			return;
		
		/*
		 * Check if the player's mute is finished
		 */
		
		if(localPlayer.isMute()) {
			
			final Mute mute = (Mute) Sanction.getSanction(uuid, SanctionType.MUTE, -1);
			
			if(!mute.isPermanent() && mute.getEnd() <= Date.from(Instant.now()).getTime()) {
				
				mute.setFinished(true);
				mute.saveInDatabase(true);
				
				localPlayer.setMute(false);
				localPlayer.sendInRedis();
				
			}
			
		}
		
		/*
		 * Remove the message if the player is mute
		 */
		
		if(localPlayer.isMute()) {
			
			player.sendMessage(new TextComponent("§cVous êtes mute. Vous ne pouvez pas discuter dans le chat global ni en messages privés."));
			
			event.setCancelled(true);
			
			return;
			
		}else
			
			return;
		
	}

}
