package fr.frivec.api.bungeecord.listeners.dev;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.logger.Logger.LogLevel;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ReconnectHandler;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class DevListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(final PostLoginEvent event) {
		
		final ServerInfo info = ProxyServer.getInstance().getServerInfo("HUB-1P");
		
		ProxyAPI.log(LogLevel.INFO, "Server Name: " + info.getName() + " | server port: " + info.getAddress().getPort());
		
		ProxyServer.getInstance().setReconnectHandler(new ReconnectHandler() {
			
			@Override
			public void setServer(ProxiedPlayer player) {}
			
			@Override
			public void save() {}
			
			@Override
			public ServerInfo getServer(ProxiedPlayer player) {
				
				return info;
				
			}
			
			@Override
			public void close() {}
			
		});
		
	}

}
