package fr.frivec.api.bungeecord.listeners.player;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RMap;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.api.core.hub.player.HubPlayer;
import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.sanction.Ban;
import fr.frivec.api.core.sanction.Mute;
import fr.frivec.api.core.sanction.Sanction;
import fr.frivec.api.core.sanction.type.SanctionType;
import fr.frivec.api.core.servers.status.ServerStatus;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.spectre.ServerInformations;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerConnectListener implements Listener {
	
	@EventHandler
	public void onPreLogin(final LoginEvent event) {
		
		final UUID uuid = event.getConnection().getUniqueId();
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(uuid);
		final ServerInformations infos = ProxyAPI.getInstance().getInformations();
		
		/*
		 * Check server status
		 */
		
		if(!infos.getServerStatus().equals(ServerStatus.OPEN.toString())) {
			
			event.getConnection().disconnect(new TextComponent("§cUne erreur est survenuie. Ce proxy est fermé. Merci de réessayer."));
			return;
			
		}
		
		if(localPlayer != null) {
			
			if(localPlayer.isBan()) {
				
				final Ban ban = (Ban) Sanction.getSanction(uuid, SanctionType.BAN, -1);
				
				if(!ban.isPermanent() && ban.getEnd() <= Date.from(Instant.now()).getTime()) {
					
					ban.setFinished(true);
					ban.saveInDatabase(true);
					
					localPlayer.setBan(false);
					localPlayer.sendInRedis();
					localPlayer.save(true);
					
				}else
					
					ban.kickPlayer(event.getConnection());
				
			}
			
			if(localPlayer.isMute()) {
				
				final Mute mute = (Mute) Sanction.getSanction(uuid, SanctionType.MUTE, -1);
				
				if(!mute.isPermanent() && mute.getEnd() <= Date.from(Instant.now()).getTime()) {
					
					mute.setFinished(true);
					mute.saveInDatabase(true);
					
					localPlayer.setMute(false);
					localPlayer.sendInRedis();
					localPlayer.save(true);
					
				}
				
			}
			
		}
		
		return;
		
	}
	
	@EventHandler
	public void onConnect(final PostLoginEvent event) {
		
		final ProxiedPlayer player = event.getPlayer();
		
		/*
		 * Choose a hub for the connection. If the hub is null, we disconnect the player
		 */
		
		if(!ProxyAPI.getInstance().getHubChooser().chooseHub()) {
			
			player.disconnect(new TextComponent("Aucun serveur n'a été trouvé pour assurer votre connexion. Veuillez réessayer."));
			
			return;
			
		}
		
		/*
		 * Check if player has already come on the server
		 */
		
		LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
		
		if(localPlayer == null) {
			
			localPlayer = new LocalPlayer(player.getUniqueId(), player.getName(), RankList.PLAYER, 100, 0, false, false, true, true, true);
			localPlayer.setHubPlayer(new HubPlayer(localPlayer, false, new ArrayList<>(), new ArrayList<>()));
			
			localPlayer.save(false);
			
			player.sendMessage(new TextComponent("§a§lBienvenue sur le serveur !"));
			player.sendMessage(new TextComponent("§6Clique sur la boussole pour choisir un jeu, sur ton profil pour obtenir plus d'informations et modifier tes paramètres !"));
			player.sendMessage(new TextComponent(""));
			player.sendMessage(new TextComponent("§3Bon jeu sur le serveur !"));
						
		}
		
		//Update the player's name if it changed
		if(!player.getName().equals(localPlayer.getName()))
			
			localPlayer.setName(player.getName());
		
		localPlayer.setOnline(true);
		localPlayer.setCurrentProxy(ProxyAPI.getInstance().getInformations().getId());
		localPlayer.sendInRedis();
		
		final Title title = ProxyServer.getInstance().createTitle();
		
		title.title(new TextComponent("§3Un serveur"));
		title.subTitle(new TextComponent("§abon jeu sur le serveur !"));
		
		title.send(player);
		
		if(ProxyAPI.getInstance().getRedisData().getPlayerDataToRemove().contains(localPlayer))
			
			ProxyAPI.getInstance().getRedisData().getPlayerDataToRemove().remove(localPlayer);
		
		/*
		 * Update the server informations
		 */
		
		final ServerInformations infos = ProxyAPI.getInstance().getInformations();
		final RMap<String, String> rMap = ProxyAPI.getInstance().getRedis().getClient().getMap(ServerInformations.redisKey);
		
		infos.setSize(ProxyServer.getInstance().getOnlineCount());
		rMap.replace(infos.getId().toString(), GsonManager.getInstance().serializeObject(infos));
		
	}
	
	@EventHandler
	public void onLeaveProxy(final PlayerDisconnectEvent event) {
		
		final ProxiedPlayer player = event.getPlayer();
		
		/*
		 * Update localplayer
		 * Delayed because the informations of LocalPlayer aren't updated at this moment.
		 */
			
		ProxyServer.getInstance().getScheduler().schedule(ProxyAPI.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
					
				localPlayer.setCurrentProxy(null);
				localPlayer.setCurrentServer(null);
				localPlayer.setOnline(false);
				localPlayer.getHubPlayer().setLocation(null);
				
				localPlayer.sendInRedis();
				
				localPlayer.save(true);
					
				ProxyAPI.getInstance().getRedisData().delayedDeleteData(localPlayer);
				
			}
			
		}, 500, TimeUnit.MILLISECONDS);
		
		/*
		 * Update the server informations
		 */
				
		final ServerInformations infos = ProxyAPI.getInstance().getInformations();
		final RMap<String, String> rMap = ProxyAPI.getInstance().getRedis().getClient().getMap(ServerInformations.redisKey);
		
		infos.setSize(ProxyServer.getInstance().getOnlineCount() - 1);//- 1 because the size isn't update when this event is triggered
		rMap.replace(infos.getId().toString(), GsonManager.getInstance().serializeObject(infos));
		
	}

}
