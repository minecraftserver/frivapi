package fr.frivec.api.bungeecord.listeners.server;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.core.spectre.ServerInformations;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Objects;

public class ServerChangeListener implements Listener {
	
	@EventHandler
	public void onServerChange(final ServerConnectEvent event) {
		
		final ProxiedPlayer player = event.getPlayer();
		final Server currentServer = player.getServer();
		final ServerInfo targetServer = event.getTarget();
		final ServerInformations infos = ServerInformations.getInfos(ProxyAPI.getInstance().getRedis().getClient(), targetServer.getName());

		switch (Objects.requireNonNull(infos).getServerStatus()) {

			case "OPEN" -> {}
			case "CLOSE" -> {

				final String message = "§cCe serveur est actuellement fermé. Impossible de le rejoindre.";

				if (currentServer == null)

					player.disconnect(new TextComponent(message));

				else {

					event.setCancelled(true);
					player.sendMessage(new TextComponent(message));

				}

			}
			case "CLOSING" -> {

				final String message = "§cCe serveur est actuellement en procédure d'arrêt. Impossible de le rejoindre.";

				if (currentServer == null)

					player.disconnect(new TextComponent(message));

				else {

					event.setCancelled(true);
					player.sendMessage(new TextComponent(message));

				}

			}
			case "MAP_LOADING" -> {

				final String message = "§6Ce serveur génère sa carte de jeu. Impossible de le rejoindre.";

				if (currentServer == null)

					player.disconnect(new TextComponent(message));

				else {

					event.setCancelled(true);
					player.sendMessage(new TextComponent(message));

				}

			}
			default -> {
			}
		}
		
	}

}
