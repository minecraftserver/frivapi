package fr.frivec.api.bungeecord.listeners.server;

import fr.frivec.api.bungeecord.ProxyAPI;
import fr.frivec.core.spectre.ServerInformations;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingListener implements Listener {
	
	@EventHandler
	public void onPing(final ProxyPingEvent event) {
		
		final ServerInformations infos = ProxyAPI.getInstance().getInformations();
		final ServerPing ping = event.getResponse(); //Get the response of the proxy in server list
		
		ping.setDescriptionComponent(new TextComponent(infos.getMotd()[0] + infos.getMotd()[1]));
		ping.getVersion().setName("Test version name");
		ping.getPlayers().setMax(infos.getMaxSize());
		
	}

}
