package fr.frivec.api.bungeecord.database.providers;

import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import fr.frivec.api.bungeecord.database.codecs.hubplayer.CosmeticSlotSerializer;
import fr.frivec.api.bungeecord.database.codecs.hubplayer.HubPlayerCodec;
import fr.frivec.api.core.hub.cosmetics.CosmeticSlot;
import fr.frivec.api.core.hub.player.HubPlayer;

public class HubPlayerProvider implements CodecProvider {

	@SuppressWarnings("unchecked")
	@Override
	public <T> Codec<T> get(Class<T> type, CodecRegistry registry) {
		
		if(type.equals(HubPlayer.class))
			
			return (Codec<T>) new HubPlayerCodec(registry);
		
		else if(type.equals(CosmeticSlot.class))
			
			return (Codec<T>) new CosmeticSlotSerializer();
		
		return null;
	}
	
}
