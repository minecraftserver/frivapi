package fr.frivec.api.bungeecord.database.codecs.jump;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import fr.frivec.api.core.hub.jump.Time;

public class TimeCodec implements Codec<Time> {

	@Override
	public void encode(BsonWriter writer, Time value, EncoderContext encoderContext) {
		
		writer.writeStartDocument();
		
		writer.writeInt32("minutes", value.getMinutes());
		writer.writeInt32("seconds", value.getSeconds());
		writer.writeInt32("milliseconds", value.getMilliseconds());
		
		writer.writeEndDocument();
		
	}

	@Override
	public Class<Time> getEncoderClass() {
		return Time.class;
	}

	@Override
	public Time decode(BsonReader reader, DecoderContext decoderContext) {
		
		final Time time = new Time(0, 0, 0);
		
		reader.readStartDocument();
		
		time.setMinutes(reader.readInt32("minutes"));
		time.setSeconds(reader.readInt32("seconds"));
		time.setMilliseconds(reader.readInt32("milliseconds"));
		
		reader.readEndDocument();
		
		return time;
	}
	
}
