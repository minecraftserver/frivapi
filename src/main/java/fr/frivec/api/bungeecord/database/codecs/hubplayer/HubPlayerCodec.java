package fr.frivec.api.bungeecord.database.codecs.hubplayer;

import java.util.ArrayList;
import java.util.UUID;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;

import fr.frivec.api.core.hub.cosmetics.CosmeticSlot;
import fr.frivec.api.core.hub.jump.JumpStatistic;
import fr.frivec.api.core.hub.player.HubPlayer;

public class HubPlayerCodec implements Codec<HubPlayer> {
	
	private final CodecRegistry codecRegistry;
	
	public HubPlayerCodec(final CodecRegistry codecRegistry) {
		
		this.codecRegistry = codecRegistry;
		
	}

	@Override
	public void encode(BsonWriter writer, HubPlayer value, EncoderContext encoderContext) {
		
		writer.writeStartDocument();
		
		final Codec<CosmeticSlot> slotCodec = this.codecRegistry.get(CosmeticSlot.class);
		
		writer.writeName("_id");
		writer.writeString(value.getUuid().toString());
		
		writer.writeName("invisibilityPowder");
		writer.writeBoolean(value.hasInvisibilityPowder());
		
		writer.writeName("surprise");
		writer.writeBoolean(value.hasSurprise());
		
		writer.writeName("jumpSurprise");
		writer.writeBoolean(value.hasJumpSurprise());
		
		writer.writeName("jumpStatistics");
		encoderContext.encodeWithChildContext(this.codecRegistry.get(JumpStatistic.class), writer, value.getJumpStatistics());
		
		writer.writeName("ownedCosmetics");
		writer.writeStartArray();
		
		for(int id : value.getOwnedCosmetics())
			
			writer.writeInt32(id);
		
		writer.writeEndArray();
		
		writer.writeName("inventorySave");
		writer.writeStartArray();
		
		for(CosmeticSlot slots : value.getInventorySave())
			
			encoderContext.encodeWithChildContext(slotCodec, writer, slots);
		
		writer.writeEndArray();
		
		writer.writeEndDocument();
		
	}

	@Override
	public Class<HubPlayer> getEncoderClass() {
		return HubPlayer.class;
	}

	@Override
	public HubPlayer decode(BsonReader reader, DecoderContext decoderContext) {
	
		final HubPlayer hubPlayer = new HubPlayer(null, false, null, null);
		final Codec<CosmeticSlot> codec = this.codecRegistry.get(CosmeticSlot.class);
		
		reader.readStartDocument();
		
		reader.readName();
		
		final String stringID = reader.readString();
		
		hubPlayer.setUuid(UUID.fromString(stringID));
		
		reader.readName();
		hubPlayer.setInvisibilityPowder(reader.readBoolean());
		
		reader.readName();
		hubPlayer.setSurprise(reader.readBoolean());
		
		reader.readName();
		hubPlayer.setJumpSurprise(reader.readBoolean());
		
		reader.readName();
		hubPlayer.setJumpStatistics(decoderContext.decodeWithChildContext(this.codecRegistry.get(JumpStatistic.class), reader));
		
		reader.readName();
		final ArrayList<Integer> ownedCosmetics = new ArrayList<>();
		
		reader.readStartArray();
		
		while(reader.readBsonType().equals(BsonType.INT32))
			
			ownedCosmetics.add(reader.readInt32());
		
		reader.readEndArray();
		
		hubPlayer.setOwnedCosmetics(ownedCosmetics);
		
		reader.readName();
		
		final ArrayList<CosmeticSlot> inventorySave = new ArrayList<>();
		
		reader.readStartArray();
		
		while(!reader.readBsonType().equals(BsonType.END_OF_DOCUMENT))
			
			inventorySave.add(decoderContext.decodeWithChildContext(codec, reader));
		
		reader.readEndArray();
		
		hubPlayer.setInventorySave(inventorySave);
		
		reader.readEndDocument();
		
		return hubPlayer;
	}
	
}
