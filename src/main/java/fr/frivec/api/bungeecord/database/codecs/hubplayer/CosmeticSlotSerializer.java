package fr.frivec.api.bungeecord.database.codecs.hubplayer;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import fr.frivec.api.core.hub.cosmetics.CosmeticSlot;

public class CosmeticSlotSerializer implements Codec<CosmeticSlot> {

	@Override
	public void encode(BsonWriter writer, CosmeticSlot value, EncoderContext encoderContext) {
		
		writer.writeStartDocument();
		
		writer.writeInt32("slot", value.getSlot());
		writer.writeInt32("cosmeticID", value.getCosmeticID());
		
		writer.writeEndDocument();
		
	}

	@Override
	public Class<CosmeticSlot> getEncoderClass() {
		
		return CosmeticSlot.class;
	}

	@Override
	public CosmeticSlot decode(BsonReader reader, DecoderContext decoderContext) {
		
		final CosmeticSlot container = new CosmeticSlot(0, 0);
		
		reader.readStartDocument();
		
		container.setSlot(reader.readInt32("slot"));
		container.setCosmeticID(reader.readInt32("cosmeticID"));
		
		reader.readEndDocument();
		
		return container;
	}
	
	
	
}
