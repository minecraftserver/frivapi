package fr.frivec.api.bungeecord.database.codecs.localplayer;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.util.UUID;

public class LocalPlayerCodec implements Codec<LocalPlayer> {

	@Override
	public void encode(BsonWriter writer, LocalPlayer value, EncoderContext encoderContext) {
		
		writer.writeStartDocument();
		
		writer.writeString("_id", value.getUuid().toString());
		writer.writeString("name", value.getName());
		writer.writeString("rank", value.getRank().getName());
		writer.writeDouble("coins", value.getCoins());
		writer.writeDouble("emeralds", value.getEmeralds());
		writer.writeBoolean("ban", value.isBan());
		writer.writeBoolean("mute", value.isMute());
		writer.writeBoolean("friendsRequests", value.isFriendsRequests());
		writer.writeBoolean("privateMessages", value.isPrivateMessages());
		writer.writeBoolean("notifications", value.isNotifications());
		
		writer.writeEndDocument();
		
	}

	@Override
	public Class<LocalPlayer> getEncoderClass() {
		return LocalPlayer.class;
	}

	@Override
	public LocalPlayer decode(BsonReader reader, DecoderContext decoderContext) {
		
		final LocalPlayer localPlayer = new LocalPlayer(null, null, null, 0, 0, false, false, false, false, false);
		
		reader.readStartDocument();
		
		final String uuid = reader.readString("_id");
		localPlayer.setUuid(UUID.fromString(uuid));
		
		localPlayer.setName(reader.readString("name"));
		
		final String rankName = reader.readString("rank");
		localPlayer.setRank(RankList.getRank(rankName));
		
		localPlayer.setCoins((float) reader.readDouble("coins"));
		localPlayer.setEmeralds((float) reader.readDouble("emeralds"));
		
		localPlayer.setBan(reader.readBoolean("ban"));
		localPlayer.setMute(reader.readBoolean("mute"));
		localPlayer.setFriendsRequests(reader.readBoolean("friendsRequests"));
		localPlayer.setPrivateMessages(reader.readBoolean("privateMessages"));
		localPlayer.setNotifications(reader.readBoolean("notifications"));
		
		reader.readEndDocument();
		
		return localPlayer;
	}
	
}
