package fr.frivec.api.bungeecord.database.providers;

import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import fr.frivec.api.bungeecord.database.codecs.jump.JumpStatisticCodec;
import fr.frivec.api.bungeecord.database.codecs.jump.TimeCodec;
import fr.frivec.api.core.hub.jump.JumpStatistic;
import fr.frivec.api.core.hub.jump.Time;

public class JumpProvider implements CodecProvider {
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> Codec<T> get(Class<T> type, CodecRegistry registry) {
		
		if(type.equals(Time.class))
			
			return (Codec<T>) new TimeCodec();
		
		else if(type.equals(JumpStatistic.class))
			
			return (Codec<T>) new JumpStatisticCodec(registry);
			
		return null;
	}

}
