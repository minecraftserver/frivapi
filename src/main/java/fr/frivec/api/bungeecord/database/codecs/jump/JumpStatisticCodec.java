package fr.frivec.api.bungeecord.database.codecs.jump;

import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecRegistry;

import fr.frivec.api.core.hub.jump.JumpStatistic;
import fr.frivec.api.core.hub.jump.Time;

public class JumpStatisticCodec implements Codec<JumpStatistic> {
	
	private final CodecRegistry registries;
	
	public JumpStatisticCodec(final CodecRegistry registries) {
		
		this.registries = registries;
		
	}

	@Override
	public void encode(BsonWriter writer, JumpStatistic value, EncoderContext encoderContext) {
		
		writer.writeStartDocument();
		
		writer.writeInt32("trials", value.getTrials());
		
		writer.writeName("bestTime");
		encoderContext.encodeWithChildContext(this.registries.get(Time.class), writer, value.getBestTime());
		
		writer.writeEndDocument();
		
	}

	@Override
	public Class<JumpStatistic> getEncoderClass() {
		return JumpStatistic.class;
	}

	@Override
	public JumpStatistic decode(BsonReader reader, DecoderContext decoderContext) {
		
		final JumpStatistic jumpStatistic = new JumpStatistic(0, null);
		
		reader.readStartDocument();
		
		jumpStatistic.setTrials(reader.readInt32("trials"));
		
		reader.readName("bestTime");
		jumpStatistic.setBestTime(decoderContext.decodeWithChildContext(this.registries.get(Time.class), reader));
		
		reader.readEndDocument();
		
		return jumpStatistic;
	}
	
}
