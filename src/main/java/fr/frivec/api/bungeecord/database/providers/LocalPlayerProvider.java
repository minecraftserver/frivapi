package fr.frivec.api.bungeecord.database.providers;

import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import fr.frivec.api.bungeecord.database.codecs.localplayer.LocalPlayerCodec;
import fr.frivec.api.core.player.LocalPlayer;

public class LocalPlayerProvider implements CodecProvider {

	@SuppressWarnings("unchecked")
	@Override
	public <T> Codec<T> get(Class<T> type, CodecRegistry registry) {
		
		if(type.equals(LocalPlayer.class))
			
			return (Codec<T>) new LocalPlayerCodec();
		
		return null;
	}
	
}
