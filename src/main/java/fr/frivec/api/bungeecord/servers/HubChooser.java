package fr.frivec.api.bungeecord.servers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ReconnectHandler;
import net.md_5.bungee.api.config.ServerInfo;

public class HubChooser {
	
	private Map<UUID, ServerInfo> hubs;
	
	private ServerInfo currentHub;
	
	private int quantum, index, currentUse;
	
	public HubChooser(final int quantum) {
		
		this.hubs = new HashMap<>();
		this.quantum = quantum;
		this.index = 0;
		this.currentUse = 0;
		
	}
	
	/*
	 * Return a new reconnect handler to connect the player to the server specified
	 * 
	 */
	private boolean updateReconnectHandler(final ServerInfo server) {
		
		if(server == null)
			
			return false;
		
		final ReconnectHandler handler = new CustomReconnectHandler(server);
		
		ProxyServer.getInstance().setReconnectHandler(handler);
		
		return true;
		
	}
	
	/*
	 * Choose a hub thanks to an algorithm. 
	 */
	
	public boolean chooseHub() {
		
		/*
		 * Round robin like algorithm
		 * The server will change every quantum connection. Example: if quantum = 5, every five connection, the following server will now take the next connection
		 */
		
		if(this.currentHub != null && this.currentUse < this.quantum)
			
			/*
			 * The server currently used can still accept players. 
			 * No need to make another research.
			 * 
			 */
			
			this.currentUse++;
		
		else {
			
			/*
			 * We have to change the server
			 * Research
			 */
			
			ServerInfo info = null;
			int i = 0;
			
			for(ServerInfo hubs : this.getHubs().values()) {
				
				if(i == this.index)
					
					info = hubs;
				
				
				i++;
				
			}
			
			if(info == null) //if the server found is null, we return false. The player will be kicked.
				
				return false;
			
			this.index++;
			
			if(this.index >= this.hubs.size()) //If the index is bigger than the hubs' list size, we set it to 0 to restart the list
				
				this.index = 0;
			
			this.currentUse = 0; //We reset the number of uses since a new server.
			
			this.currentHub = info; //Set the current hub to the new server.
			
			updateReconnectHandler(this.currentHub); //Updating the reconnect handler to redirect next players to the new server.
			
		}
		
		return true;
		
	}
	
	public ServerInfo getServer(final UUID uuid) {
		
		if(this.hubs.containsKey(uuid))
			
			return this.hubs.get(uuid);
		
		return null;
		
	}
	
	public void addHub(final UUID uuid, final ServerInfo info) {
		
		if(this.hubs.containsKey(uuid))
			
			return;
		
		this.hubs.put(uuid, info);
		
	}
	
	public void removeHub(final UUID uuid) {
		
		if(!this.hubs.containsKey(uuid))
			
			return;
		
		if(this.getServer(uuid).equals(this.currentHub)) {
			
			this.currentHub = null;
			this.index = 0;
			this.currentUse = 0;
			
		}
		
		this.hubs.remove(uuid);
		
	}
	
	public ServerInfo getCurrentHub() {
		return currentHub;
	}
	
	public Map<UUID, ServerInfo> getHubs() {
		return hubs;
	}
	
	public void setHubs(Map<UUID, ServerInfo> hubs) {
		this.hubs = hubs;
	}

}
