package fr.frivec.api.bungeecord.servers;

import net.md_5.bungee.api.ReconnectHandler;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class CustomReconnectHandler implements ReconnectHandler {
	
	private ServerInfo server;
	
	public CustomReconnectHandler(final ServerInfo server) {
		
		this.server = server;
		
	}
	
	@Override
	public ServerInfo getServer(ProxiedPlayer player) {
		
		return this.server;
		
	}

	@Override
	public void setServer(ProxiedPlayer player) {}

	@Override
	public void save() {}

	@Override
	public void close() {}

}
